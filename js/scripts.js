function openmodal(t) {
  var e = getActiveModal(),
    i = $("#" + t);
  e.length > 0 ? e.animate({
    opacity: 0
  }, 500, function () {
    $(this).removeClass("js-active js-window-absolut"), i.addClass("js-active"), modalresizer(), $("#checkout").removeClass("dv-active"), i.animate({
      opacity: 1
    }, 500, function () {
      "checkout" == t && $("#checkout").addClass("dv-active")
    })
  }) : (i.addClass("js-active"), modalresizer(), i.animate({
    opacity: 1
  }, 500, function () {
    "checkout" == t && $("#checkout").addClass("dv-active")
  }), $(".modal-shadow").addClass("js-active").animate({
    opacity: .8
  }, 500)), "checkout" == t && setTimeout(function () {
    $(".dtsp-table").addClass("active")
  }, 2e3)
}

function inwrapheight(t) {
  var e = 0;
  $(t).removeAttr("style"), $(t).each(function () {
    $(this).innerHeight() > e && (e = $(this).innerHeight())
  }), $(t).css({
    height: e
  })
}

function tablegradient() {
  $(".table-def").each(function () {
    var t = $(this).parents(".js-mdt");
    $(this).width() > t.width() ? t.addClass("md-gradient") : t.removeClass("md-gradient")
  })
}

function modalresizer() {
  var t = getActiveModal();
  t.removeClass("js-window-absolut");
  var e = getBody();
  if (e.removeClass("overflow-owt"), t.innerHeight() > $(window).height() - .1 * $(window).height()) $(".modal-window.js-active").addClass("js-window-absolut"), getBody().addClass("overflow-owt");
  else {
    var i = $(window).height(),
      n = t.innerHeight(),
      o = i - n;
    o /= 2, t.css({
      top: o
    })
  }
}

function getActiveModal() {
  return $(".modal-window.js-active")
}

function getBody() {
  return $("body")
}

function carlist(t, e, i) {
  var n = 0,
    o = i,
    s = parseInt(2 * o),
    r = 1,
    a = $(t).length;
  for ($(t).removeClass(e); r < a;) $(t).slice(n, o).addClass(e), n += s, o += s, r++
}

function yaReachGoal(t) {
  if ("undefined" == typeof yaCounter42360984) var e = setInterval(function () {
    "undefined" != typeof yaCounter42360984 && (yaCounter42360984.reachGoal(t), clearInterval(e))
  }, 500);
  else yaCounter42360984.reachGoal(t)
}! function (t, e) {
  "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function (t) {
    if (!t.document) throw new Error("jQuery requires a window with a document");
    return e(t)
  } : e(t)
}("undefined" != typeof window ? window : this, function (t, e) {
  function i(t) {
    var e = t.length,
      i = st.type(t);
    return "function" !== i && !st.isWindow(t) && (!(1 !== t.nodeType || !e) || ("array" === i || 0 === e || "number" == typeof e && e > 0 && e - 1 in t))
  }

  function n(t, e, i) {
    if (st.isFunction(e)) return st.grep(t, function (t, n) {
      return !!e.call(t, n, t) !== i
    });
    if (e.nodeType) return st.grep(t, function (t) {
      return t === e !== i
    });
    if ("string" == typeof e) {
      if (pt.test(e)) return st.filter(e, t, i);
      e = st.filter(e, t)
    }
    return st.grep(t, function (t) {
      return st.inArray(t, e) >= 0 !== i
    })
  }

  function o(t, e) {
    do t = t[e]; while (t && 1 !== t.nodeType);
    return t
  }

  function s(t) {
    var e = bt[t] = {};
    return st.each(t.match(wt) || [], function (t, i) {
      e[i] = !0
    }), e
  }

  function r() {
    gt.addEventListener ? (gt.removeEventListener("DOMContentLoaded", a, !1), t.removeEventListener("load", a, !1)) : (gt.detachEvent("onreadystatechange", a), t.detachEvent("onload", a))
  }

  function a() {
    (gt.addEventListener || "load" === event.type || "complete" === gt.readyState) && (r(), st.ready())
  }

  function l(t, e, i) {
    if (void 0 === i && 1 === t.nodeType) {
      var n = "data-" + e.replace(Tt, "-$1").toLowerCase();
      if (i = t.getAttribute(n), "string" == typeof i) {
        try {
          i = "true" === i || "false" !== i && ("null" === i ? null : +i + "" === i ? +i : $t.test(i) ? st.parseJSON(i) : i)
        } catch (t) {}
        st.data(t, e, i)
      } else i = void 0
    }
    return i
  }

  function c(t) {
    var e;
    for (e in t)
      if (("data" !== e || !st.isEmptyObject(t[e])) && "toJSON" !== e) return !1;
    return !0
  }

  function u(t, e, i, n) {
    if (st.acceptData(t)) {
      var o, s, r = st.expando,
        a = t.nodeType,
        l = a ? st.cache : t,
        c = a ? t[r] : t[r] && r;
      if (c && l[c] && (n || l[c].data) || void 0 !== i || "string" != typeof e) return c || (c = a ? t[r] = Q.pop() || st.guid++ : r), l[c] || (l[c] = a ? {} : {
        toJSON: st.noop
      }), ("object" == typeof e || "function" == typeof e) && (n ? l[c] = st.extend(l[c], e) : l[c].data = st.extend(l[c].data, e)), s = l[c], n || (s.data || (s.data = {}), s = s.data), void 0 !== i && (s[st.camelCase(e)] = i), "string" == typeof e ? (o = s[e], null == o && (o = s[st.camelCase(e)])) : o = s, o
    }
  }

  function h(t, e, i) {
    if (st.acceptData(t)) {
      var n, o, s = t.nodeType,
        r = s ? st.cache : t,
        a = s ? t[st.expando] : st.expando;
      if (r[a]) {
        if (e && (n = i ? r[a] : r[a].data)) {
          st.isArray(e) ? e = e.concat(st.map(e, st.camelCase)) : e in n ? e = [e] : (e = st.camelCase(e), e = e in n ? [e] : e.split(" ")), o = e.length;
          for (; o--;) delete n[e[o]];
          if (i ? !c(n) : !st.isEmptyObject(n)) return
        }(i || (delete r[a].data, c(r[a]))) && (s ? st.cleanData([t], !0) : nt.deleteExpando || r != r.window ? delete r[a] : r[a] = null)
      }
    }
  }

  function d() {
    return !0
  }

  function p() {
    return !1
  }

  function f() {
    try {
      return gt.activeElement
    } catch (t) {}
  }

  function g(t) {
    var e = Lt.split("|"),
      i = t.createDocumentFragment();
    if (i.createElement)
      for (; e.length;) i.createElement(e.pop());
    return i
  }

  function m(t, e) {
    var i, n, o = 0,
      s = typeof t.getElementsByTagName !== Ct ? t.getElementsByTagName(e || "*") : typeof t.querySelectorAll !== Ct ? t.querySelectorAll(e || "*") : void 0;
    if (!s)
      for (s = [], i = t.childNodes || t; null != (n = i[o]); o++) !e || st.nodeName(n, e) ? s.push(n) : st.merge(s, m(n, e));
    return void 0 === e || e && st.nodeName(t, e) ? st.merge([t], s) : s
  }

  function _(t) {
    Pt.test(t.type) && (t.defaultChecked = t.checked)
  }

  function v(t, e) {
    return st.nodeName(t, "table") && st.nodeName(11 !== e.nodeType ? e : e.firstChild, "tr") ? t.getElementsByTagName("tbody")[0] || t.appendChild(t.ownerDocument.createElement("tbody")) : t
  }

  function y(t) {
    return t.type = (null !== st.find.attr(t, "type")) + "/" + t.type, t
  }

  function w(t) {
    var e = Qt.exec(t.type);
    return e ? t.type = e[1] : t.removeAttribute("type"), t
  }

  function b(t, e) {
    for (var i, n = 0; null != (i = t[n]); n++) st._data(i, "globalEval", !e || st._data(e[n], "globalEval"))
  }

  function x(t, e) {
    if (1 === e.nodeType && st.hasData(t)) {
      var i, n, o, s = st._data(t),
        r = st._data(e, s),
        a = s.events;
      if (a) {
        delete r.handle, r.events = {};
        for (i in a)
          for (n = 0, o = a[i].length; o > n; n++) st.event.add(e, i, a[i][n])
      }
      r.data && (r.data = st.extend({}, r.data))
    }
  }

  function k(t, e) {
    var i, n, o;
    if (1 === e.nodeType) {
      if (i = e.nodeName.toLowerCase(), !nt.noCloneEvent && e[st.expando]) {
        o = st._data(e);
        for (n in o.events) st.removeEvent(e, n, o.handle);
        e.removeAttribute(st.expando)
      }
      "script" === i && e.text !== t.text ? (y(e).text = t.text, w(e)) : "object" === i ? (e.parentNode && (e.outerHTML = t.outerHTML), nt.html5Clone && t.innerHTML && !st.trim(e.innerHTML) && (e.innerHTML = t.innerHTML)) : "input" === i && Pt.test(t.type) ? (e.defaultChecked = e.checked = t.checked, e.value !== t.value && (e.value = t.value)) : "option" === i ? e.defaultSelected = e.selected = t.defaultSelected : ("input" === i || "textarea" === i) && (e.defaultValue = t.defaultValue)
    }
  }

  function C(e, i) {
    var n = st(i.createElement(e)).appendTo(i.body),
      o = t.getDefaultComputedStyle ? t.getDefaultComputedStyle(n[0]).display : st.css(n[0], "display");
    return n.detach(), o
  }

  function $(t) {
    var e = gt,
      i = te[t];
    return i || (i = C(t, e), "none" !== i && i || (Jt = (Jt || st("<iframe frameborder='0' width='0' height='0'/>")).appendTo(e.documentElement), e = (Jt[0].contentWindow || Jt[0].contentDocument).document, e.write(), e.close(), i = C(t, e), Jt.detach()), te[t] = i), i
  }

  function T(t, e) {
    return {
      get: function () {
        var i = t();
        if (null != i) return i ? void delete this.get : (this.get = e).apply(this, arguments)
      }
    }
  }

  function E(t, e) {
    if (e in t) return e;
    for (var i = e.charAt(0).toUpperCase() + e.slice(1), n = e, o = pe.length; o--;)
      if (e = pe[o] + i, e in t) return e;
    return n
  }

  function j(t, e) {
    for (var i, n, o, s = [], r = 0, a = t.length; a > r; r++) n = t[r], n.style && (s[r] = st._data(n, "olddisplay"), i = n.style.display, e ? (s[r] || "none" !== i || (n.style.display = ""), "" === n.style.display && St(n) && (s[r] = st._data(n, "olddisplay", $(n.nodeName)))) : s[r] || (o = St(n), (i && "none" !== i || !o) && st._data(n, "olddisplay", o ? i : st.css(n, "display"))));
    for (r = 0; a > r; r++) n = t[r], n.style && (e && "none" !== n.style.display && "" !== n.style.display || (n.style.display = e ? s[r] || "" : "none"));
    return t
  }

  function S(t, e, i) {
    var n = ce.exec(e);
    return n ? Math.max(0, n[1] - (i || 0)) + (n[2] || "px") : e
  }

  function A(t, e, i, n, o) {
    for (var s = i === (n ? "border" : "content") ? 4 : "width" === e ? 1 : 0, r = 0; 4 > s; s += 2) "margin" === i && (r += st.css(t, i + jt[s], !0, o)), n ? ("content" === i && (r -= st.css(t, "padding" + jt[s], !0, o)), "margin" !== i && (r -= st.css(t, "border" + jt[s] + "Width", !0, o))) : (r += st.css(t, "padding" + jt[s], !0, o), "padding" !== i && (r += st.css(t, "border" + jt[s] + "Width", !0, o)));
    return r
  }

  function P(t, e, i) {
    var n = !0,
      o = "width" === e ? t.offsetWidth : t.offsetHeight,
      s = ee(t),
      r = nt.boxSizing() && "border-box" === st.css(t, "boxSizing", !1, s);
    if (0 >= o || null == o) {
      if (o = ie(t, e, s), (0 > o || null == o) && (o = t.style[e]), oe.test(o)) return o;
      n = r && (nt.boxSizingReliable() || o === t.style[e]), o = parseFloat(o) || 0
    }
    return o + A(t, e, i || (r ? "border" : "content"), n, s) + "px"
  }

  function D(t, e, i, n, o) {
    return new D.prototype.init(t, e, i, n, o)
  }

  function O() {
    return setTimeout(function () {
      fe = void 0
    }), fe = st.now()
  }

  function I(t, e) {
    var i, n = {
        height: t
      },
      o = 0;
    for (e = e ? 1 : 0; 4 > o; o += 2 - e) i = jt[o], n["margin" + i] = n["padding" + i] = t;
    return e && (n.opacity = n.width = t), n
  }

  function N(t, e, i) {
    for (var n, o = (we[e] || []).concat(we["*"]), s = 0, r = o.length; r > s; s++)
      if (n = o[s].call(i, e, t)) return n
  }

  function M(t, e, i) {
    var n, o, s, r, a, l, c, u, h = this,
      d = {},
      p = t.style,
      f = t.nodeType && St(t),
      g = st._data(t, "fxshow");
    i.queue || (a = st._queueHooks(t, "fx"), null == a.unqueued && (a.unqueued = 0, l = a.empty.fire, a.empty.fire = function () {
      a.unqueued || l()
    }), a.unqueued++, h.always(function () {
      h.always(function () {
        a.unqueued--, st.queue(t, "fx").length || a.empty.fire()
      })
    })), 1 === t.nodeType && ("height" in e || "width" in e) && (i.overflow = [p.overflow, p.overflowX, p.overflowY], c = st.css(t, "display"), u = $(t.nodeName), "none" === c && (c = u), "inline" === c && "none" === st.css(t, "float") && (nt.inlineBlockNeedsLayout && "inline" !== u ? p.zoom = 1 : p.display = "inline-block")), i.overflow && (p.overflow = "hidden", nt.shrinkWrapBlocks() || h.always(function () {
      p.overflow = i.overflow[0], p.overflowX = i.overflow[1], p.overflowY = i.overflow[2]
    }));
    for (n in e)
      if (o = e[n], me.exec(o)) {
        if (delete e[n], s = s || "toggle" === o, o === (f ? "hide" : "show")) {
          if ("show" !== o || !g || void 0 === g[n]) continue;
          f = !0
        }
        d[n] = g && g[n] || st.style(t, n)
      }
    if (!st.isEmptyObject(d)) {
      g ? "hidden" in g && (f = g.hidden) : g = st._data(t, "fxshow", {}), s && (g.hidden = !f), f ? st(t).show() : h.done(function () {
        st(t).hide()
      }), h.done(function () {
        var e;
        st._removeData(t, "fxshow");
        for (e in d) st.style(t, e, d[e])
      });
      for (n in d) r = N(f ? g[n] : 0, n, h), n in g || (g[n] = r.start, f && (r.end = r.start, r.start = "width" === n || "height" === n ? 1 : 0))
    }
  }

  function L(t, e) {
    var i, n, o, s, r;
    for (i in t)
      if (n = st.camelCase(i), o = e[n], s = t[i], st.isArray(s) && (o = s[1], s = t[i] = s[0]), i !== n && (t[n] = s, delete t[i]), r = st.cssHooks[n], r && "expand" in r) {
        s = r.expand(s), delete t[n];
        for (i in s) i in t || (t[i] = s[i], e[i] = o)
      } else e[n] = o
  }

  function z(t, e, i) {
    var n, o, s = 0,
      r = ye.length,
      a = st.Deferred().always(function () {
        delete l.elem
      }),
      l = function () {
        if (o) return !1;
        for (var e = fe || O(), i = Math.max(0, c.startTime + c.duration - e), n = i / c.duration || 0, s = 1 - n, r = 0, l = c.tweens.length; l > r; r++) c.tweens[r].run(s);
        return a.notifyWith(t, [c, s, i]), 1 > s && l ? i : (a.resolveWith(t, [c]), !1)
      },
      c = a.promise({
        elem: t,
        props: st.extend({}, e),
        opts: st.extend(!0, {
          specialEasing: {}
        }, i),
        originalProperties: e,
        originalOptions: i,
        startTime: fe || O(),
        duration: i.duration,
        tweens: [],
        createTween: function (e, i) {
          var n = st.Tween(t, c.opts, e, i, c.opts.specialEasing[e] || c.opts.easing);
          return c.tweens.push(n), n
        },
        stop: function (e) {
          var i = 0,
            n = e ? c.tweens.length : 0;
          if (o) return this;
          for (o = !0; n > i; i++) c.tweens[i].run(1);
          return e ? a.resolveWith(t, [c, e]) : a.rejectWith(t, [c, e]), this
        }
      }),
      u = c.props;
    for (L(u, c.opts.specialEasing); r > s; s++)
      if (n = ye[s].call(c, t, u, c.opts)) return n;
    return st.map(u, N, c), st.isFunction(c.opts.start) && c.opts.start.call(t, c), st.fx.timer(st.extend(l, {
      elem: t,
      anim: c,
      queue: c.opts.queue
    })), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
  }

  function H(t) {
    return function (e, i) {
      "string" != typeof e && (i = e, e = "*");
      var n, o = 0,
        s = e.toLowerCase().match(wt) || [];
      if (st.isFunction(i))
        for (; n = s[o++];) "+" === n.charAt(0) ? (n = n.slice(1) || "*", (t[n] = t[n] || []).unshift(i)) : (t[n] = t[n] || []).push(i)
    }
  }

  function R(t, e, i, n) {
    function o(a) {
      var l;
      return s[a] = !0, st.each(t[a] || [], function (t, a) {
        var c = a(e, i, n);
        return "string" != typeof c || r || s[c] ? r ? !(l = c) : void 0 : (e.dataTypes.unshift(c), o(c), !1)
      }), l
    }
    var s = {},
      r = t === We;
    return o(e.dataTypes[0]) || !s["*"] && o("*")
  }

  function F(t, e) {
    var i, n, o = st.ajaxSettings.flatOptions || {};
    for (n in e) void 0 !== e[n] && ((o[n] ? t : i || (i = {}))[n] = e[n]);
    return i && st.extend(!0, t, i), t
  }

  function B(t, e, i) {
    for (var n, o, s, r, a = t.contents, l = t.dataTypes;
      "*" === l[0];) l.shift(), void 0 === o && (o = t.mimeType || e.getResponseHeader("Content-Type"));
    if (o)
      for (r in a)
        if (a[r] && a[r].test(o)) {
          l.unshift(r);
          break
        }
    if (l[0] in i) s = l[0];
    else {
      for (r in i) {
        if (!l[0] || t.converters[r + " " + l[0]]) {
          s = r;
          break
        }
        n || (n = r)
      }
      s = s || n
    }
    return s ? (s !== l[0] && l.unshift(s), i[s]) : void 0
  }

  function q(t, e, i, n) {
    var o, s, r, a, l, c = {},
      u = t.dataTypes.slice();
    if (u[1])
      for (r in t.converters) c[r.toLowerCase()] = t.converters[r];
    for (s = u.shift(); s;)
      if (t.responseFields[s] && (i[t.responseFields[s]] = e), !l && n && t.dataFilter && (e = t.dataFilter(e, t.dataType)), l = s, s = u.shift())
        if ("*" === s) s = l;
        else if ("*" !== l && l !== s) {
      if (r = c[l + " " + s] || c["* " + s], !r)
        for (o in c)
          if (a = o.split(" "), a[1] === s && (r = c[l + " " + a[0]] || c["* " + a[0]])) {
            r === !0 ? r = c[o] : c[o] !== !0 && (s = a[0], u.unshift(a[1]));
            break
          }
      if (r !== !0)
        if (r && t.throws) e = r(e);
        else try {
          e = r(e)
        } catch (t) {
          return {
            state: "parsererror",
            error: r ? t : "No conversion from " + l + " to " + s
          }
        }
    }
    return {
      state: "success",
      data: e
    }
  }

  function W(t, e, i, n) {
    var o;
    if (st.isArray(e)) st.each(e, function (e, o) {
      i || Ue.test(t) ? n(t, o) : W(t + "[" + ("object" == typeof o ? e : "") + "]", o, i, n)
    });
    else if (i || "object" !== st.type(e)) n(t, e);
    else
      for (o in e) W(t + "[" + o + "]", e[o], i, n)
  }

  function G() {
    try {
      return new t.XMLHttpRequest
    } catch (t) {}
  }

  function X() {
    try {
      return new t.ActiveXObject("Microsoft.XMLHTTP")
    } catch (t) {}
  }

  function U(t) {
    return st.isWindow(t) ? t : 9 === t.nodeType && (t.defaultView || t.parentWindow)
  }
  var Q = [],
    V = Q.slice,
    K = Q.concat,
    Y = Q.push,
    Z = Q.indexOf,
    J = {},
    tt = J.toString,
    et = J.hasOwnProperty,
    it = "".trim,
    nt = {},
    ot = "1.11.0",
    st = function (t, e) {
      return new st.fn.init(t, e)
    },
    rt = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
    at = /^-ms-/,
    lt = /-([\da-z])/gi,
    ct = function (t, e) {
      return e.toUpperCase()
    };
  st.fn = st.prototype = {
    jquery: ot,
    constructor: st,
    selector: "",
    length: 0,
    toArray: function () {
      return V.call(this)
    },
    get: function (t) {
      return null != t ? 0 > t ? this[t + this.length] : this[t] : V.call(this)
    },
    pushStack: function (t) {
      var e = st.merge(this.constructor(), t);
      return e.prevObject = this, e.context = this.context, e
    },
    each: function (t, e) {
      return st.each(this, t, e)
    },
    map: function (t) {
      return this.pushStack(st.map(this, function (e, i) {
        return t.call(e, i, e)
      }))
    },
    slice: function () {
      return this.pushStack(V.apply(this, arguments))
    },
    first: function () {
      return this.eq(0)
    },
    last: function () {
      return this.eq(-1)
    },
    eq: function (t) {
      var e = this.length,
        i = +t + (0 > t ? e : 0);
      return this.pushStack(i >= 0 && e > i ? [this[i]] : [])
    },
    end: function () {
      return this.prevObject || this.constructor(null)
    },
    push: Y,
    sort: Q.sort,
    splice: Q.splice
  }, st.extend = st.fn.extend = function () {
    var t, e, i, n, o, s, r = arguments[0] || {},
      a = 1,
      l = arguments.length,
      c = !1;
    for ("boolean" == typeof r && (c = r, r = arguments[a] || {}, a++), "object" == typeof r || st.isFunction(r) || (r = {}), a === l && (r = this, a--); l > a; a++)
      if (null != (o = arguments[a]))
        for (n in o) t = r[n], i = o[n], r !== i && (c && i && (st.isPlainObject(i) || (e = st.isArray(i))) ? (e ? (e = !1, s = t && st.isArray(t) ? t : []) : s = t && st.isPlainObject(t) ? t : {}, r[n] = st.extend(c, s, i)) : void 0 !== i && (r[n] = i));
    return r
  }, st.extend({
    expando: "jQuery" + (ot + Math.random()).replace(/\D/g, ""),
    isReady: !0,
    error: function (t) {
      throw new Error(t)
    },
    noop: function () {},
    isFunction: function (t) {
      return "function" === st.type(t)
    },
    isArray: Array.isArray || function (t) {
      return "array" === st.type(t)
    },
    isWindow: function (t) {
      return null != t && t == t.window
    },
    isNumeric: function (t) {
      return t - parseFloat(t) >= 0
    },
    isEmptyObject: function (t) {
      var e;
      for (e in t) return !1;
      return !0
    },
    isPlainObject: function (t) {
      var e;
      if (!t || "object" !== st.type(t) || t.nodeType || st.isWindow(t)) return !1;
      try {
        if (t.constructor && !et.call(t, "constructor") && !et.call(t.constructor.prototype, "isPrototypeOf")) return !1
      } catch (t) {
        return !1
      }
      if (nt.ownLast)
        for (e in t) return et.call(t, e);
      for (e in t);
      return void 0 === e || et.call(t, e)
    },
    type: function (t) {
      return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? J[tt.call(t)] || "object" : typeof t
    },
    globalEval: function (e) {
      e && st.trim(e) && (t.execScript || function (e) {
        t.eval.call(t, e)
      })(e)
    },
    camelCase: function (t) {
      return t.replace(at, "ms-").replace(lt, ct)
    },
    nodeName: function (t, e) {
      return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
    },
    each: function (t, e, n) {
      var o, s = 0,
        r = t.length,
        a = i(t);
      if (n) {
        if (a)
          for (; r > s && (o = e.apply(t[s], n), o !== !1); s++);
        else
          for (s in t)
            if (o = e.apply(t[s], n), o === !1) break
      } else if (a)
        for (; r > s && (o = e.call(t[s], s, t[s]), o !== !1); s++);
      else
        for (s in t)
          if (o = e.call(t[s], s, t[s]), o === !1) break;
      return t
    },
    trim: it && !it.call("\ufeff ") ? function (t) {
      return null == t ? "" : it.call(t)
    } : function (t) {
      return null == t ? "" : (t + "").replace(rt, "")
    },
    makeArray: function (t, e) {
      var n = e || [];
      return null != t && (i(Object(t)) ? st.merge(n, "string" == typeof t ? [t] : t) : Y.call(n, t)), n
    },
    inArray: function (t, e, i) {
      var n;
      if (e) {
        if (Z) return Z.call(e, t, i);
        for (n = e.length, i = i ? 0 > i ? Math.max(0, n + i) : i : 0; n > i; i++)
          if (i in e && e[i] === t) return i
      }
      return -1
    },
    merge: function (t, e) {
      for (var i = +e.length, n = 0, o = t.length; i > n;) t[o++] = e[n++];
      if (i !== i)
        for (; void 0 !== e[n];) t[o++] = e[n++];
      return t.length = o, t
    },
    grep: function (t, e, i) {
      for (var n, o = [], s = 0, r = t.length, a = !i; r > s; s++) n = !e(t[s], s), n !== a && o.push(t[s]);
      return o
    },
    map: function (t, e, n) {
      var o, s = 0,
        r = t.length,
        a = i(t),
        l = [];
      if (a)
        for (; r > s; s++) o = e(t[s], s, n), null != o && l.push(o);
      else
        for (s in t) o = e(t[s], s, n), null != o && l.push(o);
      return K.apply([], l)
    },
    guid: 1,
    proxy: function (t, e) {
      var i, n, o;
      return "string" == typeof e && (o = t[e], e = t, t = o), st.isFunction(t) ? (i = V.call(arguments, 2), n = function () {
        return t.apply(e || this, i.concat(V.call(arguments)))
      }, n.guid = t.guid = t.guid || st.guid++, n) : void 0
    },
    now: function () {
      return +new Date
    },
    support: nt
  }), st.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (t, e) {
    J["[object " + e + "]"] = e.toLowerCase()
  });
  var ut = function (t) {
    function e(t, e, i, n) {
      var o, s, r, a, l, c, h, f, g, m;
      if ((e ? e.ownerDocument || e : R) !== D && P(e), e = e || D, i = i || [], !t || "string" != typeof t) return i;
      if (1 !== (a = e.nodeType) && 9 !== a) return [];
      if (I && !n) {
        if (o = vt.exec(t))
          if (r = o[1]) {
            if (9 === a) {
              if (s = e.getElementById(r), !s || !s.parentNode) return i;
              if (s.id === r) return i.push(s), i
            } else if (e.ownerDocument && (s = e.ownerDocument.getElementById(r)) && z(e, s) && s.id === r) return i.push(s), i
          } else {
            if (o[2]) return J.apply(i, e.getElementsByTagName(t)), i;
            if ((r = o[3]) && k.getElementsByClassName && e.getElementsByClassName) return J.apply(i, e.getElementsByClassName(r)), i
          }
        if (k.qsa && (!N || !N.test(t))) {
          if (f = h = H, g = e, m = 9 === a && t, 1 === a && "object" !== e.nodeName.toLowerCase()) {
            for (c = d(t), (h = e.getAttribute("id")) ? f = h.replace(wt, "\\$&") : e.setAttribute("id", f), f = "[id='" + f + "'] ", l = c.length; l--;) c[l] = f + p(c[l]);
            g = yt.test(t) && u(e.parentNode) || e, m = c.join(",")
          }
          if (m) try {
            return J.apply(i, g.querySelectorAll(m)), i
          } catch (t) {} finally {
            h || e.removeAttribute("id")
          }
        }
      }
      return b(t.replace(lt, "$1"), e, i, n)
    }

    function i() {
      function t(i, n) {
        return e.push(i + " ") > C.cacheLength && delete t[e.shift()], t[i + " "] = n
      }
      var e = [];
      return t
    }

    function n(t) {
      return t[H] = !0, t
    }

    function o(t) {
      var e = D.createElement("div");
      try {
        return !!t(e)
      } catch (t) {
        return !1
      } finally {
        e.parentNode && e.parentNode.removeChild(e), e = null
      }
    }

    function s(t, e) {
      for (var i = t.split("|"), n = t.length; n--;) C.attrHandle[i[n]] = e
    }

    function r(t, e) {
      var i = e && t,
        n = i && 1 === t.nodeType && 1 === e.nodeType && (~e.sourceIndex || Q) - (~t.sourceIndex || Q);
      if (n) return n;
      if (i)
        for (; i = i.nextSibling;)
          if (i === e) return -1;
      return t ? 1 : -1
    }

    function a(t) {
      return function (e) {
        var i = e.nodeName.toLowerCase();
        return "input" === i && e.type === t
      }
    }

    function l(t) {
      return function (e) {
        var i = e.nodeName.toLowerCase();
        return ("input" === i || "button" === i) && e.type === t
      }
    }

    function c(t) {
      return n(function (e) {
        return e = +e, n(function (i, n) {
          for (var o, s = t([], i.length, e), r = s.length; r--;) i[o = s[r]] && (i[o] = !(n[o] = i[o]))
        })
      })
    }

    function u(t) {
      return t && typeof t.getElementsByTagName !== U && t
    }

    function h() {}

    function d(t, i) {
      var n, o, s, r, a, l, c, u = W[t + " "];
      if (u) return i ? 0 : u.slice(0);
      for (a = t, l = [], c = C.preFilter; a;) {
        (!n || (o = ct.exec(a))) && (o && (a = a.slice(o[0].length) || a), l.push(s = [])), n = !1, (o = ut.exec(a)) && (n = o.shift(), s.push({
          value: n,
          type: o[0].replace(lt, " ")
        }), a = a.slice(n.length));
        for (r in C.filter) !(o = ft[r].exec(a)) || c[r] && !(o = c[r](o)) || (n = o.shift(), s.push({
          value: n,
          type: r,
          matches: o
        }), a = a.slice(n.length));
        if (!n) break
      }
      return i ? a.length : a ? e.error(t) : W(t, l).slice(0)
    }

    function p(t) {
      for (var e = 0, i = t.length, n = ""; i > e; e++) n += t[e].value;
      return n
    }

    function f(t, e, i) {
      var n = e.dir,
        o = i && "parentNode" === n,
        s = B++;
      return e.first ? function (e, i, s) {
        for (; e = e[n];)
          if (1 === e.nodeType || o) return t(e, i, s)
      } : function (e, i, r) {
        var a, l, c = [F, s];
        if (r) {
          for (; e = e[n];)
            if ((1 === e.nodeType || o) && t(e, i, r)) return !0
        } else
          for (; e = e[n];)
            if (1 === e.nodeType || o) {
              if (l = e[H] || (e[H] = {}), (a = l[n]) && a[0] === F && a[1] === s) return c[2] = a[2];
              if (l[n] = c, c[2] = t(e, i, r)) return !0
            }
      }
    }

    function g(t) {
      return t.length > 1 ? function (e, i, n) {
        for (var o = t.length; o--;)
          if (!t[o](e, i, n)) return !1;
        return !0
      } : t[0]
    }

    function m(t, e, i, n, o) {
      for (var s, r = [], a = 0, l = t.length, c = null != e; l > a; a++)(s = t[a]) && (!i || i(s, n, o)) && (r.push(s), c && e.push(a));
      return r
    }

    function _(t, e, i, o, s, r) {
      return o && !o[H] && (o = _(o)), s && !s[H] && (s = _(s, r)), n(function (n, r, a, l) {
        var c, u, h, d = [],
          p = [],
          f = r.length,
          g = n || w(e || "*", a.nodeType ? [a] : a, []),
          _ = !t || !n && e ? g : m(g, d, t, a, l),
          v = i ? s || (n ? t : f || o) ? [] : r : _;
        if (i && i(_, v, a, l), o)
          for (c = m(v, p), o(c, [], a, l), u = c.length; u--;)(h = c[u]) && (v[p[u]] = !(_[p[u]] = h));
        if (n) {
          if (s || t) {
            if (s) {
              for (c = [], u = v.length; u--;)(h = v[u]) && c.push(_[u] = h);
              s(null, v = [], c, l)
            }
            for (u = v.length; u--;)(h = v[u]) && (c = s ? et.call(n, h) : d[u]) > -1 && (n[c] = !(r[c] = h))
          }
        } else v = m(v === r ? v.splice(f, v.length) : v), s ? s(null, r, v, l) : J.apply(r, v)
      })
    }

    function v(t) {
      for (var e, i, n, o = t.length, s = C.relative[t[0].type], r = s || C.relative[" "], a = s ? 1 : 0, l = f(function (t) {
          return t === e
        }, r, !0), c = f(function (t) {
          return et.call(e, t) > -1
        }, r, !0), u = [function (t, i, n) {
          return !s && (n || i !== j) || ((e = i).nodeType ? l(t, i, n) : c(t, i, n))
        }]; o > a; a++)
        if (i = C.relative[t[a].type]) u = [f(g(u), i)];
        else {
          if (i = C.filter[t[a].type].apply(null, t[a].matches), i[H]) {
            for (n = ++a; o > n && !C.relative[t[n].type]; n++);
            return _(a > 1 && g(u), a > 1 && p(t.slice(0, a - 1).concat({
              value: " " === t[a - 2].type ? "*" : ""
            })).replace(lt, "$1"), i, n > a && v(t.slice(a, n)), o > n && v(t = t.slice(n)), o > n && p(t))
          }
          u.push(i)
        }
      return g(u)
    }

    function y(t, i) {
      var o = i.length > 0,
        s = t.length > 0,
        r = function (n, r, a, l, c) {
          var u, h, d, p = 0,
            f = "0",
            g = n && [],
            _ = [],
            v = j,
            y = n || s && C.find.TAG("*", c),
            w = F += null == v ? 1 : Math.random() || .1,
            b = y.length;
          for (c && (j = r !== D && r); f !== b && null != (u = y[f]); f++) {
            if (s && u) {
              for (h = 0; d = t[h++];)
                if (d(u, r, a)) {
                  l.push(u);
                  break
                }
              c && (F = w)
            }
            o && ((u = !d && u) && p--, n && g.push(u))
          }
          if (p += f, o && f !== p) {
            for (h = 0; d = i[h++];) d(g, _, r, a);
            if (n) {
              if (p > 0)
                for (; f--;) g[f] || _[f] || (_[f] = Y.call(l));
              _ = m(_)
            }
            J.apply(l, _), c && !n && _.length > 0 && p + i.length > 1 && e.uniqueSort(l)
          }
          return c && (F = w, j = v), g
        };
      return o ? n(r) : r
    }

    function w(t, i, n) {
      for (var o = 0, s = i.length; s > o; o++) e(t, i[o], n);
      return n
    }

    function b(t, e, i, n) {
      var o, s, r, a, l, c = d(t);
      if (!n && 1 === c.length) {
        if (s = c[0] = c[0].slice(0), s.length > 2 && "ID" === (r = s[0]).type && k.getById && 9 === e.nodeType && I && C.relative[s[1].type]) {
          if (e = (C.find.ID(r.matches[0].replace(bt, xt), e) || [])[0], !e) return i;
          t = t.slice(s.shift().value.length)
        }
        for (o = ft.needsContext.test(t) ? 0 : s.length; o-- && (r = s[o], !C.relative[a = r.type]);)
          if ((l = C.find[a]) && (n = l(r.matches[0].replace(bt, xt), yt.test(s[0].type) && u(e.parentNode) || e))) {
            if (s.splice(o, 1), t = n.length && p(s), !t) return J.apply(i, n), i;
            break
          }
      }
      return E(t, c)(n, e, !I, i, yt.test(t) && u(e.parentNode) || e), i
    }
    var x, k, C, $, T, E, j, S, A, P, D, O, I, N, M, L, z, H = "sizzle" + -new Date,
      R = t.document,
      F = 0,
      B = 0,
      q = i(),
      W = i(),
      G = i(),
      X = function (t, e) {
        return t === e && (A = !0), 0
      },
      U = "undefined",
      Q = 1 << 31,
      V = {}.hasOwnProperty,
      K = [],
      Y = K.pop,
      Z = K.push,
      J = K.push,
      tt = K.slice,
      et = K.indexOf || function (t) {
        for (var e = 0, i = this.length; i > e; e++)
          if (this[e] === t) return e;
        return -1
      },
      it = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
      nt = "[\\x20\\t\\r\\n\\f]",
      ot = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
      st = ot.replace("w", "w#"),
      rt = "\\[" + nt + "*(" + ot + ")" + nt + "*(?:([*^$|!~]?=)" + nt + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + st + ")|)|)" + nt + "*\\]",
      at = ":(" + ot + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + rt.replace(3, 8) + ")*)|.*)\\)|)",
      lt = new RegExp("^" + nt + "+|((?:^|[^\\\\])(?:\\\\.)*)" + nt + "+$", "g"),
      ct = new RegExp("^" + nt + "*," + nt + "*"),
      ut = new RegExp("^" + nt + "*([>+~]|" + nt + ")" + nt + "*"),
      ht = new RegExp("=" + nt + "*([^\\]'\"]*?)" + nt + "*\\]", "g"),
      dt = new RegExp(at),
      pt = new RegExp("^" + st + "$"),
      ft = {
        ID: new RegExp("^#(" + ot + ")"),
        CLASS: new RegExp("^\\.(" + ot + ")"),
        TAG: new RegExp("^(" + ot.replace("w", "w*") + ")"),
        ATTR: new RegExp("^" + rt),
        PSEUDO: new RegExp("^" + at),
        CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + nt + "*(even|odd|(([+-]|)(\\d*)n|)" + nt + "*(?:([+-]|)" + nt + "*(\\d+)|))" + nt + "*\\)|)", "i"),
        bool: new RegExp("^(?:" + it + ")$", "i"),
        needsContext: new RegExp("^" + nt + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + nt + "*((?:-\\d)?\\d*)" + nt + "*\\)|)(?=[^-]|$)", "i")
      },
      gt = /^(?:input|select|textarea|button)$/i,
      mt = /^h\d$/i,
      _t = /^[^{]+\{\s*\[native \w/,
      vt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
      yt = /[+~]/,
      wt = /'|\\/g,
      bt = new RegExp("\\\\([\\da-f]{1,6}" + nt + "?|(" + nt + ")|.)", "ig"),
      xt = function (t, e, i) {
        var n = "0x" + e - 65536;
        return n !== n || i ? e : 0 > n ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320)
      };
    try {
      J.apply(K = tt.call(R.childNodes), R.childNodes), K[R.childNodes.length].nodeType
    } catch (t) {
      J = {
        apply: K.length ? function (t, e) {
          Z.apply(t, tt.call(e))
        } : function (t, e) {
          for (var i = t.length, n = 0; t[i++] = e[n++];);
          t.length = i - 1
        }
      }
    }
    k = e.support = {}, T = e.isXML = function (t) {
      var e = t && (t.ownerDocument || t).documentElement;
      return !!e && "HTML" !== e.nodeName
    }, P = e.setDocument = function (t) {
      var e, i = t ? t.ownerDocument || t : R,
        n = i.defaultView;
      return i !== D && 9 === i.nodeType && i.documentElement ? (D = i, O = i.documentElement, I = !T(i), n && n !== n.top && (n.addEventListener ? n.addEventListener("unload", function () {
        P()
      }, !1) : n.attachEvent && n.attachEvent("onunload", function () {
        P()
      })), k.attributes = o(function (t) {
        return t.className = "i", !t.getAttribute("className")
      }), k.getElementsByTagName = o(function (t) {
        return t.appendChild(i.createComment("")), !t.getElementsByTagName("*").length
      }), k.getElementsByClassName = _t.test(i.getElementsByClassName) && o(function (t) {
        return t.innerHTML = "<div class='a'></div><div class='a i'></div>", t.firstChild.className = "i", 2 === t.getElementsByClassName("i").length
      }), k.getById = o(function (t) {
        return O.appendChild(t).id = H, !i.getElementsByName || !i.getElementsByName(H).length
      }), k.getById ? (C.find.ID = function (t, e) {
        if (typeof e.getElementById !== U && I) {
          var i = e.getElementById(t);
          return i && i.parentNode ? [i] : []
        }
      }, C.filter.ID = function (t) {
        var e = t.replace(bt, xt);
        return function (t) {
          return t.getAttribute("id") === e
        }
      }) : (delete C.find.ID, C.filter.ID = function (t) {
        var e = t.replace(bt, xt);
        return function (t) {
          var i = typeof t.getAttributeNode !== U && t.getAttributeNode("id");
          return i && i.value === e
        }
      }), C.find.TAG = k.getElementsByTagName ? function (t, e) {
        return typeof e.getElementsByTagName !== U ? e.getElementsByTagName(t) : void 0
      } : function (t, e) {
        var i, n = [],
          o = 0,
          s = e.getElementsByTagName(t);
        if ("*" === t) {
          for (; i = s[o++];) 1 === i.nodeType && n.push(i);
          return n
        }
        return s
      }, C.find.CLASS = k.getElementsByClassName && function (t, e) {
        return typeof e.getElementsByClassName !== U && I ? e.getElementsByClassName(t) : void 0
      }, M = [], N = [], (k.qsa = _t.test(i.querySelectorAll)) && (o(function (t) {
        t.innerHTML = "<select t=''><option selected=''></option></select>", t.querySelectorAll("[t^='']").length && N.push("[*^$]=" + nt + "*(?:''|\"\")"), t.querySelectorAll("[selected]").length || N.push("\\[" + nt + "*(?:value|" + it + ")"), t.querySelectorAll(":checked").length || N.push(":checked")
      }), o(function (t) {
        var e = i.createElement("input");
        e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && N.push("name" + nt + "*[*^$|!~]?="), t.querySelectorAll(":enabled").length || N.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), N.push(",.*:")
      })), (k.matchesSelector = _t.test(L = O.webkitMatchesSelector || O.mozMatchesSelector || O.oMatchesSelector || O.msMatchesSelector)) && o(function (t) {
        k.disconnectedMatch = L.call(t, "div"), L.call(t, "[s!='']:x"), M.push("!=", at)
      }), N = N.length && new RegExp(N.join("|")), M = M.length && new RegExp(M.join("|")), e = _t.test(O.compareDocumentPosition), z = e || _t.test(O.contains) ? function (t, e) {
        var i = 9 === t.nodeType ? t.documentElement : t,
          n = e && e.parentNode;
        return t === n || !(!n || 1 !== n.nodeType || !(i.contains ? i.contains(n) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(n)))
      } : function (t, e) {
        if (e)
          for (; e = e.parentNode;)
            if (e === t) return !0;
        return !1
      }, X = e ? function (t, e) {
        if (t === e) return A = !0, 0;
        var n = !t.compareDocumentPosition - !e.compareDocumentPosition;
        return n ? n : (n = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1, 1 & n || !k.sortDetached && e.compareDocumentPosition(t) === n ? t === i || t.ownerDocument === R && z(R, t) ? -1 : e === i || e.ownerDocument === R && z(R, e) ? 1 : S ? et.call(S, t) - et.call(S, e) : 0 : 4 & n ? -1 : 1)
      } : function (t, e) {
        if (t === e) return A = !0, 0;
        var n, o = 0,
          s = t.parentNode,
          a = e.parentNode,
          l = [t],
          c = [e];
        if (!s || !a) return t === i ? -1 : e === i ? 1 : s ? -1 : a ? 1 : S ? et.call(S, t) - et.call(S, e) : 0;
        if (s === a) return r(t, e);
        for (n = t; n = n.parentNode;) l.unshift(n);
        for (n = e; n = n.parentNode;) c.unshift(n);
        for (; l[o] === c[o];) o++;
        return o ? r(l[o], c[o]) : l[o] === R ? -1 : c[o] === R ? 1 : 0
      }, i) : D
    }, e.matches = function (t, i) {
      return e(t, null, null, i)
    }, e.matchesSelector = function (t, i) {
      if ((t.ownerDocument || t) !== D && P(t), i = i.replace(ht, "='$1']"), !(!k.matchesSelector || !I || M && M.test(i) || N && N.test(i))) try {
        var n = L.call(t, i);
        if (n || k.disconnectedMatch || t.document && 11 !== t.document.nodeType) return n
      } catch (t) {}
      return e(i, D, null, [t]).length > 0
    }, e.contains = function (t, e) {
      return (t.ownerDocument || t) !== D && P(t), z(t, e)
    }, e.attr = function (t, e) {
      (t.ownerDocument || t) !== D && P(t);
      var i = C.attrHandle[e.toLowerCase()],
        n = i && V.call(C.attrHandle, e.toLowerCase()) ? i(t, e, !I) : void 0;
      return void 0 !== n ? n : k.attributes || !I ? t.getAttribute(e) : (n = t.getAttributeNode(e)) && n.specified ? n.value : null
    }, e.error = function (t) {
      throw new Error("Syntax error, unrecognized expression: " + t)
    }, e.uniqueSort = function (t) {
      var e, i = [],
        n = 0,
        o = 0;
      if (A = !k.detectDuplicates, S = !k.sortStable && t.slice(0), t.sort(X), A) {
        for (; e = t[o++];) e === t[o] && (n = i.push(o));
        for (; n--;) t.splice(i[n], 1)
      }
      return S = null, t
    }, $ = e.getText = function (t) {
      var e, i = "",
        n = 0,
        o = t.nodeType;
      if (o) {
        if (1 === o || 9 === o || 11 === o) {
          if ("string" == typeof t.textContent) return t.textContent;
          for (t = t.firstChild; t; t = t.nextSibling) i += $(t)
        } else if (3 === o || 4 === o) return t.nodeValue
      } else
        for (; e = t[n++];) i += $(e);
      return i
    }, C = e.selectors = {
      cacheLength: 50,
      createPseudo: n,
      match: ft,
      attrHandle: {},
      find: {},
      relative: {
        ">": {
          dir: "parentNode",
          first: !0
        },
        " ": {
          dir: "parentNode"
        },
        "+": {
          dir: "previousSibling",
          first: !0
        },
        "~": {
          dir: "previousSibling"
        }
      },
      preFilter: {
        ATTR: function (t) {
          return t[1] = t[1].replace(bt, xt), t[3] = (t[4] || t[5] || "").replace(bt, xt), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4)
        },
        CHILD: function (t) {
          return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || e.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && e.error(t[0]), t
        },
        PSEUDO: function (t) {
          var e, i = !t[5] && t[2];
          return ft.CHILD.test(t[0]) ? null : (t[3] && void 0 !== t[4] ? t[2] = t[4] : i && dt.test(i) && (e = d(i, !0)) && (e = i.indexOf(")", i.length - e) - i.length) && (t[0] = t[0].slice(0, e), t[2] = i.slice(0, e)), t.slice(0, 3))
        }
      },
      filter: {
        TAG: function (t) {
          var e = t.replace(bt, xt).toLowerCase();
          return "*" === t ? function () {
            return !0
          } : function (t) {
            return t.nodeName && t.nodeName.toLowerCase() === e
          }
        },
        CLASS: function (t) {
          var e = q[t + " "];
          return e || (e = new RegExp("(^|" + nt + ")" + t + "(" + nt + "|$)")) && q(t, function (t) {
            return e.test("string" == typeof t.className && t.className || typeof t.getAttribute !== U && t.getAttribute("class") || "")
          })
        },
        ATTR: function (t, i, n) {
          return function (o) {
            var s = e.attr(o, t);
            return null == s ? "!=" === i : !i || (s += "", "=" === i ? s === n : "!=" === i ? s !== n : "^=" === i ? n && 0 === s.indexOf(n) : "*=" === i ? n && s.indexOf(n) > -1 : "$=" === i ? n && s.slice(-n.length) === n : "~=" === i ? (" " + s + " ").indexOf(n) > -1 : "|=" === i && (s === n || s.slice(0, n.length + 1) === n + "-"))
          }
        },
        CHILD: function (t, e, i, n, o) {
          var s = "nth" !== t.slice(0, 3),
            r = "last" !== t.slice(-4),
            a = "of-type" === e;
          return 1 === n && 0 === o ? function (t) {
            return !!t.parentNode
          } : function (e, i, l) {
            var c, u, h, d, p, f, g = s !== r ? "nextSibling" : "previousSibling",
              m = e.parentNode,
              _ = a && e.nodeName.toLowerCase(),
              v = !l && !a;
            if (m) {
              if (s) {
                for (; g;) {
                  for (h = e; h = h[g];)
                    if (a ? h.nodeName.toLowerCase() === _ : 1 === h.nodeType) return !1;
                  f = g = "only" === t && !f && "nextSibling"
                }
                return !0
              }
              if (f = [r ? m.firstChild : m.lastChild], r && v) {
                for (u = m[H] || (m[H] = {}), c = u[t] || [], p = c[0] === F && c[1], d = c[0] === F && c[2], h = p && m.childNodes[p]; h = ++p && h && h[g] || (d = p = 0) || f.pop();)
                  if (1 === h.nodeType && ++d && h === e) {
                    u[t] = [F, p, d];
                    break
                  }
              } else if (v && (c = (e[H] || (e[H] = {}))[t]) && c[0] === F) d = c[1];
              else
                for (;
                  (h = ++p && h && h[g] || (d = p = 0) || f.pop()) && ((a ? h.nodeName.toLowerCase() !== _ : 1 !== h.nodeType) || !++d || (v && ((h[H] || (h[H] = {}))[t] = [F, d]), h !== e)););
              return d -= o, d === n || d % n === 0 && d / n >= 0
            }
          }
        },
        PSEUDO: function (t, i) {
          var o, s = C.pseudos[t] || C.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
          return s[H] ? s(i) : s.length > 1 ? (o = [t, t, "", i], C.setFilters.hasOwnProperty(t.toLowerCase()) ? n(function (t, e) {
            for (var n, o = s(t, i), r = o.length; r--;) n = et.call(t, o[r]), t[n] = !(e[n] = o[r])
          }) : function (t) {
            return s(t, 0, o)
          }) : s
        }
      },
      pseudos: {
        not: n(function (t) {
          var e = [],
            i = [],
            o = E(t.replace(lt, "$1"));
          return o[H] ? n(function (t, e, i, n) {
            for (var s, r = o(t, null, n, []), a = t.length; a--;)(s = r[a]) && (t[a] = !(e[a] = s))
          }) : function (t, n, s) {
            return e[0] = t, o(e, null, s, i), !i.pop()
          }
        }),
        has: n(function (t) {
          return function (i) {
            return e(t, i).length > 0
          }
        }),
        contains: n(function (t) {
          return function (e) {
            return (e.textContent || e.innerText || $(e)).indexOf(t) > -1
          }
        }),
        lang: n(function (t) {
          return pt.test(t || "") || e.error("unsupported lang: " + t), t = t.replace(bt, xt).toLowerCase(),
            function (e) {
              var i;
              do
                if (i = I ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return i = i.toLowerCase(), i === t || 0 === i.indexOf(t + "-"); while ((e = e.parentNode) && 1 === e.nodeType);
              return !1
            }
        }),
        target: function (e) {
          var i = t.location && t.location.hash;
          return i && i.slice(1) === e.id
        },
        root: function (t) {
          return t === O
        },
        focus: function (t) {
          return t === D.activeElement && (!D.hasFocus || D.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
        },
        enabled: function (t) {
          return t.disabled === !1
        },
        disabled: function (t) {
          return t.disabled === !0
        },
        checked: function (t) {
          var e = t.nodeName.toLowerCase();
          return "input" === e && !!t.checked || "option" === e && !!t.selected
        },
        selected: function (t) {
          return t.parentNode && t.parentNode.selectedIndex, t.selected === !0
        },
        empty: function (t) {
          for (t = t.firstChild; t; t = t.nextSibling)
            if (t.nodeType < 6) return !1;
          return !0
        },
        parent: function (t) {
          return !C.pseudos.empty(t)
        },
        header: function (t) {
          return mt.test(t.nodeName)
        },
        input: function (t) {
          return gt.test(t.nodeName)
        },
        button: function (t) {
          var e = t.nodeName.toLowerCase();
          return "input" === e && "button" === t.type || "button" === e
        },
        text: function (t) {
          var e;
          return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
        },
        first: c(function () {
          return [0]
        }),
        last: c(function (t, e) {
          return [e - 1]
        }),
        eq: c(function (t, e, i) {
          return [0 > i ? i + e : i]
        }),
        even: c(function (t, e) {
          for (var i = 0; e > i; i += 2) t.push(i);
          return t
        }),
        odd: c(function (t, e) {
          for (var i = 1; e > i; i += 2) t.push(i);
          return t
        }),
        lt: c(function (t, e, i) {
          for (var n = 0 > i ? i + e : i; --n >= 0;) t.push(n);
          return t
        }),
        gt: c(function (t, e, i) {
          for (var n = 0 > i ? i + e : i; ++n < e;) t.push(n);
          return t
        })
      }
    }, C.pseudos.nth = C.pseudos.eq;
    for (x in {
        radio: !0,
        checkbox: !0,
        file: !0,
        password: !0,
        image: !0
      }) C.pseudos[x] = a(x);
    for (x in {
        submit: !0,
        reset: !0
      }) C.pseudos[x] = l(x);
    return h.prototype = C.filters = C.pseudos, C.setFilters = new h, E = e.compile = function (t, e) {
      var i, n = [],
        o = [],
        s = G[t + " "];
      if (!s) {
        for (e || (e = d(t)), i = e.length; i--;) s = v(e[i]), s[H] ? n.push(s) : o.push(s);
        s = G(t, y(o, n))
      }
      return s
    }, k.sortStable = H.split("").sort(X).join("") === H, k.detectDuplicates = !!A, P(), k.sortDetached = o(function (t) {
      return 1 & t.compareDocumentPosition(D.createElement("div"))
    }), o(function (t) {
      return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href")
    }) || s("type|href|height|width", function (t, e, i) {
      return i ? void 0 : t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
    }), k.attributes && o(function (t) {
      return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value")
    }) || s("value", function (t, e, i) {
      return i || "input" !== t.nodeName.toLowerCase() ? void 0 : t.defaultValue
    }), o(function (t) {
      return null == t.getAttribute("disabled")
    }) || s(it, function (t, e, i) {
      var n;
      return i ? void 0 : t[e] === !0 ? e.toLowerCase() : (n = t.getAttributeNode(e)) && n.specified ? n.value : null
    }), e
  }(t);
  st.find = ut, st.expr = ut.selectors, st.expr[":"] = st.expr.pseudos, st.unique = ut.uniqueSort, st.text = ut.getText, st.isXMLDoc = ut.isXML, st.contains = ut.contains;
  var ht = st.expr.match.needsContext,
    dt = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
    pt = /^.[^:#\[\.,]*$/;
  st.filter = function (t, e, i) {
    var n = e[0];
    return i && (t = ":not(" + t + ")"), 1 === e.length && 1 === n.nodeType ? st.find.matchesSelector(n, t) ? [n] : [] : st.find.matches(t, st.grep(e, function (t) {
      return 1 === t.nodeType
    }))
  }, st.fn.extend({
    find: function (t) {
      var e, i = [],
        n = this,
        o = n.length;
      if ("string" != typeof t) return this.pushStack(st(t).filter(function () {
        for (e = 0; o > e; e++)
          if (st.contains(n[e], this)) return !0
      }));
      for (e = 0; o > e; e++) st.find(t, n[e], i);
      return i = this.pushStack(o > 1 ? st.unique(i) : i), i.selector = this.selector ? this.selector + " " + t : t, i
    },
    filter: function (t) {
      return this.pushStack(n(this, t || [], !1))
    },
    not: function (t) {
      return this.pushStack(n(this, t || [], !0))
    },
    is: function (t) {
      return !!n(this, "string" == typeof t && ht.test(t) ? st(t) : t || [], !1).length
    }
  });
  var ft, gt = t.document,
    mt = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
    _t = st.fn.init = function (t, e) {
      var i, n;
      if (!t) return this;
      if ("string" == typeof t) {
        if (i = "<" === t.charAt(0) && ">" === t.charAt(t.length - 1) && t.length >= 3 ? [null, t, null] : mt.exec(t), !i || !i[1] && e) return !e || e.jquery ? (e || ft).find(t) : this.constructor(e).find(t);
        if (i[1]) {
          if (e = e instanceof st ? e[0] : e, st.merge(this, st.parseHTML(i[1], e && e.nodeType ? e.ownerDocument || e : gt, !0)), dt.test(i[1]) && st.isPlainObject(e))
            for (i in e) st.isFunction(this[i]) ? this[i](e[i]) : this.attr(i, e[i]);
          return this
        }
        if (n = gt.getElementById(i[2]), n && n.parentNode) {
          if (n.id !== i[2]) return ft.find(t);
          this.length = 1, this[0] = n
        }
        return this.context = gt, this.selector = t, this
      }
      return t.nodeType ? (this.context = this[0] = t, this.length = 1, this) : st.isFunction(t) ? "undefined" != typeof ft.ready ? ft.ready(t) : t(st) : (void 0 !== t.selector && (this.selector = t.selector, this.context = t.context), st.makeArray(t, this))
    };
  _t.prototype = st.fn, ft = st(gt);
  var vt = /^(?:parents|prev(?:Until|All))/,
    yt = {
      children: !0,
      contents: !0,
      next: !0,
      prev: !0
    };
  st.extend({
    dir: function (t, e, i) {
      for (var n = [], o = t[e]; o && 9 !== o.nodeType && (void 0 === i || 1 !== o.nodeType || !st(o).is(i));) 1 === o.nodeType && n.push(o), o = o[e];
      return n
    },
    sibling: function (t, e) {
      for (var i = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && i.push(t);
      return i
    }
  }), st.fn.extend({
    has: function (t) {
      var e, i = st(t, this),
        n = i.length;
      return this.filter(function () {
        for (e = 0; n > e; e++)
          if (st.contains(this, i[e])) return !0
      })
    },
    closest: function (t, e) {
      for (var i, n = 0, o = this.length, s = [], r = ht.test(t) || "string" != typeof t ? st(t, e || this.context) : 0; o > n; n++)
        for (i = this[n]; i && i !== e; i = i.parentNode)
          if (i.nodeType < 11 && (r ? r.index(i) > -1 : 1 === i.nodeType && st.find.matchesSelector(i, t))) {
            s.push(i);
            break
          }
      return this.pushStack(s.length > 1 ? st.unique(s) : s)
    },
    index: function (t) {
      return t ? "string" == typeof t ? st.inArray(this[0], st(t)) : st.inArray(t.jquery ? t[0] : t, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
    },
    add: function (t, e) {
      return this.pushStack(st.unique(st.merge(this.get(), st(t, e))))
    },
    addBack: function (t) {
      return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
    }
  }), st.each({
    parent: function (t) {
      var e = t.parentNode;
      return e && 11 !== e.nodeType ? e : null
    },
    parents: function (t) {
      return st.dir(t, "parentNode")
    },
    parentsUntil: function (t, e, i) {
      return st.dir(t, "parentNode", i)
    },
    next: function (t) {
      return o(t, "nextSibling")
    },
    prev: function (t) {
      return o(t, "previousSibling")
    },
    nextAll: function (t) {
      return st.dir(t, "nextSibling")
    },
    prevAll: function (t) {
      return st.dir(t, "previousSibling")
    },
    nextUntil: function (t, e, i) {
      return st.dir(t, "nextSibling", i)
    },
    prevUntil: function (t, e, i) {
      return st.dir(t, "previousSibling", i)
    },
    siblings: function (t) {
      return st.sibling((t.parentNode || {}).firstChild, t)
    },
    children: function (t) {
      return st.sibling(t.firstChild)
    },
    contents: function (t) {
      return st.nodeName(t, "iframe") ? t.contentDocument || t.contentWindow.document : st.merge([], t.childNodes)
    }
  }, function (t, e) {
    st.fn[t] = function (i, n) {
      var o = st.map(this, e, i);
      return "Until" !== t.slice(-5) && (n = i), n && "string" == typeof n && (o = st.filter(n, o)), this.length > 1 && (yt[t] || (o = st.unique(o)), vt.test(t) && (o = o.reverse())), this.pushStack(o)
    }
  });
  var wt = /\S+/g,
    bt = {};
  st.Callbacks = function (t) {
    t = "string" == typeof t ? bt[t] || s(t) : st.extend({}, t);
    var e, i, n, o, r, a, l = [],
      c = !t.once && [],
      u = function (s) {
        for (i = t.memory && s, n = !0, r = a || 0, a = 0, o = l.length, e = !0; l && o > r; r++)
          if (l[r].apply(s[0], s[1]) === !1 && t.stopOnFalse) {
            i = !1;
            break
          }
        e = !1, l && (c ? c.length && u(c.shift()) : i ? l = [] : h.disable())
      },
      h = {
        add: function () {
          if (l) {
            var n = l.length;
            ! function e(i) {
              st.each(i, function (i, n) {
                var o = st.type(n);
                "function" === o ? t.unique && h.has(n) || l.push(n) : n && n.length && "string" !== o && e(n)
              })
            }(arguments), e ? o = l.length : i && (a = n, u(i))
          }
          return this
        },
        remove: function () {
          return l && st.each(arguments, function (t, i) {
            for (var n;
              (n = st.inArray(i, l, n)) > -1;) l.splice(n, 1), e && (o >= n && o--, r >= n && r--)
          }), this
        },
        has: function (t) {
          return t ? st.inArray(t, l) > -1 : !(!l || !l.length)
        },
        empty: function () {
          return l = [], o = 0, this
        },
        disable: function () {
          return l = c = i = void 0, this
        },
        disabled: function () {
          return !l
        },
        lock: function () {
          return c = void 0, i || h.disable(), this
        },
        locked: function () {
          return !c
        },
        fireWith: function (t, i) {
          return !l || n && !c || (i = i || [], i = [t, i.slice ? i.slice() : i], e ? c.push(i) : u(i)), this
        },
        fire: function () {
          return h.fireWith(this, arguments), this
        },
        fired: function () {
          return !!n
        }
      };
    return h
  }, st.extend({
    Deferred: function (t) {
      var e = [
          ["resolve", "done", st.Callbacks("once memory"), "resolved"],
          ["reject", "fail", st.Callbacks("once memory"), "rejected"],
          ["notify", "progress", st.Callbacks("memory")]
        ],
        i = "pending",
        n = {
          state: function () {
            return i
          },
          always: function () {
            return o.done(arguments).fail(arguments), this
          },
          then: function () {
            var t = arguments;
            return st.Deferred(function (i) {
              st.each(e, function (e, s) {
                var r = st.isFunction(t[e]) && t[e];
                o[s[1]](function () {
                  var t = r && r.apply(this, arguments);
                  t && st.isFunction(t.promise) ? t.promise().done(i.resolve).fail(i.reject).progress(i.notify) : i[s[0] + "With"](this === n ? i.promise() : this, r ? [t] : arguments)
                })
              }), t = null
            }).promise()
          },
          promise: function (t) {
            return null != t ? st.extend(t, n) : n
          }
        },
        o = {};
      return n.pipe = n.then, st.each(e, function (t, s) {
        var r = s[2],
          a = s[3];
        n[s[1]] = r.add, a && r.add(function () {
          i = a
        }, e[1 ^ t][2].disable, e[2][2].lock), o[s[0]] = function () {
          return o[s[0] + "With"](this === o ? n : this, arguments), this
        }, o[s[0] + "With"] = r.fireWith
      }), n.promise(o), t && t.call(o, o), o
    },
    when: function (t) {
      var e, i, n, o = 0,
        s = V.call(arguments),
        r = s.length,
        a = 1 !== r || t && st.isFunction(t.promise) ? r : 0,
        l = 1 === a ? t : st.Deferred(),
        c = function (t, i, n) {
          return function (o) {
            i[t] = this, n[t] = arguments.length > 1 ? V.call(arguments) : o, n === e ? l.notifyWith(i, n) : --a || l.resolveWith(i, n)
          }
        };
      if (r > 1)
        for (e = new Array(r), i = new Array(r), n = new Array(r); r > o; o++) s[o] && st.isFunction(s[o].promise) ? s[o].promise().done(c(o, n, s)).fail(l.reject).progress(c(o, i, e)) : --a;
      return a || l.resolveWith(n, s), l.promise()
    }
  });
  var xt;
  st.fn.ready = function (t) {
    return st.ready.promise().done(t), this
  }, st.extend({
    isReady: !1,
    readyWait: 1,
    holdReady: function (t) {
      t ? st.readyWait++ : st.ready(!0)
    },
    ready: function (t) {
      if (t === !0 ? !--st.readyWait : !st.isReady) {
        if (!gt.body) return setTimeout(st.ready);
        st.isReady = !0, t !== !0 && --st.readyWait > 0 || (xt.resolveWith(gt, [st]), st.fn.trigger && st(gt).trigger("ready").off("ready"))
      }
    }
  }), st.ready.promise = function (e) {
    if (!xt)
      if (xt = st.Deferred(), "complete" === gt.readyState) setTimeout(st.ready);
      else if (gt.addEventListener) gt.addEventListener("DOMContentLoaded", a, !1), t.addEventListener("load", a, !1);
    else {
      gt.attachEvent("onreadystatechange", a), t.attachEvent("onload", a);
      var i = !1;
      try {
        i = null == t.frameElement && gt.documentElement
      } catch (t) {}
      i && i.doScroll && ! function t() {
        if (!st.isReady) {
          try {
            i.doScroll("left")
          } catch (e) {
            return setTimeout(t, 50)
          }
          r(), st.ready()
        }
      }()
    }
    return xt.promise(e)
  };
  var kt, Ct = "undefined";
  for (kt in st(nt)) break;
  nt.ownLast = "0" !== kt, nt.inlineBlockNeedsLayout = !1, st(function () {
      var t, e, i = gt.getElementsByTagName("body")[0];
      i && (t = gt.createElement("div"), t.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px", e = gt.createElement("div"), i.appendChild(t).appendChild(e), typeof e.style.zoom !== Ct && (e.style.cssText = "border:0;margin:0;width:1px;padding:1px;display:inline;zoom:1", (nt.inlineBlockNeedsLayout = 3 === e.offsetWidth) && (i.style.zoom = 1)), i.removeChild(t), t = e = null)
    }),
    function () {
      var t = gt.createElement("div");
      if (null == nt.deleteExpando) {
        nt.deleteExpando = !0;
        try {
          delete t.test
        } catch (t) {
          nt.deleteExpando = !1
        }
      }
      t = null
    }(), st.acceptData = function (t) {
      var e = st.noData[(t.nodeName + " ").toLowerCase()],
        i = +t.nodeType || 1;
      return (1 === i || 9 === i) && (!e || e !== !0 && t.getAttribute("classid") === e)
    };
  var $t = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
    Tt = /([A-Z])/g;
  st.extend({
    cache: {},
    noData: {
      "applet ": !0,
      "embed ": !0,
      "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
    },
    hasData: function (t) {
      return t = t.nodeType ? st.cache[t[st.expando]] : t[st.expando], !!t && !c(t)
    },
    data: function (t, e, i) {
      return u(t, e, i)
    },
    removeData: function (t, e) {
      return h(t, e)
    },
    _data: function (t, e, i) {
      return u(t, e, i, !0)
    },
    _removeData: function (t, e) {
      return h(t, e, !0)
    }
  }), st.fn.extend({
    data: function (t, e) {
      var i, n, o, s = this[0],
        r = s && s.attributes;
      if (void 0 === t) {
        if (this.length && (o = st.data(s), 1 === s.nodeType && !st._data(s, "parsedAttrs"))) {
          for (i = r.length; i--;) n = r[i].name, 0 === n.indexOf("data-") && (n = st.camelCase(n.slice(5)), l(s, n, o[n]));
          st._data(s, "parsedAttrs", !0)
        }
        return o
      }
      return "object" == typeof t ? this.each(function () {
        st.data(this, t)
      }) : arguments.length > 1 ? this.each(function () {
        st.data(this, t, e)
      }) : s ? l(s, t, st.data(s, t)) : void 0
    },
    removeData: function (t) {
      return this.each(function () {
        st.removeData(this, t)
      })
    }
  }), st.extend({
    queue: function (t, e, i) {
      var n;
      return t ? (e = (e || "fx") + "queue", n = st._data(t, e), i && (!n || st.isArray(i) ? n = st._data(t, e, st.makeArray(i)) : n.push(i)), n || []) : void 0
    },
    dequeue: function (t, e) {
      e = e || "fx";
      var i = st.queue(t, e),
        n = i.length,
        o = i.shift(),
        s = st._queueHooks(t, e),
        r = function () {
          st.dequeue(t, e)
        };
      "inprogress" === o && (o = i.shift(), n--), o && ("fx" === e && i.unshift("inprogress"), delete s.stop, o.call(t, r, s)), !n && s && s.empty.fire()
    },
    _queueHooks: function (t, e) {
      var i = e + "queueHooks";
      return st._data(t, i) || st._data(t, i, {
        empty: st.Callbacks("once memory").add(function () {
          st._removeData(t, e + "queue"), st._removeData(t, i)
        })
      })
    }
  }), st.fn.extend({
    queue: function (t, e) {
      var i = 2;
      return "string" != typeof t && (e = t, t = "fx", i--), arguments.length < i ? st.queue(this[0], t) : void 0 === e ? this : this.each(function () {
        var i = st.queue(this, t, e);
        st._queueHooks(this, t), "fx" === t && "inprogress" !== i[0] && st.dequeue(this, t)
      })
    },
    dequeue: function (t) {
      return this.each(function () {
        st.dequeue(this, t)
      })
    },
    clearQueue: function (t) {
      return this.queue(t || "fx", [])
    },
    promise: function (t, e) {
      var i, n = 1,
        o = st.Deferred(),
        s = this,
        r = this.length,
        a = function () {
          --n || o.resolveWith(s, [s])
        };
      for ("string" != typeof t && (e = t, t = void 0), t = t || "fx"; r--;) i = st._data(s[r], t + "queueHooks"), i && i.empty && (n++, i.empty.add(a));
      return a(), o.promise(e)
    }
  });
  var Et = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
    jt = ["Top", "Right", "Bottom", "Left"],
    St = function (t, e) {
      return t = e || t, "none" === st.css(t, "display") || !st.contains(t.ownerDocument, t)
    },
    At = st.access = function (t, e, i, n, o, s, r) {
      var a = 0,
        l = t.length,
        c = null == i;
      if ("object" === st.type(i)) {
        o = !0;
        for (a in i) st.access(t, e, a, i[a], !0, s, r)
      } else if (void 0 !== n && (o = !0, st.isFunction(n) || (r = !0), c && (r ? (e.call(t, n), e = null) : (c = e, e = function (t, e, i) {
          return c.call(st(t), i)
        })), e))
        for (; l > a; a++) e(t[a], i, r ? n : n.call(t[a], a, e(t[a], i)));
      return o ? t : c ? e.call(t) : l ? e(t[0], i) : s
    },
    Pt = /^(?:checkbox|radio)$/i;
  ! function () {
    var t = gt.createDocumentFragment(),
      e = gt.createElement("div"),
      i = gt.createElement("input");
    if (e.setAttribute("className", "t"), e.innerHTML = "  <link/><table></table><a href='/a'>a</a>", nt.leadingWhitespace = 3 === e.firstChild.nodeType, nt.tbody = !e.getElementsByTagName("tbody").length, nt.htmlSerialize = !!e.getElementsByTagName("link").length, nt.html5Clone = "<:nav></:nav>" !== gt.createElement("nav").cloneNode(!0).outerHTML, i.type = "checkbox", i.checked = !0, t.appendChild(i), nt.appendChecked = i.checked, e.innerHTML = "<textarea>x</textarea>", nt.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue, t.appendChild(e), e.innerHTML = "<input type='radio' checked='checked' name='t'/>", nt.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked, nt.noCloneEvent = !0, e.attachEvent && (e.attachEvent("onclick", function () {
        nt.noCloneEvent = !1
      }), e.cloneNode(!0).click()), null == nt.deleteExpando) {
      nt.deleteExpando = !0;
      try {
        delete e.test
      } catch (t) {
        nt.deleteExpando = !1
      }
    }
    t = e = i = null
  }(),
  function () {
    var e, i, n = gt.createElement("div");
    for (e in {
        submit: !0,
        change: !0,
        focusin: !0
      }) i = "on" + e, (nt[e + "Bubbles"] = i in t) || (n.setAttribute(i, "t"), nt[e + "Bubbles"] = n.attributes[i].expando === !1);
    n = null
  }();
  var Dt = /^(?:input|select|textarea)$/i,
    Ot = /^key/,
    It = /^(?:mouse|contextmenu)|click/,
    Nt = /^(?:focusinfocus|focusoutblur)$/,
    Mt = /^([^.]*)(?:\.(.+)|)$/;
  st.event = {
    global: {},
    add: function (t, e, i, n, o) {
      var s, r, a, l, c, u, h, d, p, f, g, m = st._data(t);
      if (m) {
        for (i.handler && (l = i, i = l.handler, o = l.selector), i.guid || (i.guid = st.guid++), (r = m.events) || (r = m.events = {}), (u = m.handle) || (u = m.handle = function (t) {
            return typeof st === Ct || t && st.event.triggered === t.type ? void 0 : st.event.dispatch.apply(u.elem, arguments)
          }, u.elem = t), e = (e || "").match(wt) || [""], a = e.length; a--;) s = Mt.exec(e[a]) || [], p = g = s[1], f = (s[2] || "").split(".").sort(), p && (c = st.event.special[p] || {}, p = (o ? c.delegateType : c.bindType) || p, c = st.event.special[p] || {}, h = st.extend({
          type: p,
          origType: g,
          data: n,
          handler: i,
          guid: i.guid,
          selector: o,
          needsContext: o && st.expr.match.needsContext.test(o),
          namespace: f.join(".")
        }, l), (d = r[p]) || (d = r[p] = [], d.delegateCount = 0, c.setup && c.setup.call(t, n, f, u) !== !1 || (t.addEventListener ? t.addEventListener(p, u, !1) : t.attachEvent && t.attachEvent("on" + p, u))), c.add && (c.add.call(t, h), h.handler.guid || (h.handler.guid = i.guid)), o ? d.splice(d.delegateCount++, 0, h) : d.push(h), st.event.global[p] = !0);
        t = null
      }
    },
    remove: function (t, e, i, n, o) {
      var s, r, a, l, c, u, h, d, p, f, g, m = st.hasData(t) && st._data(t);
      if (m && (u = m.events)) {
        for (e = (e || "").match(wt) || [""], c = e.length; c--;)
          if (a = Mt.exec(e[c]) || [], p = g = a[1], f = (a[2] || "").split(".").sort(), p) {
            for (h = st.event.special[p] || {}, p = (n ? h.delegateType : h.bindType) || p, d = u[p] || [], a = a[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), l = s = d.length; s--;) r = d[s], !o && g !== r.origType || i && i.guid !== r.guid || a && !a.test(r.namespace) || n && n !== r.selector && ("**" !== n || !r.selector) || (d.splice(s, 1), r.selector && d.delegateCount--, h.remove && h.remove.call(t, r));
            l && !d.length && (h.teardown && h.teardown.call(t, f, m.handle) !== !1 || st.removeEvent(t, p, m.handle), delete u[p])
          } else
            for (p in u) st.event.remove(t, p + e[c], i, n, !0);
        st.isEmptyObject(u) && (delete m.handle, st._removeData(t, "events"))
      }
    },
    trigger: function (e, i, n, o) {
      var s, r, a, l, c, u, h, d = [n || gt],
        p = et.call(e, "type") ? e.type : e,
        f = et.call(e, "namespace") ? e.namespace.split(".") : [];
      if (a = u = n = n || gt, 3 !== n.nodeType && 8 !== n.nodeType && !Nt.test(p + st.event.triggered) && (p.indexOf(".") >= 0 && (f = p.split("."), p = f.shift(), f.sort()), r = p.indexOf(":") < 0 && "on" + p, e = e[st.expando] ? e : new st.Event(p, "object" == typeof e && e), e.isTrigger = o ? 2 : 3, e.namespace = f.join("."), e.namespace_re = e.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = n), i = null == i ? [e] : st.makeArray(i, [e]), c = st.event.special[p] || {}, o || !c.trigger || c.trigger.apply(n, i) !== !1)) {
        if (!o && !c.noBubble && !st.isWindow(n)) {
          for (l = c.delegateType || p, Nt.test(l + p) || (a = a.parentNode); a; a = a.parentNode) d.push(a), u = a;
          u === (n.ownerDocument || gt) && d.push(u.defaultView || u.parentWindow || t)
        }
        for (h = 0;
          (a = d[h++]) && !e.isPropagationStopped();) e.type = h > 1 ? l : c.bindType || p, s = (st._data(a, "events") || {})[e.type] && st._data(a, "handle"), s && s.apply(a, i), s = r && a[r], s && s.apply && st.acceptData(a) && (e.result = s.apply(a, i), e.result === !1 && e.preventDefault());
        if (e.type = p, !o && !e.isDefaultPrevented() && (!c._default || c._default.apply(d.pop(), i) === !1) && st.acceptData(n) && r && n[p] && !st.isWindow(n)) {
          u = n[r], u && (n[r] = null), st.event.triggered = p;
          try {
            n[p]()
          } catch (t) {}
          st.event.triggered = void 0, u && (n[r] = u)
        }
        return e.result
      }
    },
    dispatch: function (t) {
      t = st.event.fix(t);
      var e, i, n, o, s, r = [],
        a = V.call(arguments),
        l = (st._data(this, "events") || {})[t.type] || [],
        c = st.event.special[t.type] || {};
      if (a[0] = t, t.delegateTarget = this, !c.preDispatch || c.preDispatch.call(this, t) !== !1) {
        for (r = st.event.handlers.call(this, t, l), e = 0;
          (o = r[e++]) && !t.isPropagationStopped();)
          for (t.currentTarget = o.elem, s = 0;
            (n = o.handlers[s++]) && !t.isImmediatePropagationStopped();)(!t.namespace_re || t.namespace_re.test(n.namespace)) && (t.handleObj = n, t.data = n.data, i = ((st.event.special[n.origType] || {}).handle || n.handler).apply(o.elem, a), void 0 !== i && (t.result = i) === !1 && (t.preventDefault(), t.stopPropagation()));
        return c.postDispatch && c.postDispatch.call(this, t), t.result
      }
    },
    handlers: function (t, e) {
      var i, n, o, s, r = [],
        a = e.delegateCount,
        l = t.target;
      if (a && l.nodeType && (!t.button || "click" !== t.type))
        for (; l != this; l = l.parentNode || this)
          if (1 === l.nodeType && (l.disabled !== !0 || "click" !== t.type)) {
            for (o = [], s = 0; a > s; s++) n = e[s], i = n.selector + " ", void 0 === o[i] && (o[i] = n.needsContext ? st(i, this).index(l) >= 0 : st.find(i, this, null, [l]).length), o[i] && o.push(n);
            o.length && r.push({
              elem: l,
              handlers: o
            })
          }
      return a < e.length && r.push({
        elem: this,
        handlers: e.slice(a)
      }), r
    },
    fix: function (t) {
      if (t[st.expando]) return t;
      var e, i, n, o = t.type,
        s = t,
        r = this.fixHooks[o];
      for (r || (this.fixHooks[o] = r = It.test(o) ? this.mouseHooks : Ot.test(o) ? this.keyHooks : {}), n = r.props ? this.props.concat(r.props) : this.props, t = new st.Event(s), e = n.length; e--;) i = n[e], t[i] = s[i];
      return t.target || (t.target = s.srcElement || gt), 3 === t.target.nodeType && (t.target = t.target.parentNode), t.metaKey = !!t.metaKey, r.filter ? r.filter(t, s) : t
    },
    props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
    fixHooks: {},
    keyHooks: {
      props: "char charCode key keyCode".split(" "),
      filter: function (t, e) {
        return null == t.which && (t.which = null != e.charCode ? e.charCode : e.keyCode), t
      }
    },
    mouseHooks: {
      props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
      filter: function (t, e) {
        var i, n, o, s = e.button,
          r = e.fromElement;
        return null == t.pageX && null != e.clientX && (n = t.target.ownerDocument || gt, o = n.documentElement, i = n.body, t.pageX = e.clientX + (o && o.scrollLeft || i && i.scrollLeft || 0) - (o && o.clientLeft || i && i.clientLeft || 0), t.pageY = e.clientY + (o && o.scrollTop || i && i.scrollTop || 0) - (o && o.clientTop || i && i.clientTop || 0)), !t.relatedTarget && r && (t.relatedTarget = r === t.target ? e.toElement : r), t.which || void 0 === s || (t.which = 1 & s ? 1 : 2 & s ? 3 : 4 & s ? 2 : 0), t
      }
    },
    special: {
      load: {
        noBubble: !0
      },
      focus: {
        trigger: function () {
          if (this !== f() && this.focus) try {
            return this.focus(), !1
          } catch (t) {}
        },
        delegateType: "focusin"
      },
      blur: {
        trigger: function () {
          return this === f() && this.blur ? (this.blur(), !1) : void 0
        },
        delegateType: "focusout"
      },
      click: {
        trigger: function () {
          return st.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
        },
        _default: function (t) {
          return st.nodeName(t.target, "a")
        }
      },
      beforeunload: {
        postDispatch: function (t) {
          void 0 !== t.result && (t.originalEvent.returnValue = t.result)
        }
      }
    },
    simulate: function (t, e, i, n) {
      var o = st.extend(new st.Event, i, {
        type: t,
        isSimulated: !0,
        originalEvent: {}
      });
      n ? st.event.trigger(o, null, e) : st.event.dispatch.call(e, o), o.isDefaultPrevented() && i.preventDefault()
    }
  }, st.removeEvent = gt.removeEventListener ? function (t, e, i) {
    t.removeEventListener && t.removeEventListener(e, i, !1)
  } : function (t, e, i) {
    var n = "on" + e;
    t.detachEvent && (typeof t[n] === Ct && (t[n] = null), t.detachEvent(n, i))
  }, st.Event = function (t, e) {
    return this instanceof st.Event ? (t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && (t.returnValue === !1 || t.getPreventDefault && t.getPreventDefault()) ? d : p) : this.type = t, e && st.extend(this, e), this.timeStamp = t && t.timeStamp || st.now(), void(this[st.expando] = !0)) : new st.Event(t, e)
  }, st.Event.prototype = {
    isDefaultPrevented: p,
    isPropagationStopped: p,
    isImmediatePropagationStopped: p,
    preventDefault: function () {
      var t = this.originalEvent;
      this.isDefaultPrevented = d, t && (t.preventDefault ? t.preventDefault() : t.returnValue = !1)
    },
    stopPropagation: function () {
      var t = this.originalEvent;
      this.isPropagationStopped = d, t && (t.stopPropagation && t.stopPropagation(), t.cancelBubble = !0)
    },
    stopImmediatePropagation: function () {
      this.isImmediatePropagationStopped = d, this.stopPropagation()
    }
  }, st.each({
    mouseenter: "mouseover",
    mouseleave: "mouseout"
  }, function (t, e) {
    st.event.special[t] = {
      delegateType: e,
      bindType: e,
      handle: function (t) {
        var i, n = this,
          o = t.relatedTarget,
          s = t.handleObj;
        return (!o || o !== n && !st.contains(n, o)) && (t.type = s.origType, i = s.handler.apply(this, arguments), t.type = e), i
      }
    }
  }), nt.submitBubbles || (st.event.special.submit = {
    setup: function () {
      return !st.nodeName(this, "form") && void st.event.add(this, "click._submit keypress._submit", function (t) {
        var e = t.target,
          i = st.nodeName(e, "input") || st.nodeName(e, "button") ? e.form : void 0;
        i && !st._data(i, "submitBubbles") && (st.event.add(i, "submit._submit", function (t) {
          t._submit_bubble = !0
        }), st._data(i, "submitBubbles", !0))
      })
    },
    postDispatch: function (t) {
      t._submit_bubble && (delete t._submit_bubble, this.parentNode && !t.isTrigger && st.event.simulate("submit", this.parentNode, t, !0))
    },
    teardown: function () {
      return !st.nodeName(this, "form") && void st.event.remove(this, "._submit")
    }
  }), nt.changeBubbles || (st.event.special.change = {
    setup: function () {
      return Dt.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (st.event.add(this, "propertychange._change", function (t) {
        "checked" === t.originalEvent.propertyName && (this._just_changed = !0)
      }), st.event.add(this, "click._change", function (t) {
        this._just_changed && !t.isTrigger && (this._just_changed = !1), st.event.simulate("change", this, t, !0)
      })), !1) : void st.event.add(this, "beforeactivate._change", function (t) {
        var e = t.target;
        Dt.test(e.nodeName) && !st._data(e, "changeBubbles") && (st.event.add(e, "change._change", function (t) {
          !this.parentNode || t.isSimulated || t.isTrigger || st.event.simulate("change", this.parentNode, t, !0)
        }), st._data(e, "changeBubbles", !0))
      })
    },
    handle: function (t) {
      var e = t.target;
      return this !== e || t.isSimulated || t.isTrigger || "radio" !== e.type && "checkbox" !== e.type ? t.handleObj.handler.apply(this, arguments) : void 0
    },
    teardown: function () {
      return st.event.remove(this, "._change"), !Dt.test(this.nodeName)
    }
  }), nt.focusinBubbles || st.each({
    focus: "focusin",
    blur: "focusout"
  }, function (t, e) {
    var i = function (t) {
      st.event.simulate(e, t.target, st.event.fix(t), !0)
    };
    st.event.special[e] = {
      setup: function () {
        var n = this.ownerDocument || this,
          o = st._data(n, e);
        o || n.addEventListener(t, i, !0), st._data(n, e, (o || 0) + 1)
      },
      teardown: function () {
        var n = this.ownerDocument || this,
          o = st._data(n, e) - 1;
        o ? st._data(n, e, o) : (n.removeEventListener(t, i, !0), st._removeData(n, e))
      }
    }
  }), st.fn.extend({
    on: function (t, e, i, n, o) {
      var s, r;
      if ("object" == typeof t) {
        "string" != typeof e && (i = i || e, e = void 0);
        for (s in t) this.on(s, e, i, t[s], o);
        return this
      }
      if (null == i && null == n ? (n = e, i = e = void 0) : null == n && ("string" == typeof e ? (n = i, i = void 0) : (n = i, i = e, e = void 0)), n === !1) n = p;
      else if (!n) return this;
      return 1 === o && (r = n, n = function (t) {
        return st().off(t), r.apply(this, arguments)
      }, n.guid = r.guid || (r.guid = st.guid++)), this.each(function () {
        st.event.add(this, t, n, i, e)
      })
    },
    one: function (t, e, i, n) {
      return this.on(t, e, i, n, 1)
    },
    off: function (t, e, i) {
      var n, o;
      if (t && t.preventDefault && t.handleObj) return n = t.handleObj, st(t.delegateTarget).off(n.namespace ? n.origType + "." + n.namespace : n.origType, n.selector, n.handler), this;
      if ("object" == typeof t) {
        for (o in t) this.off(o, e, t[o]);
        return this
      }
      return (e === !1 || "function" == typeof e) && (i = e, e = void 0), i === !1 && (i = p), this.each(function () {
        st.event.remove(this, t, i, e)
      })
    },
    trigger: function (t, e) {
      return this.each(function () {
        st.event.trigger(t, e, this)
      })
    },
    triggerHandler: function (t, e) {
      var i = this[0];
      return i ? st.event.trigger(t, e, i, !0) : void 0
    }
  });
  var Lt = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
    zt = / jQuery\d+="(?:null|\d+)"/g,
    Ht = new RegExp("<(?:" + Lt + ")[\\s/>]", "i"),
    Rt = /^\s+/,
    Ft = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
    Bt = /<([\w:]+)/,
    qt = /<tbody/i,
    Wt = /<|&#?\w+;/,
    Gt = /<(?:script|style|link)/i,
    Xt = /checked\s*(?:[^=]|=\s*.checked.)/i,
    Ut = /^$|\/(?:java|ecma)script/i,
    Qt = /^true\/(.*)/,
    Vt = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
    Kt = {
      option: [1, "<select multiple='multiple'>", "</select>"],
      legend: [1, "<fieldset>", "</fieldset>"],
      area: [1, "<map>", "</map>"],
      param: [1, "<object>", "</object>"],
      thead: [1, "<table>", "</table>"],
      tr: [2, "<table><tbody>", "</tbody></table>"],
      col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
      td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
      _default: nt.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
    },
    Yt = g(gt),
    Zt = Yt.appendChild(gt.createElement("div"));
  Kt.optgroup = Kt.option, Kt.tbody = Kt.tfoot = Kt.colgroup = Kt.caption = Kt.thead, Kt.th = Kt.td, st.extend({
    clone: function (t, e, i) {
      var n, o, s, r, a, l = st.contains(t.ownerDocument, t);
      if (nt.html5Clone || st.isXMLDoc(t) || !Ht.test("<" + t.nodeName + ">") ? s = t.cloneNode(!0) : (Zt.innerHTML = t.outerHTML, Zt.removeChild(s = Zt.firstChild)), !(nt.noCloneEvent && nt.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || st.isXMLDoc(t)))
        for (n = m(s), a = m(t), r = 0; null != (o = a[r]); ++r) n[r] && k(o, n[r]);
      if (e)
        if (i)
          for (a = a || m(t), n = n || m(s), r = 0; null != (o = a[r]); r++) x(o, n[r]);
        else x(t, s);
      return n = m(s, "script"), n.length > 0 && b(n, !l && m(t, "script")), n = a = o = null, s
    },
    buildFragment: function (t, e, i, n) {
      for (var o, s, r, a, l, c, u, h = t.length, d = g(e), p = [], f = 0; h > f; f++)
        if (s = t[f], s || 0 === s)
          if ("object" === st.type(s)) st.merge(p, s.nodeType ? [s] : s);
          else if (Wt.test(s)) {
        for (a = a || d.appendChild(e.createElement("div")), l = (Bt.exec(s) || ["", ""])[1].toLowerCase(), u = Kt[l] || Kt._default, a.innerHTML = u[1] + s.replace(Ft, "<$1><$2>") + u[2], o = u[0]; o--;) a = a.lastChild;
        if (!nt.leadingWhitespace && Rt.test(s) && p.push(e.createTextNode(Rt.exec(s)[0])), !nt.tbody)
          for (s = "table" !== l || qt.test(s) ? "<table>" !== u[1] || qt.test(s) ? 0 : a : a.firstChild, o = s && s.childNodes.length; o--;) st.nodeName(c = s.childNodes[o], "tbody") && !c.childNodes.length && s.removeChild(c);
        for (st.merge(p, a.childNodes), a.textContent = ""; a.firstChild;) a.removeChild(a.firstChild);
        a = d.lastChild
      } else p.push(e.createTextNode(s));
      for (a && d.removeChild(a), nt.appendChecked || st.grep(m(p, "input"), _), f = 0; s = p[f++];)
        if ((!n || -1 === st.inArray(s, n)) && (r = st.contains(s.ownerDocument, s), a = m(d.appendChild(s), "script"), r && b(a), i))
          for (o = 0; s = a[o++];) Ut.test(s.type || "") && i.push(s);
      return a = null, d
    },
    cleanData: function (t, e) {
      for (var i, n, o, s, r = 0, a = st.expando, l = st.cache, c = nt.deleteExpando, u = st.event.special; null != (i = t[r]); r++)
        if ((e || st.acceptData(i)) && (o = i[a], s = o && l[o])) {
          if (s.events)
            for (n in s.events) u[n] ? st.event.remove(i, n) : st.removeEvent(i, n, s.handle);
          l[o] && (delete l[o], c ? delete i[a] : typeof i.removeAttribute !== Ct ? i.removeAttribute(a) : i[a] = null, Q.push(o))
        }
    }
  }), st.fn.extend({
    text: function (t) {
      return At(this, function (t) {
        return void 0 === t ? st.text(this) : this.empty().append((this[0] && this[0].ownerDocument || gt).createTextNode(t))
      }, null, t, arguments.length)
    },
    append: function () {
      return this.domManip(arguments, function (t) {
        if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
          var e = v(this, t);
          e.appendChild(t)
        }
      })
    },
    prepend: function () {
      return this.domManip(arguments, function (t) {
        if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
          var e = v(this, t);
          e.insertBefore(t, e.firstChild)
        }
      })
    },
    before: function () {
      return this.domManip(arguments, function (t) {
        this.parentNode && this.parentNode.insertBefore(t, this)
      })
    },
    after: function () {
      return this.domManip(arguments, function (t) {
        this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
      })
    },
    remove: function (t, e) {
      for (var i, n = t ? st.filter(t, this) : this, o = 0; null != (i = n[o]); o++) e || 1 !== i.nodeType || st.cleanData(m(i)), i.parentNode && (e && st.contains(i.ownerDocument, i) && b(m(i, "script")), i.parentNode.removeChild(i));
      return this
    },
    empty: function () {
      for (var t, e = 0; null != (t = this[e]); e++) {
        for (1 === t.nodeType && st.cleanData(m(t, !1)); t.firstChild;) t.removeChild(t.firstChild);
        t.options && st.nodeName(t, "select") && (t.options.length = 0)
      }
      return this
    },
    clone: function (t, e) {
      return t = null != t && t, e = null == e ? t : e, this.map(function () {
        return st.clone(this, t, e)
      })
    },
    html: function (t) {
      return At(this, function (t) {
        var e = this[0] || {},
          i = 0,
          n = this.length;
        if (void 0 === t) return 1 === e.nodeType ? e.innerHTML.replace(zt, "") : void 0;
        if (!("string" != typeof t || Gt.test(t) || !nt.htmlSerialize && Ht.test(t) || !nt.leadingWhitespace && Rt.test(t) || Kt[(Bt.exec(t) || ["", ""])[1].toLowerCase()])) {
          t = t.replace(Ft, "<$1><$2>");
          try {
            for (; n > i; i++) e = this[i] || {}, 1 === e.nodeType && (st.cleanData(m(e, !1)), e.innerHTML = t);
            e = 0
          } catch (t) {}
        }
        e && this.empty().append(t)
      }, null, t, arguments.length)
    },
    replaceWith: function () {
      var t = arguments[0];
      return this.domManip(arguments, function (e) {
        t = this.parentNode, st.cleanData(m(this)), t && t.replaceChild(e, this)
      }), t && (t.length || t.nodeType) ? this : this.remove()
    },
    detach: function (t) {
      return this.remove(t, !0)
    },
    domManip: function (t, e) {
      t = K.apply([], t);
      var i, n, o, s, r, a, l = 0,
        c = this.length,
        u = this,
        h = c - 1,
        d = t[0],
        p = st.isFunction(d);
      if (p || c > 1 && "string" == typeof d && !nt.checkClone && Xt.test(d)) return this.each(function (i) {
        var n = u.eq(i);
        p && (t[0] = d.call(this, i, n.html())), n.domManip(t, e)
      });
      if (c && (a = st.buildFragment(t, this[0].ownerDocument, !1, this), i = a.firstChild, 1 === a.childNodes.length && (a = i), i)) {
        for (s = st.map(m(a, "script"), y), o = s.length; c > l; l++) n = a, l !== h && (n = st.clone(n, !0, !0), o && st.merge(s, m(n, "script"))), e.call(this[l], n, l);
        if (o)
          for (r = s[s.length - 1].ownerDocument, st.map(s, w), l = 0; o > l; l++) n = s[l], Ut.test(n.type || "") && !st._data(n, "globalEval") && st.contains(r, n) && (n.src ? st._evalUrl && st._evalUrl(n.src) : st.globalEval((n.text || n.textContent || n.innerHTML || "").replace(Vt, "")));
        a = i = null
      }
      return this;
    }
  }), st.each({
    appendTo: "append",
    prependTo: "prepend",
    insertBefore: "before",
    insertAfter: "after",
    replaceAll: "replaceWith"
  }, function (t, e) {
    st.fn[t] = function (t) {
      for (var i, n = 0, o = [], s = st(t), r = s.length - 1; r >= n; n++) i = n === r ? this : this.clone(!0), st(s[n])[e](i), Y.apply(o, i.get());
      return this.pushStack(o)
    }
  });
  var Jt, te = {};
  ! function () {
    var t, e, i = gt.createElement("div"),
      n = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;padding:0;margin:0;border:0";
    i.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", t = i.getElementsByTagName("a")[0], t.style.cssText = "float:left;opacity:.5", nt.opacity = /^0.5/.test(t.style.opacity), nt.cssFloat = !!t.style.cssFloat, i.style.backgroundClip = "content-box", i.cloneNode(!0).style.backgroundClip = "", nt.clearCloneStyle = "content-box" === i.style.backgroundClip, t = i = null, nt.shrinkWrapBlocks = function () {
      var t, i, o, s;
      if (null == e) {
        if (t = gt.getElementsByTagName("body")[0], !t) return;
        s = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px", i = gt.createElement("div"), o = gt.createElement("div"), t.appendChild(i).appendChild(o), e = !1, typeof o.style.zoom !== Ct && (o.style.cssText = n + ";width:1px;padding:1px;zoom:1", o.innerHTML = "<div></div>", o.firstChild.style.width = "5px", e = 3 !== o.offsetWidth), t.removeChild(i), t = i = o = null
      }
      return e
    }
  }();
  var ee, ie, ne = /^margin/,
    oe = new RegExp("^(" + Et + ")(?!px)[a-z%]+$", "i"),
    se = /^(top|right|bottom|left)$/;
  t.getComputedStyle ? (ee = function (t) {
    return t.ownerDocument.defaultView.getComputedStyle(t, null)
  }, ie = function (t, e, i) {
    var n, o, s, r, a = t.style;
    return i = i || ee(t), r = i ? i.getPropertyValue(e) || i[e] : void 0, i && ("" !== r || st.contains(t.ownerDocument, t) || (r = st.style(t, e)), oe.test(r) && ne.test(e) && (n = a.width, o = a.minWidth, s = a.maxWidth, a.minWidth = a.maxWidth = a.width = r, r = i.width, a.width = n, a.minWidth = o, a.maxWidth = s)), void 0 === r ? r : r + ""
  }) : gt.documentElement.currentStyle && (ee = function (t) {
    return t.currentStyle
  }, ie = function (t, e, i) {
    var n, o, s, r, a = t.style;
    return i = i || ee(t), r = i ? i[e] : void 0, null == r && a && a[e] && (r = a[e]), oe.test(r) && !se.test(e) && (n = a.left, o = t.runtimeStyle, s = o && o.left, s && (o.left = t.currentStyle.left), a.left = "fontSize" === e ? "1em" : r, r = a.pixelLeft + "px", a.left = n, s && (o.left = s)), void 0 === r ? r : r + "" || "auto"
  }), ! function () {
    function e() {
      var e, i, n = gt.getElementsByTagName("body")[0];
      n && (e = gt.createElement("div"), i = gt.createElement("div"), e.style.cssText = c, n.appendChild(e).appendChild(i), i.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:absolute;display:block;padding:1px;border:1px;width:4px;margin-top:1%;top:1%", st.swap(n, null != n.style.zoom ? {
        zoom: 1
      } : {}, function () {
        o = 4 === i.offsetWidth
      }), s = !0, r = !1, a = !0, t.getComputedStyle && (r = "1%" !== (t.getComputedStyle(i, null) || {}).top, s = "4px" === (t.getComputedStyle(i, null) || {
        width: "4px"
      }).width), n.removeChild(e), i = n = null)
    }
    var i, n, o, s, r, a, l = gt.createElement("div"),
      c = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px",
      u = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;padding:0;margin:0;border:0";
    l.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", i = l.getElementsByTagName("a")[0], i.style.cssText = "float:left;opacity:.5", nt.opacity = /^0.5/.test(i.style.opacity), nt.cssFloat = !!i.style.cssFloat, l.style.backgroundClip = "content-box", l.cloneNode(!0).style.backgroundClip = "", nt.clearCloneStyle = "content-box" === l.style.backgroundClip, i = l = null, st.extend(nt, {
      reliableHiddenOffsets: function () {
        if (null != n) return n;
        var t, e, i, o = gt.createElement("div"),
          s = gt.getElementsByTagName("body")[0];
        return s ? (o.setAttribute("className", "t"), o.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", t = gt.createElement("div"), t.style.cssText = c, s.appendChild(t).appendChild(o), o.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", e = o.getElementsByTagName("td"), e[0].style.cssText = "padding:0;margin:0;border:0;display:none", i = 0 === e[0].offsetHeight, e[0].style.display = "", e[1].style.display = "none", n = i && 0 === e[0].offsetHeight, s.removeChild(t), o = s = null, n) : void 0
      },
      boxSizing: function () {
        return null == o && e(), o
      },
      boxSizingReliable: function () {
        return null == s && e(), s
      },
      pixelPosition: function () {
        return null == r && e(), r
      },
      reliableMarginRight: function () {
        var e, i, n, o;
        if (null == a && t.getComputedStyle) {
          if (e = gt.getElementsByTagName("body")[0], !e) return;
          i = gt.createElement("div"), n = gt.createElement("div"), i.style.cssText = c, e.appendChild(i).appendChild(n), o = n.appendChild(gt.createElement("div")), o.style.cssText = n.style.cssText = u, o.style.marginRight = o.style.width = "0", n.style.width = "1px", a = !parseFloat((t.getComputedStyle(o, null) || {}).marginRight), e.removeChild(i)
        }
        return a
      }
    })
  }(), st.swap = function (t, e, i, n) {
    var o, s, r = {};
    for (s in e) r[s] = t.style[s], t.style[s] = e[s];
    o = i.apply(t, n || []);
    for (s in e) t.style[s] = r[s];
    return o
  };
  var re = /alpha\([^)]*\)/i,
    ae = /opacity\s*=\s*([^)]*)/,
    le = /^(none|table(?!-c[ea]).+)/,
    ce = new RegExp("^(" + Et + ")(.*)$", "i"),
    ue = new RegExp("^([+-])=(" + Et + ")", "i"),
    he = {
      position: "absolute",
      visibility: "hidden",
      display: "block"
    },
    de = {
      letterSpacing: 0,
      fontWeight: 400
    },
    pe = ["Webkit", "O", "Moz", "ms"];
  st.extend({
    cssHooks: {
      opacity: {
        get: function (t, e) {
          if (e) {
            var i = ie(t, "opacity");
            return "" === i ? "1" : i
          }
        }
      }
    },
    cssNumber: {
      columnCount: !0,
      fillOpacity: !0,
      fontWeight: !0,
      lineHeight: !0,
      opacity: !0,
      order: !0,
      orphans: !0,
      widows: !0,
      zIndex: !0,
      zoom: !0
    },
    cssProps: {
      float: nt.cssFloat ? "cssFloat" : "styleFloat"
    },
    style: function (t, e, i, n) {
      if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
        var o, s, r, a = st.camelCase(e),
          l = t.style;
        if (e = st.cssProps[a] || (st.cssProps[a] = E(l, a)), r = st.cssHooks[e] || st.cssHooks[a], void 0 === i) return r && "get" in r && void 0 !== (o = r.get(t, !1, n)) ? o : l[e];
        if (s = typeof i, "string" === s && (o = ue.exec(i)) && (i = (o[1] + 1) * o[2] + parseFloat(st.css(t, e)), s = "number"), null != i && i === i && ("number" !== s || st.cssNumber[a] || (i += "px"), nt.clearCloneStyle || "" !== i || 0 !== e.indexOf("background") || (l[e] = "inherit"), !(r && "set" in r && void 0 === (i = r.set(t, i, n))))) try {
          l[e] = "", l[e] = i
        } catch (t) {}
      }
    },
    css: function (t, e, i, n) {
      var o, s, r, a = st.camelCase(e);
      return e = st.cssProps[a] || (st.cssProps[a] = E(t.style, a)), r = st.cssHooks[e] || st.cssHooks[a], r && "get" in r && (s = r.get(t, !0, i)), void 0 === s && (s = ie(t, e, n)), "normal" === s && e in de && (s = de[e]), "" === i || i ? (o = parseFloat(s), i === !0 || st.isNumeric(o) ? o || 0 : s) : s
    }
  }), st.each(["height", "width"], function (t, e) {
    st.cssHooks[e] = {
      get: function (t, i, n) {
        return i ? 0 === t.offsetWidth && le.test(st.css(t, "display")) ? st.swap(t, he, function () {
          return P(t, e, n)
        }) : P(t, e, n) : void 0
      },
      set: function (t, i, n) {
        var o = n && ee(t);
        return S(t, i, n ? A(t, e, n, nt.boxSizing() && "border-box" === st.css(t, "boxSizing", !1, o), o) : 0)
      }
    }
  }), nt.opacity || (st.cssHooks.opacity = {
    get: function (t, e) {
      return ae.test((e && t.currentStyle ? t.currentStyle.filter : t.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : e ? "1" : ""
    },
    set: function (t, e) {
      var i = t.style,
        n = t.currentStyle,
        o = st.isNumeric(e) ? "alpha(opacity=" + 100 * e + ")" : "",
        s = n && n.filter || i.filter || "";
      i.zoom = 1, (e >= 1 || "" === e) && "" === st.trim(s.replace(re, "")) && i.removeAttribute && (i.removeAttribute("filter"), "" === e || n && !n.filter) || (i.filter = re.test(s) ? s.replace(re, o) : s + " " + o)
    }
  }), st.cssHooks.marginRight = T(nt.reliableMarginRight, function (t, e) {
    return e ? st.swap(t, {
      display: "inline-block"
    }, ie, [t, "marginRight"]) : void 0
  }), st.each({
    margin: "",
    padding: "",
    border: "Width"
  }, function (t, e) {
    st.cssHooks[t + e] = {
      expand: function (i) {
        for (var n = 0, o = {}, s = "string" == typeof i ? i.split(" ") : [i]; 4 > n; n++) o[t + jt[n] + e] = s[n] || s[n - 2] || s[0];
        return o
      }
    }, ne.test(t) || (st.cssHooks[t + e].set = S)
  }), st.fn.extend({
    css: function (t, e) {
      return At(this, function (t, e, i) {
        var n, o, s = {},
          r = 0;
        if (st.isArray(e)) {
          for (n = ee(t), o = e.length; o > r; r++) s[e[r]] = st.css(t, e[r], !1, n);
          return s
        }
        return void 0 !== i ? st.style(t, e, i) : st.css(t, e)
      }, t, e, arguments.length > 1)
    },
    show: function () {
      return j(this, !0)
    },
    hide: function () {
      return j(this)
    },
    toggle: function (t) {
      return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function () {
        St(this) ? st(this).show() : st(this).hide()
      })
    }
  }), st.Tween = D, D.prototype = {
    constructor: D,
    init: function (t, e, i, n, o, s) {
      this.elem = t, this.prop = i, this.easing = o || "swing", this.options = e, this.start = this.now = this.cur(), this.end = n, this.unit = s || (st.cssNumber[i] ? "" : "px")
    },
    cur: function () {
      var t = D.propHooks[this.prop];
      return t && t.get ? t.get(this) : D.propHooks._default.get(this)
    },
    run: function (t) {
      var e, i = D.propHooks[this.prop];
      return this.pos = e = this.options.duration ? st.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), i && i.set ? i.set(this) : D.propHooks._default.set(this), this
    }
  }, D.prototype.init.prototype = D.prototype, D.propHooks = {
    _default: {
      get: function (t) {
        var e;
        return null == t.elem[t.prop] || t.elem.style && null != t.elem.style[t.prop] ? (e = st.css(t.elem, t.prop, ""), e && "auto" !== e ? e : 0) : t.elem[t.prop]
      },
      set: function (t) {
        st.fx.step[t.prop] ? st.fx.step[t.prop](t) : t.elem.style && (null != t.elem.style[st.cssProps[t.prop]] || st.cssHooks[t.prop]) ? st.style(t.elem, t.prop, t.now + t.unit) : t.elem[t.prop] = t.now
      }
    }
  }, D.propHooks.scrollTop = D.propHooks.scrollLeft = {
    set: function (t) {
      t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
    }
  }, st.easing = {
    linear: function (t) {
      return t
    },
    swing: function (t) {
      return .5 - Math.cos(t * Math.PI) / 2
    }
  }, st.fx = D.prototype.init, st.fx.step = {};
  var fe, ge, me = /^(?:toggle|show|hide)$/,
    _e = new RegExp("^(?:([+-])=|)(" + Et + ")([a-z%]*)$", "i"),
    ve = /queueHooks$/,
    ye = [M],
    we = {
      "*": [function (t, e) {
        var i = this.createTween(t, e),
          n = i.cur(),
          o = _e.exec(e),
          s = o && o[3] || (st.cssNumber[t] ? "" : "px"),
          r = (st.cssNumber[t] || "px" !== s && +n) && _e.exec(st.css(i.elem, t)),
          a = 1,
          l = 20;
        if (r && r[3] !== s) {
          s = s || r[3], o = o || [], r = +n || 1;
          do a = a || ".5", r /= a, st.style(i.elem, t, r + s); while (a !== (a = i.cur() / n) && 1 !== a && --l)
        }
        return o && (r = i.start = +r || +n || 0, i.unit = s, i.end = o[1] ? r + (o[1] + 1) * o[2] : +o[2]), i
      }]
    };
  st.Animation = st.extend(z, {
      tweener: function (t, e) {
        st.isFunction(t) ? (e = t, t = ["*"]) : t = t.split(" ");
        for (var i, n = 0, o = t.length; o > n; n++) i = t[n], we[i] = we[i] || [], we[i].unshift(e)
      },
      prefilter: function (t, e) {
        e ? ye.unshift(t) : ye.push(t)
      }
    }), st.speed = function (t, e, i) {
      var n = t && "object" == typeof t ? st.extend({}, t) : {
        complete: i || !i && e || st.isFunction(t) && t,
        duration: t,
        easing: i && e || e && !st.isFunction(e) && e
      };
      return n.duration = st.fx.off ? 0 : "number" == typeof n.duration ? n.duration : n.duration in st.fx.speeds ? st.fx.speeds[n.duration] : st.fx.speeds._default, (null == n.queue || n.queue === !0) && (n.queue = "fx"), n.old = n.complete, n.complete = function () {
        st.isFunction(n.old) && n.old.call(this), n.queue && st.dequeue(this, n.queue)
      }, n
    }, st.fn.extend({
      fadeTo: function (t, e, i, n) {
        return this.filter(St).css("opacity", 0).show().end().animate({
          opacity: e
        }, t, i, n)
      },
      animate: function (t, e, i, n) {
        var o = st.isEmptyObject(t),
          s = st.speed(e, i, n),
          r = function () {
            var e = z(this, st.extend({}, t), s);
            (o || st._data(this, "finish")) && e.stop(!0)
          };
        return r.finish = r, o || s.queue === !1 ? this.each(r) : this.queue(s.queue, r)
      },
      stop: function (t, e, i) {
        var n = function (t) {
          var e = t.stop;
          delete t.stop, e(i)
        };
        return "string" != typeof t && (i = e, e = t, t = void 0), e && t !== !1 && this.queue(t || "fx", []), this.each(function () {
          var e = !0,
            o = null != t && t + "queueHooks",
            s = st.timers,
            r = st._data(this);
          if (o) r[o] && r[o].stop && n(r[o]);
          else
            for (o in r) r[o] && r[o].stop && ve.test(o) && n(r[o]);
          for (o = s.length; o--;) s[o].elem !== this || null != t && s[o].queue !== t || (s[o].anim.stop(i), e = !1, s.splice(o, 1));
          (e || !i) && st.dequeue(this, t)
        })
      },
      finish: function (t) {
        return t !== !1 && (t = t || "fx"), this.each(function () {
          var e, i = st._data(this),
            n = i[t + "queue"],
            o = i[t + "queueHooks"],
            s = st.timers,
            r = n ? n.length : 0;
          for (i.finish = !0, st.queue(this, t, []), o && o.stop && o.stop.call(this, !0), e = s.length; e--;) s[e].elem === this && s[e].queue === t && (s[e].anim.stop(!0), s.splice(e, 1));
          for (e = 0; r > e; e++) n[e] && n[e].finish && n[e].finish.call(this);
          delete i.finish
        })
      }
    }), st.each(["toggle", "show", "hide"], function (t, e) {
      var i = st.fn[e];
      st.fn[e] = function (t, n, o) {
        return null == t || "boolean" == typeof t ? i.apply(this, arguments) : this.animate(I(e, !0), t, n, o)
      }
    }), st.each({
      slideDown: I("show"),
      slideUp: I("hide"),
      slideToggle: I("toggle"),
      fadeIn: {
        opacity: "show"
      },
      fadeOut: {
        opacity: "hide"
      },
      fadeToggle: {
        opacity: "toggle"
      }
    }, function (t, e) {
      st.fn[t] = function (t, i, n) {
        return this.animate(e, t, i, n)
      }
    }), st.timers = [], st.fx.tick = function () {
      var t, e = st.timers,
        i = 0;
      for (fe = st.now(); i < e.length; i++) t = e[i], t() || e[i] !== t || e.splice(i--, 1);
      e.length || st.fx.stop(), fe = void 0
    }, st.fx.timer = function (t) {
      st.timers.push(t), t() ? st.fx.start() : st.timers.pop()
    }, st.fx.interval = 13, st.fx.start = function () {
      ge || (ge = setInterval(st.fx.tick, st.fx.interval))
    }, st.fx.stop = function () {
      clearInterval(ge), ge = null
    }, st.fx.speeds = {
      slow: 600,
      fast: 200,
      _default: 400
    }, st.fn.delay = function (t, e) {
      return t = st.fx ? st.fx.speeds[t] || t : t, e = e || "fx", this.queue(e, function (e, i) {
        var n = setTimeout(e, t);
        i.stop = function () {
          clearTimeout(n)
        }
      })
    },
    function () {
      var t, e, i, n, o = gt.createElement("div");
      o.setAttribute("className", "t"), o.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", t = o.getElementsByTagName("a")[0], i = gt.createElement("select"), n = i.appendChild(gt.createElement("option")), e = o.getElementsByTagName("input")[0], t.style.cssText = "top:1px", nt.getSetAttribute = "t" !== o.className, nt.style = /top/.test(t.getAttribute("style")), nt.hrefNormalized = "/a" === t.getAttribute("href"), nt.checkOn = !!e.value, nt.optSelected = n.selected, nt.enctype = !!gt.createElement("form").enctype, i.disabled = !0, nt.optDisabled = !n.disabled, e = gt.createElement("input"), e.setAttribute("value", ""), nt.input = "" === e.getAttribute("value"), e.value = "t", e.setAttribute("type", "radio"), nt.radioValue = "t" === e.value, t = e = i = n = o = null
    }();
  var be = /\r/g;
  st.fn.extend({
    val: function (t) {
      var e, i, n, o = this[0];
      return arguments.length ? (n = st.isFunction(t), this.each(function (i) {
        var o;
        1 === this.nodeType && (o = n ? t.call(this, i, st(this).val()) : t, null == o ? o = "" : "number" == typeof o ? o += "" : st.isArray(o) && (o = st.map(o, function (t) {
          return null == t ? "" : t + ""
        })), e = st.valHooks[this.type] || st.valHooks[this.nodeName.toLowerCase()], e && "set" in e && void 0 !== e.set(this, o, "value") || (this.value = o))
      })) : o ? (e = st.valHooks[o.type] || st.valHooks[o.nodeName.toLowerCase()], e && "get" in e && void 0 !== (i = e.get(o, "value")) ? i : (i = o.value, "string" == typeof i ? i.replace(be, "") : null == i ? "" : i)) : void 0
    }
  }), st.extend({
    valHooks: {
      option: {
        get: function (t) {
          var e = st.find.attr(t, "value");
          return null != e ? e : st.text(t)
        }
      },
      select: {
        get: function (t) {
          for (var e, i, n = t.options, o = t.selectedIndex, s = "select-one" === t.type || 0 > o, r = s ? null : [], a = s ? o + 1 : n.length, l = 0 > o ? a : s ? o : 0; a > l; l++)
            if (i = n[l], !(!i.selected && l !== o || (nt.optDisabled ? i.disabled : null !== i.getAttribute("disabled")) || i.parentNode.disabled && st.nodeName(i.parentNode, "optgroup"))) {
              if (e = st(i).val(), s) return e;
              r.push(e)
            }
          return r
        },
        set: function (t, e) {
          for (var i, n, o = t.options, s = st.makeArray(e), r = o.length; r--;)
            if (n = o[r], st.inArray(st.valHooks.option.get(n), s) >= 0) try {
              n.selected = i = !0
            } catch (t) {
              n.scrollHeight
            } else n.selected = !1;
          return i || (t.selectedIndex = -1), o
        }
      }
    }
  }), st.each(["radio", "checkbox"], function () {
    st.valHooks[this] = {
      set: function (t, e) {
        return st.isArray(e) ? t.checked = st.inArray(st(t).val(), e) >= 0 : void 0
      }
    }, nt.checkOn || (st.valHooks[this].get = function (t) {
      return null === t.getAttribute("value") ? "on" : t.value
    })
  });
  var xe, ke, Ce = st.expr.attrHandle,
    $e = /^(?:checked|selected)$/i,
    Te = nt.getSetAttribute,
    Ee = nt.input;
  st.fn.extend({
    attr: function (t, e) {
      return At(this, st.attr, t, e, arguments.length > 1)
    },
    removeAttr: function (t) {
      return this.each(function () {
        st.removeAttr(this, t)
      })
    }
  }), st.extend({
    attr: function (t, e, i) {
      var n, o, s = t.nodeType;
      if (t && 3 !== s && 8 !== s && 2 !== s) return typeof t.getAttribute === Ct ? st.prop(t, e, i) : (1 === s && st.isXMLDoc(t) || (e = e.toLowerCase(), n = st.attrHooks[e] || (st.expr.match.bool.test(e) ? ke : xe)), void 0 === i ? n && "get" in n && null !== (o = n.get(t, e)) ? o : (o = st.find.attr(t, e), null == o ? void 0 : o) : null !== i ? n && "set" in n && void 0 !== (o = n.set(t, i, e)) ? o : (t.setAttribute(e, i + ""), i) : void st.removeAttr(t, e))
    },
    removeAttr: function (t, e) {
      var i, n, o = 0,
        s = e && e.match(wt);
      if (s && 1 === t.nodeType)
        for (; i = s[o++];) n = st.propFix[i] || i, st.expr.match.bool.test(i) ? Ee && Te || !$e.test(i) ? t[n] = !1 : t[st.camelCase("default-" + i)] = t[n] = !1 : st.attr(t, i, ""), t.removeAttribute(Te ? i : n)
    },
    attrHooks: {
      type: {
        set: function (t, e) {
          if (!nt.radioValue && "radio" === e && st.nodeName(t, "input")) {
            var i = t.value;
            return t.setAttribute("type", e), i && (t.value = i), e
          }
        }
      }
    }
  }), ke = {
    set: function (t, e, i) {
      return e === !1 ? st.removeAttr(t, i) : Ee && Te || !$e.test(i) ? t.setAttribute(!Te && st.propFix[i] || i, i) : t[st.camelCase("default-" + i)] = t[i] = !0, i
    }
  }, st.each(st.expr.match.bool.source.match(/\w+/g), function (t, e) {
    var i = Ce[e] || st.find.attr;
    Ce[e] = Ee && Te || !$e.test(e) ? function (t, e, n) {
      var o, s;
      return n || (s = Ce[e], Ce[e] = o, o = null != i(t, e, n) ? e.toLowerCase() : null, Ce[e] = s), o
    } : function (t, e, i) {
      return i ? void 0 : t[st.camelCase("default-" + e)] ? e.toLowerCase() : null
    }
  }), Ee && Te || (st.attrHooks.value = {
    set: function (t, e, i) {
      return st.nodeName(t, "input") ? void(t.defaultValue = e) : xe && xe.set(t, e, i)
    }
  }), Te || (xe = {
    set: function (t, e, i) {
      var n = t.getAttributeNode(i);
      return n || t.setAttributeNode(n = t.ownerDocument.createAttribute(i)), n.value = e += "", "value" === i || e === t.getAttribute(i) ? e : void 0
    }
  }, Ce.id = Ce.name = Ce.coords = function (t, e, i) {
    var n;
    return i ? void 0 : (n = t.getAttributeNode(e)) && "" !== n.value ? n.value : null
  }, st.valHooks.button = {
    get: function (t, e) {
      var i = t.getAttributeNode(e);
      return i && i.specified ? i.value : void 0
    },
    set: xe.set
  }, st.attrHooks.contenteditable = {
    set: function (t, e, i) {
      xe.set(t, "" !== e && e, i)
    }
  }, st.each(["width", "height"], function (t, e) {
    st.attrHooks[e] = {
      set: function (t, i) {
        return "" === i ? (t.setAttribute(e, "auto"), i) : void 0
      }
    }
  })), nt.style || (st.attrHooks.style = {
    get: function (t) {
      return t.style.cssText || void 0
    },
    set: function (t, e) {
      return t.style.cssText = e + ""
    }
  });
  var je = /^(?:input|select|textarea|button|object)$/i,
    Se = /^(?:a|area)$/i;
  st.fn.extend({
    prop: function (t, e) {
      return At(this, st.prop, t, e, arguments.length > 1)
    },
    removeProp: function (t) {
      return t = st.propFix[t] || t, this.each(function () {
        try {
          this[t] = void 0, delete this[t]
        } catch (t) {}
      })
    }
  }), st.extend({
    propFix: {
      for: "htmlFor",
      class: "className"
    },
    prop: function (t, e, i) {
      var n, o, s, r = t.nodeType;
      if (t && 3 !== r && 8 !== r && 2 !== r) return s = 1 !== r || !st.isXMLDoc(t), s && (e = st.propFix[e] || e, o = st.propHooks[e]), void 0 !== i ? o && "set" in o && void 0 !== (n = o.set(t, i, e)) ? n : t[e] = i : o && "get" in o && null !== (n = o.get(t, e)) ? n : t[e]
    },
    propHooks: {
      tabIndex: {
        get: function (t) {
          var e = st.find.attr(t, "tabindex");
          return e ? parseInt(e, 10) : je.test(t.nodeName) || Se.test(t.nodeName) && t.href ? 0 : -1
        }
      }
    }
  }), nt.hrefNormalized || st.each(["href", "src"], function (t, e) {
    st.propHooks[e] = {
      get: function (t) {
        return t.getAttribute(e, 4)
      }
    }
  }), nt.optSelected || (st.propHooks.selected = {
    get: function (t) {
      var e = t.parentNode;
      return e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex), null
    }
  }), st.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
    st.propFix[this.toLowerCase()] = this
  }), nt.enctype || (st.propFix.enctype = "encoding");
  var Ae = /[\t\r\n\f]/g;
  st.fn.extend({
    addClass: function (t) {
      var e, i, n, o, s, r, a = 0,
        l = this.length,
        c = "string" == typeof t && t;
      if (st.isFunction(t)) return this.each(function (e) {
        st(this).addClass(t.call(this, e, this.className))
      });
      if (c)
        for (e = (t || "").match(wt) || []; l > a; a++)
          if (i = this[a], n = 1 === i.nodeType && (i.className ? (" " + i.className + " ").replace(Ae, " ") : " ")) {
            for (s = 0; o = e[s++];) n.indexOf(" " + o + " ") < 0 && (n += o + " ");
            r = st.trim(n), i.className !== r && (i.className = r)
          }
      return this
    },
    removeClass: function (t) {
      var e, i, n, o, s, r, a = 0,
        l = this.length,
        c = 0 === arguments.length || "string" == typeof t && t;
      if (st.isFunction(t)) return this.each(function (e) {
        st(this).removeClass(t.call(this, e, this.className))
      });
      if (c)
        for (e = (t || "").match(wt) || []; l > a; a++)
          if (i = this[a], n = 1 === i.nodeType && (i.className ? (" " + i.className + " ").replace(Ae, " ") : "")) {
            for (s = 0; o = e[s++];)
              for (; n.indexOf(" " + o + " ") >= 0;) n = n.replace(" " + o + " ", " ");
            r = t ? st.trim(n) : "", i.className !== r && (i.className = r)
          }
      return this
    },
    toggleClass: function (t, e) {
      var i = typeof t;
      return "boolean" == typeof e && "string" === i ? e ? this.addClass(t) : this.removeClass(t) : this.each(st.isFunction(t) ? function (i) {
        st(this).toggleClass(t.call(this, i, this.className, e), e)
      } : function () {
        if ("string" === i)
          for (var e, n = 0, o = st(this), s = t.match(wt) || []; e = s[n++];) o.hasClass(e) ? o.removeClass(e) : o.addClass(e);
        else(i === Ct || "boolean" === i) && (this.className && st._data(this, "__className__", this.className), this.className = this.className || t === !1 ? "" : st._data(this, "__className__") || "")
      })
    },
    hasClass: function (t) {
      for (var e = " " + t + " ", i = 0, n = this.length; n > i; i++)
        if (1 === this[i].nodeType && (" " + this[i].className + " ").replace(Ae, " ").indexOf(e) >= 0) return !0;
      return !1
    }
  }), st.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (t, e) {
    st.fn[e] = function (t, i) {
      return arguments.length > 0 ? this.on(e, null, t, i) : this.trigger(e)
    }
  }), st.fn.extend({
    hover: function (t, e) {
      return this.mouseenter(t).mouseleave(e || t)
    },
    bind: function (t, e, i) {
      return this.on(t, null, e, i)
    },
    unbind: function (t, e) {
      return this.off(t, null, e)
    },
    delegate: function (t, e, i, n) {
      return this.on(e, t, i, n)
    },
    undelegate: function (t, e, i) {
      return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", i)
    }
  });
  var Pe = st.now(),
    De = /\?/,
    Oe = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
  st.parseJSON = function (e) {
    if (t.JSON && t.JSON.parse) return t.JSON.parse(e + "");
    var i, n = null,
      o = st.trim(e + "");
    return o && !st.trim(o.replace(Oe, function (t, e, o, s) {
      return i && e && (n = 0), 0 === n ? t : (i = o || e, n += !s - !o, "")
    })) ? Function("return " + o)() : st.error("Invalid JSON: " + e)
  }, st.parseXML = function (e) {
    var i, n;
    if (!e || "string" != typeof e) return null;
    try {
      t.DOMParser ? (n = new DOMParser, i = n.parseFromString(e, "text/xml")) : (i = new ActiveXObject("Microsoft.XMLDOM"), i.async = "false", i.loadXML(e))
    } catch (t) {
      i = void 0
    }
    return i && i.documentElement && !i.getElementsByTagName("parsererror").length || st.error("Invalid XML: " + e), i
  };
  var Ie, Ne, Me = /#.*$/,
    Le = /([?&])_=[^&]*/,
    ze = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
    He = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
    Re = /^(?:GET|HEAD)$/,
    Fe = /^\/\//,
    Be = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
    qe = {},
    We = {},
    Ge = "*/".concat("*");
  try {
    Ne = location.href
  } catch (t) {
    Ne = gt.createElement("a"), Ne.href = "", Ne = Ne.href
  }
  Ie = Be.exec(Ne.toLowerCase()) || [], st.extend({
    active: 0,
    lastModified: {},
    etag: {},
    ajaxSettings: {
      url: Ne,
      type: "GET",
      isLocal: He.test(Ie[1]),
      global: !0,
      processData: !0,
      async: !0,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      accepts: {
        "*": Ge,
        text: "text/plain",
        html: "text/html",
        xml: "application/xml, text/xml",
        json: "application/json, text/javascript"
      },
      contents: {
        xml: /xml/,
        html: /html/,
        json: /json/
      },
      responseFields: {
        xml: "responseXML",
        text: "responseText",
        json: "responseJSON"
      },
      converters: {
        "* text": String,
        "text html": !0,
        "text json": st.parseJSON,
        "text xml": st.parseXML
      },
      flatOptions: {
        url: !0,
        context: !0
      }
    },
    ajaxSetup: function (t, e) {
      return e ? F(F(t, st.ajaxSettings), e) : F(st.ajaxSettings, t)
    },
    ajaxPrefilter: H(qe),
    ajaxTransport: H(We),
    ajax: function (t, e) {
      function i(t, e, i, n) {
        var o, u, _, v, w, x = e;
        2 !== y && (y = 2, a && clearTimeout(a), c = void 0, r = n || "", b.readyState = t > 0 ? 4 : 0, o = t >= 200 && 300 > t || 304 === t, i && (v = B(h, b, i)), v = q(h, v, b, o), o ? (h.ifModified && (w = b.getResponseHeader("Last-Modified"), w && (st.lastModified[s] = w), w = b.getResponseHeader("etag"), w && (st.etag[s] = w)), 204 === t || "HEAD" === h.type ? x = "nocontent" : 304 === t ? x = "notmodified" : (x = v.state, u = v.data, _ = v.error, o = !_)) : (_ = x, (t || !x) && (x = "error", 0 > t && (t = 0))), b.status = t, b.statusText = (e || x) + "", o ? f.resolveWith(d, [u, x, b]) : f.rejectWith(d, [b, x, _]), b.statusCode(m), m = void 0, l && p.trigger(o ? "ajaxSuccess" : "ajaxError", [b, h, o ? u : _]), g.fireWith(d, [b, x]), l && (p.trigger("ajaxComplete", [b, h]), --st.active || st.event.trigger("ajaxStop")))
      }
      "object" == typeof t && (e = t, t = void 0), e = e || {};
      var n, o, s, r, a, l, c, u, h = st.ajaxSetup({}, e),
        d = h.context || h,
        p = h.context && (d.nodeType || d.jquery) ? st(d) : st.event,
        f = st.Deferred(),
        g = st.Callbacks("once memory"),
        m = h.statusCode || {},
        _ = {},
        v = {},
        y = 0,
        w = "canceled",
        b = {
          readyState: 0,
          getResponseHeader: function (t) {
            var e;
            if (2 === y) {
              if (!u)
                for (u = {}; e = ze.exec(r);) u[e[1].toLowerCase()] = e[2];
              e = u[t.toLowerCase()]
            }
            return null == e ? null : e
          },
          getAllResponseHeaders: function () {
            return 2 === y ? r : null
          },
          setRequestHeader: function (t, e) {
            var i = t.toLowerCase();
            return y || (t = v[i] = v[i] || t, _[t] = e), this
          },
          overrideMimeType: function (t) {
            return y || (h.mimeType = t), this
          },
          statusCode: function (t) {
            var e;
            if (t)
              if (2 > y)
                for (e in t) m[e] = [m[e], t[e]];
              else b.always(t[b.status]);
            return this
          },
          abort: function (t) {
            var e = t || w;
            return c && c.abort(e), i(0, e), this
          }
        };
      if (f.promise(b).complete = g.add, b.success = b.done, b.error = b.fail, h.url = ((t || h.url || Ne) + "").replace(Me, "").replace(Fe, Ie[1] + "//"), h.type = e.method || e.type || h.method || h.type, h.dataTypes = st.trim(h.dataType || "*").toLowerCase().match(wt) || [""], null == h.crossDomain && (n = Be.exec(h.url.toLowerCase()), h.crossDomain = !(!n || n[1] === Ie[1] && n[2] === Ie[2] && (n[3] || ("http:" === n[1] ? "80" : "443")) === (Ie[3] || ("http:" === Ie[1] ? "80" : "443")))), h.data && h.processData && "string" != typeof h.data && (h.data = st.param(h.data, h.traditional)), R(qe, h, e, b), 2 === y) return b;
      l = h.global, l && 0 === st.active++ && st.event.trigger("ajaxStart"), h.type = h.type.toUpperCase(), h.hasContent = !Re.test(h.type), s = h.url, h.hasContent || (h.data && (s = h.url += (De.test(s) ? "&" : "?") + h.data, delete h.data), h.cache === !1 && (h.url = Le.test(s) ? s.replace(Le, "$1_=" + Pe++) : s + (De.test(s) ? "&" : "?") + "_=" + Pe++)), h.ifModified && (st.lastModified[s] && b.setRequestHeader("If-Modified-Since", st.lastModified[s]), st.etag[s] && b.setRequestHeader("If-None-Match", st.etag[s])), (h.data && h.hasContent && h.contentType !== !1 || e.contentType) && b.setRequestHeader("Content-Type", h.contentType), b.setRequestHeader("Accept", h.dataTypes[0] && h.accepts[h.dataTypes[0]] ? h.accepts[h.dataTypes[0]] + ("*" !== h.dataTypes[0] ? ", " + Ge + "; q=0.01" : "") : h.accepts["*"]);
      for (o in h.headers) b.setRequestHeader(o, h.headers[o]);
      if (h.beforeSend && (h.beforeSend.call(d, b, h) === !1 || 2 === y)) return b.abort();
      w = "abort";
      for (o in {
          success: 1,
          error: 1,
          complete: 1
        }) b[o](h[o]);
      if (c = R(We, h, e, b)) {
        b.readyState = 1, l && p.trigger("ajaxSend", [b, h]), h.async && h.timeout > 0 && (a = setTimeout(function () {
          b.abort("timeout")
        }, h.timeout));
        try {
          y = 1, c.send(_, i)
        } catch (t) {
          if (!(2 > y)) throw t;
          i(-1, t)
        }
      } else i(-1, "No Transport");
      return b
    },
    getJSON: function (t, e, i) {
      return st.get(t, e, i, "json")
    },
    getScript: function (t, e) {
      return st.get(t, void 0, e, "script")
    }
  }), st.each(["get", "post"], function (t, e) {
    st[e] = function (t, i, n, o) {
      return st.isFunction(i) && (o = o || n, n = i, i = void 0), st.ajax({
        url: t,
        type: e,
        dataType: o,
        data: i,
        success: n
      })
    }
  }), st.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (t, e) {
    st.fn[e] = function (t) {
      return this.on(e, t)
    }
  }), st._evalUrl = function (t) {
    return st.ajax({
      url: t,
      type: "GET",
      dataType: "script",
      async: !1,
      global: !1,
      throws: !0
    })
  }, st.fn.extend({
    wrapAll: function (t) {
      if (st.isFunction(t)) return this.each(function (e) {
        st(this).wrapAll(t.call(this, e))
      });
      if (this[0]) {
        var e = st(t, this[0].ownerDocument).eq(0).clone(!0);
        this[0].parentNode && e.insertBefore(this[0]), e.map(function () {
          for (var t = this; t.firstChild && 1 === t.firstChild.nodeType;) t = t.firstChild;
          return t
        }).append(this)
      }
      return this
    },
    wrapInner: function (t) {
      return this.each(st.isFunction(t) ? function (e) {
        st(this).wrapInner(t.call(this, e))
      } : function () {
        var e = st(this),
          i = e.contents();
        i.length ? i.wrapAll(t) : e.append(t)
      })
    },
    wrap: function (t) {
      var e = st.isFunction(t);
      return this.each(function (i) {
        st(this).wrapAll(e ? t.call(this, i) : t)
      })
    },
    unwrap: function () {
      return this.parent().each(function () {
        st.nodeName(this, "body") || st(this).replaceWith(this.childNodes)
      }).end()
    }
  }), st.expr.filters.hidden = function (t) {
    return t.offsetWidth <= 0 && t.offsetHeight <= 0 || !nt.reliableHiddenOffsets() && "none" === (t.style && t.style.display || st.css(t, "display"))
  }, st.expr.filters.visible = function (t) {
    return !st.expr.filters.hidden(t)
  };
  var Xe = /%20/g,
    Ue = /\[\]$/,
    Qe = /\r?\n/g,
    Ve = /^(?:submit|button|image|reset|file)$/i,
    Ke = /^(?:input|select|textarea|keygen)/i;
  st.param = function (t, e) {
    var i, n = [],
      o = function (t, e) {
        e = st.isFunction(e) ? e() : null == e ? "" : e, n[n.length] = encodeURIComponent(t) + "=" + encodeURIComponent(e)
      };
    if (void 0 === e && (e = st.ajaxSettings && st.ajaxSettings.traditional), st.isArray(t) || t.jquery && !st.isPlainObject(t)) st.each(t, function () {
      o(this.name, this.value)
    });
    else
      for (i in t) W(i, t[i], e, o);
    return n.join("&").replace(Xe, "+")
  }, st.fn.extend({
    serialize: function () {
      return st.param(this.serializeArray())
    },
    serializeArray: function () {
      return this.map(function () {
        var t = st.prop(this, "elements");
        return t ? st.makeArray(t) : this
      }).filter(function () {
        var t = this.type;
        return this.name && !st(this).is(":disabled") && Ke.test(this.nodeName) && !Ve.test(t) && (this.checked || !Pt.test(t))
      }).map(function (t, e) {
        var i = st(this).val();
        return null == i ? null : st.isArray(i) ? st.map(i, function (t) {
          return {
            name: e.name,
            value: t.replace(Qe, "\r\n")
          }
        }) : {
          name: e.name,
          value: i.replace(Qe, "\r\n")
        }
      }).get()
    }
  }), st.ajaxSettings.xhr = void 0 !== t.ActiveXObject ? function () {
    return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && G() || X()
  } : G;
  var Ye = 0,
    Ze = {},
    Je = st.ajaxSettings.xhr();
  t.ActiveXObject && st(t).on("unload", function () {
    for (var t in Ze) Ze[t](void 0, !0)
  }), nt.cors = !!Je && "withCredentials" in Je, Je = nt.ajax = !!Je, Je && st.ajaxTransport(function (t) {
    if (!t.crossDomain || nt.cors) {
      var e;
      return {
        send: function (i, n) {
          var o, s = t.xhr(),
            r = ++Ye;
          if (s.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields)
            for (o in t.xhrFields) s[o] = t.xhrFields[o];
          t.mimeType && s.overrideMimeType && s.overrideMimeType(t.mimeType), t.crossDomain || i["X-Requested-With"] || (i["X-Requested-With"] = "XMLHttpRequest");
          for (o in i) void 0 !== i[o] && s.setRequestHeader(o, i[o] + "");
          s.send(t.hasContent && t.data || null), e = function (i, o) {
            var a, l, c;
            if (e && (o || 4 === s.readyState))
              if (delete Ze[r], e = void 0, s.onreadystatechange = st.noop, o) 4 !== s.readyState && s.abort();
              else {
                c = {}, a = s.status, "string" == typeof s.responseText && (c.text = s.responseText);
                try {
                  l = s.statusText
                } catch (t) {
                  l = ""
                }
                a || !t.isLocal || t.crossDomain ? 1223 === a && (a = 204) : a = c.text ? 200 : 404
              }
            c && n(a, l, c, s.getAllResponseHeaders())
          }, t.async ? 4 === s.readyState ? setTimeout(e) : s.onreadystatechange = Ze[r] = e : e()
        },
        abort: function () {
          e && e(void 0, !0)
        }
      }
    }
  }), st.ajaxSetup({
    accepts: {
      script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
    },
    contents: {
      script: /(?:java|ecma)script/
    },
    converters: {
      "text script": function (t) {
        return st.globalEval(t), t
      }
    }
  }), st.ajaxPrefilter("script", function (t) {
    void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = "GET", t.global = !1)
  }), st.ajaxTransport("script", function (t) {
    if (t.crossDomain) {
      var e, i = gt.head || st("head")[0] || gt.documentElement;
      return {
        send: function (n, o) {
          e = gt.createElement("script"), e.async = !0, t.scriptCharset && (e.charset = t.scriptCharset), e.src = t.url, e.onload = e.onreadystatechange = function (t, i) {
            (i || !e.readyState || /loaded|complete/.test(e.readyState)) && (e.onload = e.onreadystatechange = null, e.parentNode && e.parentNode.removeChild(e), e = null, i || o(200, "success"))
          }, i.insertBefore(e, i.firstChild)
        },
        abort: function () {
          e && e.onload(void 0, !0)
        }
      }
    }
  });
  var ti = [],
    ei = /(=)\?(?=&|$)|\?\?/;
  st.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function () {
      var t = ti.pop() || st.expando + "_" + Pe++;
      return this[t] = !0, t
    }
  }), st.ajaxPrefilter("json jsonp", function (e, i, n) {
    var o, s, r, a = e.jsonp !== !1 && (ei.test(e.url) ? "url" : "string" == typeof e.data && !(e.contentType || "").indexOf("application/x-www-form-urlencoded") && ei.test(e.data) && "data");
    return a || "jsonp" === e.dataTypes[0] ? (o = e.jsonpCallback = st.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, a ? e[a] = e[a].replace(ei, "$1" + o) : e.jsonp !== !1 && (e.url += (De.test(e.url) ? "&" : "?") + e.jsonp + "=" + o), e.converters["script json"] = function () {
      return r || st.error(o + " was not called"), r[0]
    }, e.dataTypes[0] = "json", s = t[o], t[o] = function () {
      r = arguments
    }, n.always(function () {
      t[o] = s, e[o] && (e.jsonpCallback = i.jsonpCallback, ti.push(o)), r && st.isFunction(s) && s(r[0]), r = s = void 0
    }), "script") : void 0
  }), st.parseHTML = function (t, e, i) {
    if (!t || "string" != typeof t) return null;
    "boolean" == typeof e && (i = e, e = !1), e = e || gt;
    var n = dt.exec(t),
      o = !i && [];
    return n ? [e.createElement(n[1])] : (n = st.buildFragment([t], e, o), o && o.length && st(o).remove(), st.merge([], n.childNodes))
  };
  var ii = st.fn.load;
  st.fn.load = function (t, e, i) {
    if ("string" != typeof t && ii) return ii.apply(this, arguments);
    var n, o, s, r = this,
      a = t.indexOf(" ");
    return a >= 0 && (n = t.slice(a, t.length), t = t.slice(0, a)), st.isFunction(e) ? (i = e, e = void 0) : e && "object" == typeof e && (s = "POST"), r.length > 0 && st.ajax({
      url: t,
      type: s,
      dataType: "html",
      data: e
    }).done(function (t) {
      o = arguments, r.html(n ? st("<div>").append(st.parseHTML(t)).find(n) : t)
    }).complete(i && function (t, e) {
      r.each(i, o || [t.responseText, e, t])
    }), this
  }, st.expr.filters.animated = function (t) {
    return st.grep(st.timers, function (e) {
      return t === e.elem
    }).length
  };
  var ni = t.document.documentElement;
  st.offset = {
    setOffset: function (t, e, i) {
      var n, o, s, r, a, l, c, u = st.css(t, "position"),
        h = st(t),
        d = {};
      "static" === u && (t.style.position = "relative"), a = h.offset(), s = st.css(t, "top"), l = st.css(t, "left"), c = ("absolute" === u || "fixed" === u) && st.inArray("auto", [s, l]) > -1, c ? (n = h.position(), r = n.top, o = n.left) : (r = parseFloat(s) || 0, o = parseFloat(l) || 0), st.isFunction(e) && (e = e.call(t, i, a)), null != e.top && (d.top = e.top - a.top + r), null != e.left && (d.left = e.left - a.left + o), "using" in e ? e.using.call(t, d) : h.css(d)
    }
  }, st.fn.extend({
    offset: function (t) {
      if (arguments.length) return void 0 === t ? this : this.each(function (e) {
        st.offset.setOffset(this, t, e)
      });
      var e, i, n = {
          top: 0,
          left: 0
        },
        o = this[0],
        s = o && o.ownerDocument;
      return s ? (e = s.documentElement, st.contains(e, o) ? (typeof o.getBoundingClientRect !== Ct && (n = o.getBoundingClientRect()), i = U(s), {
        top: n.top + (i.pageYOffset || e.scrollTop) - (e.clientTop || 0),
        left: n.left + (i.pageXOffset || e.scrollLeft) - (e.clientLeft || 0)
      }) : n) : void 0
    },
    position: function () {
      if (this[0]) {
        var t, e, i = {
            top: 0,
            left: 0
          },
          n = this[0];
        return "fixed" === st.css(n, "position") ? e = n.getBoundingClientRect() : (t = this.offsetParent(), e = this.offset(), st.nodeName(t[0], "html") || (i = t.offset()), i.top += st.css(t[0], "borderTopWidth", !0), i.left += st.css(t[0], "borderLeftWidth", !0)), {
          top: e.top - i.top - st.css(n, "marginTop", !0),
          left: e.left - i.left - st.css(n, "marginLeft", !0)
        }
      }
    },
    offsetParent: function () {
      return this.map(function () {
        for (var t = this.offsetParent || ni; t && !st.nodeName(t, "html") && "static" === st.css(t, "position");) t = t.offsetParent;
        return t || ni
      })
    }
  }), st.each({
    scrollLeft: "pageXOffset",
    scrollTop: "pageYOffset"
  }, function (t, e) {
    var i = /Y/.test(e);
    st.fn[t] = function (n) {
      return At(this, function (t, n, o) {
        var s = U(t);
        return void 0 === o ? s ? e in s ? s[e] : s.document.documentElement[n] : t[n] : void(s ? s.scrollTo(i ? st(s).scrollLeft() : o, i ? o : st(s).scrollTop()) : t[n] = o)
      }, t, n, arguments.length, null)
    }
  }), st.each(["top", "left"], function (t, e) {
    st.cssHooks[e] = T(nt.pixelPosition, function (t, i) {
      return i ? (i = ie(t, e), oe.test(i) ? st(t).position()[e] + "px" : i) : void 0
    })
  }), st.each({
    Height: "height",
    Width: "width"
  }, function (t, e) {
    st.each({
      padding: "inner" + t,
      content: e,
      "": "outer" + t
    }, function (i, n) {
      st.fn[n] = function (n, o) {
        var s = arguments.length && (i || "boolean" != typeof n),
          r = i || (n === !0 || o === !0 ? "margin" : "border");
        return At(this, function (e, i, n) {
          var o;
          return st.isWindow(e) ? e.document.documentElement["client" + t] : 9 === e.nodeType ? (o = e.documentElement, Math.max(e.body["scroll" + t], o["scroll" + t], e.body["offset" + t], o["offset" + t], o["client" + t])) : void 0 === n ? st.css(e, i, r) : st.style(e, i, n, r)
        }, e, s ? n : void 0, s, null)
      }
    })
  }), st.fn.size = function () {
    return this.length
  }, st.fn.andSelf = st.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () {
    return st
  });
  var oi = t.jQuery,
    si = t.$;
  return st.noConflict = function (e) {
    return t.$ === st && (t.$ = si), e && t.jQuery === st && (t.jQuery = oi), st
  }, typeof e === Ct && (t.jQuery = t.$ = st), st
}), jQuery.easing.jswing = jQuery.easing.swing, jQuery.extend(jQuery.easing, {
    def: "easeOutQuad",
    swing: function (t, e, i, n, o) {
      return jQuery.easing[jQuery.easing.def](t, e, i, n, o)
    },
    easeInQuad: function (t, e, i, n, o) {
      return n * (e /= o) * e + i
    },
    easeOutQuad: function (t, e, i, n, o) {
      return -n * (e /= o) * (e - 2) + i
    },
    easeInOutQuad: function (t, e, i, n, o) {
      return (e /= o / 2) < 1 ? n / 2 * e * e + i : -n / 2 * (--e * (e - 2) - 1) + i
    },
    easeInCubic: function (t, e, i, n, o) {
      return n * (e /= o) * e * e + i
    },
    easeOutCubic: function (t, e, i, n, o) {
      return n * ((e = e / o - 1) * e * e + 1) + i
    },
    easeInOutCubic: function (t, e, i, n, o) {
      return (e /= o / 2) < 1 ? n / 2 * e * e * e + i : n / 2 * ((e -= 2) * e * e + 2) + i
    },
    easeInQuart: function (t, e, i, n, o) {
      return n * (e /= o) * e * e * e + i
    },
    easeOutQuart: function (t, e, i, n, o) {
      return -n * ((e = e / o - 1) * e * e * e - 1) + i
    },
    easeInOutQuart: function (t, e, i, n, o) {
      return (e /= o / 2) < 1 ? n / 2 * e * e * e * e + i : -n / 2 * ((e -= 2) * e * e * e - 2) + i
    },
    easeInQuint: function (t, e, i, n, o) {
      return n * (e /= o) * e * e * e * e + i
    },
    easeOutQuint: function (t, e, i, n, o) {
      return n * ((e = e / o - 1) * e * e * e * e + 1) + i
    },
    easeInOutQuint: function (t, e, i, n, o) {
      return (e /= o / 2) < 1 ? n / 2 * e * e * e * e * e + i : n / 2 * ((e -= 2) * e * e * e * e + 2) + i
    },
    easeInSine: function (t, e, i, n, o) {
      return -n * Math.cos(e / o * (Math.PI / 2)) + n + i
    },
    easeOutSine: function (t, e, i, n, o) {
      return n * Math.sin(e / o * (Math.PI / 2)) + i
    },
    easeInOutSine: function (t, e, i, n, o) {
      return -n / 2 * (Math.cos(Math.PI * e / o) - 1) + i
    },
    easeInExpo: function (t, e, i, n, o) {
      return 0 == e ? i : n * Math.pow(2, 10 * (e / o - 1)) + i
    },
    easeOutExpo: function (t, e, i, n, o) {
      return e == o ? i + n : n * (-Math.pow(2, -10 * e / o) + 1) + i
    },
    easeInOutExpo: function (t, e, i, n, o) {
      return 0 == e ? i : e == o ? i + n : (e /= o / 2) < 1 ? n / 2 * Math.pow(2, 10 * (e - 1)) + i : n / 2 * (-Math.pow(2, -10 * --e) + 2) + i
    },
    easeInCirc: function (t, e, i, n, o) {
      return -n * (Math.sqrt(1 - (e /= o) * e) - 1) + i
    },
    easeOutCirc: function (t, e, i, n, o) {
      return n * Math.sqrt(1 - (e = e / o - 1) * e) + i
    },
    easeInOutCirc: function (t, e, i, n, o) {
      return (e /= o / 2) < 1 ? -n / 2 * (Math.sqrt(1 - e * e) - 1) + i : n / 2 * (Math.sqrt(1 - (e -= 2) * e) + 1) + i
    },
    easeInElastic: function (t, e, i, n, o) {
      var s = 1.70158,
        r = 0,
        a = n;
      if (0 == e) return i;
      if (1 == (e /= o)) return i + n;
      if (r || (r = .3 * o), a < Math.abs(n)) {
        a = n;
        var s = r / 4
      } else var s = r / (2 * Math.PI) * Math.asin(n / a);
      return -(a * Math.pow(2, 10 * (e -= 1)) * Math.sin((e * o - s) * (2 * Math.PI) / r)) + i
    },
    easeOutElastic: function (t, e, i, n, o) {
      var s = 1.70158,
        r = 0,
        a = n;
      if (0 == e) return i;
      if (1 == (e /= o)) return i + n;
      if (r || (r = .3 * o), a < Math.abs(n)) {
        a = n;
        var s = r / 4
      } else var s = r / (2 * Math.PI) * Math.asin(n / a);
      return a * Math.pow(2, -10 * e) * Math.sin((e * o - s) * (2 * Math.PI) / r) + n + i
    },
    easeInOutElastic: function (t, e, i, n, o) {
      var s = 1.70158,
        r = 0,
        a = n;
      if (0 == e) return i;
      if (2 == (e /= o / 2)) return i + n;
      if (r || (r = o * (.3 * 1.5)), a < Math.abs(n)) {
        a = n;
        var s = r / 4
      } else var s = r / (2 * Math.PI) * Math.asin(n / a);
      return e < 1 ? -.5 * (a * Math.pow(2, 10 * (e -= 1)) * Math.sin((e * o - s) * (2 * Math.PI) / r)) + i : a * Math.pow(2, -10 * (e -= 1)) * Math.sin((e * o - s) * (2 * Math.PI) / r) * .5 + n + i
    },
    easeInBack: function (t, e, i, n, o, s) {
      return void 0 == s && (s = 1.70158), n * (e /= o) * e * ((s + 1) * e - s) + i
    },
    easeOutBack: function (t, e, i, n, o, s) {
      return void 0 == s && (s = 1.70158), n * ((e = e / o - 1) * e * ((s + 1) * e + s) + 1) + i
    },
    easeInOutBack: function (t, e, i, n, o, s) {
      return void 0 == s && (s = 1.70158), (e /= o / 2) < 1 ? n / 2 * (e * e * (((s *= 1.525) + 1) * e - s)) + i : n / 2 * ((e -= 2) * e * (((s *= 1.525) + 1) * e + s) + 2) + i
    },
    easeInBounce: function (t, e, i, n, o) {
      return n - jQuery.easing.easeOutBounce(t, o - e, 0, n, o) + i
    },
    easeOutBounce: function (t, e, i, n, o) {
      return (e /= o) < 1 / 2.75 ? n * (7.5625 * e * e) + i : e < 2 / 2.75 ? n * (7.5625 * (e -= 1.5 / 2.75) * e + .75) + i : e < 2.5 / 2.75 ? n * (7.5625 * (e -= 2.25 / 2.75) * e + .9375) + i : n * (7.5625 * (e -= 2.625 / 2.75) * e + .984375) + i
    },
    easeInOutBounce: function (t, e, i, n, o) {
      return e < o / 2 ? .5 * jQuery.easing.easeInBounce(t, 2 * e, 0, n, o) + i : .5 * jQuery.easing.easeOutBounce(t, 2 * e - o, 0, n, o) + .5 * n + i
    }
  }), ! function (t, e) {
    "function" == typeof define && define.amd ? define(["jquery"], function (t) {
      return e(t)
    }) : "object" == typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
  }(this, function (t) {
    var e = function (t, e) {
        var i, n = document.createElement("canvas");
        t.appendChild(n), "object" == typeof G_vmlCanvasManager && G_vmlCanvasManager.initElement(n);
        var o = n.getContext("2d");
        n.width = n.height = e.size;
        var s = 1;
        window.devicePixelRatio > 1 && (s = window.devicePixelRatio, n.style.width = n.style.height = [e.size, "px"].join(""), n.width = n.height = e.size * s, o.scale(s, s)), o.translate(e.size / 2, e.size / 2), o.rotate((-.5 + e.rotate / 180) * Math.PI);
        var r = (e.size - e.lineWidth) / 2;
        e.scaleColor && e.scaleLength && (r -= e.scaleLength + 2), Date.now = Date.now || function () {
          return +new Date
        };
        var a = function (t, e, i) {
            i = Math.min(Math.max(-1, i || 0), 1);
            var n = 0 >= i;
            o.beginPath(), o.arc(0, 0, r, 0, 2 * Math.PI * i, n), o.strokeStyle = t, o.lineWidth = e, o.stroke()
          },
          l = function () {
            var t, i;
            o.lineWidth = 1, o.fillStyle = e.scaleColor, o.save();
            for (var n = 24; n > 0; --n) n % 6 === 0 ? (i = e.scaleLength, t = 0) : (i = .6 * e.scaleLength, t = e.scaleLength - i), o.fillRect(-e.size / 2 + t, 0, i, 1), o.rotate(Math.PI / 12);
            o.restore()
          },
          c = function () {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (t) {
              window.setTimeout(t, 1e3 / 60)
            }
          }(),
          u = function () {
            e.scaleColor && l(), e.trackColor && a(e.trackColor, e.trackWidth || e.lineWidth, 1)
          };
        this.getCanvas = function () {
          return n
        }, this.getCtx = function () {
          return o
        }, this.clear = function () {
          o.clearRect(e.size / -2, e.size / -2, e.size, e.size)
        }, this.draw = function (t) {
          e.scaleColor || e.trackColor ? o.getImageData && o.putImageData ? i ? o.putImageData(i, 0, 0) : (u(), i = o.getImageData(0, 0, e.size * s, e.size * s)) : (this.clear(), u()) : this.clear(), o.lineCap = e.lineCap;
          var n;
          n = "function" == typeof e.barColor ? e.barColor(t) : e.barColor, a(n, e.lineWidth, t / 100)
        }.bind(this), this.animate = function (t, i) {
          var n = Date.now();
          e.onStart(t, i);
          var o = function () {
            var s = Math.min(Date.now() - n, e.animate.duration),
              r = e.easing(this, s, t, i - t, e.animate.duration);
            this.draw(r), e.onStep(t, i, r), s >= e.animate.duration ? e.onStop(t, i) : c(o)
          }.bind(this);
          c(o)
        }.bind(this)
      },
      i = function (t, i) {
        var n = {
          barColor: "#ef1e25",
          trackColor: "#f9f9f9",
          scaleColor: "#dfe0e0",
          scaleLength: 5,
          lineCap: "round",
          lineWidth: 3,
          trackWidth: void 0,
          size: 110,
          rotate: 0,
          animate: {
            duration: 1e3,
            enabled: !0
          },
          easing: function (t, e, i, n, o) {
            return e /= o / 2, 1 > e ? n / 2 * e * e + i : -n / 2 * (--e * (e - 2) - 1) + i
          },
          onStart: function (t, e) {},
          onStep: function (t, e, i) {},
          onStop: function (t, e) {}
        };
        if ("undefined" != typeof e) n.renderer = e;
        else {
          if ("undefined" == typeof SVGRenderer) throw new Error("Please load either the SVG- or the CanvasRenderer");
          n.renderer = SVGRenderer
        }
        var o = {},
          s = 0,
          r = function () {
            this.el = t, this.options = o;
            for (var e in n) n.hasOwnProperty(e) && (o[e] = i && "undefined" != typeof i[e] ? i[e] : n[e], "function" == typeof o[e] && (o[e] = o[e].bind(this)));
            "string" == typeof o.easing && "undefined" != typeof jQuery && jQuery.isFunction(jQuery.easing[o.easing]) ? o.easing = jQuery.easing[o.easing] : o.easing = n.easing, "number" == typeof o.animate && (o.animate = {
              duration: o.animate,
              enabled: !0
            }), "boolean" != typeof o.animate || o.animate || (o.animate = {
              duration: 1e3,
              enabled: o.animate
            }), this.renderer = new o.renderer(t, o), this.renderer.draw(s), t.dataset && t.dataset.percent ? this.update(parseFloat(t.dataset.percent)) : t.getAttribute && t.getAttribute("data-percent") && this.update(parseFloat(t.getAttribute("data-percent")))
          }.bind(this);
        this.update = function (t) {
          return t = parseFloat(t), o.animate.enabled ? this.renderer.animate(s, t) : this.renderer.draw(t), s = t, this
        }.bind(this), this.disableAnimation = function () {
          return o.animate.enabled = !1, this
        }, this.enableAnimation = function () {
          return o.animate.enabled = !0, this
        }, r()
      };
    t.fn.easyPieChart = function (e) {
      return this.each(function () {
        var n;
        t.data(this, "easyPieChart") || (n = t.extend({}, e, t(this).data()), t.data(this, "easyPieChart", new i(this, n)))
      })
    }
  }),
  function (t, e, i, n) {
    function o(e, i) {
      this.settings = null, this.options = t.extend({}, o.Defaults, i), this.$element = t(e), this.drag = t.extend({}, d), this.state = t.extend({}, p), this.e = t.extend({}, f), this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._invalidated = {}, this._pipe = [], t.each(o.Plugins, t.proxy(function (t, e) {
        this._plugins[t[0].toLowerCase() + t.slice(1)] = new e(this)
      }, this)), t.each(o.Pipe, t.proxy(function (e, i) {
        this._pipe.push({
          filter: i.filter,
          run: t.proxy(i.run, this)
        })
      }, this)), this.setup(), this.initialize()
    }

    function s(t) {
      if (t.touches !== n) return {
        x: t.touches[0].pageX,
        y: t.touches[0].pageY
      };
      if (t.touches === n) {
        if (t.pageX !== n) return {
          x: t.pageX,
          y: t.pageY
        };
        if (t.pageX === n) return {
          x: t.clientX,
          y: t.clientY
        }
      }
    }

    function r(t) {
      var e, n, o = i.createElement("div"),
        s = t;
      for (e in s)
        if (n = s[e], "undefined" != typeof o.style[n]) return o = null, [n, e];
      return [!1]
    }

    function a() {
      return r(["transition", "WebkitTransition", "MozTransition", "OTransition"])[1]
    }

    function l() {
      return r(["transform", "WebkitTransform", "MozTransform", "OTransform", "msTransform"])[0]
    }

    function c() {
      return r(["perspective", "webkitPerspective", "MozPerspective", "OPerspective", "MsPerspective"])[0]
    }

    function u() {
      return "ontouchstart" in e || !!navigator.msMaxTouchPoints
    }

    function h() {
      return e.navigator.msPointerEnabled
    }
    var d, p, f;
    d = {
      start: 0,
      startX: 0,
      startY: 0,
      current: 0,
      currentX: 0,
      currentY: 0,
      offsetX: 0,
      offsetY: 0,
      distance: null,
      startTime: 0,
      endTime: 0,
      updatedX: 0,
      targetEl: null
    }, p = {
      isTouch: !1,
      isScrolling: !1,
      isSwiping: !1,
      direction: !1,
      inMotion: !1
    }, f = {
      _onDragStart: null,
      _onDragMove: null,
      _onDragEnd: null,
      _transitionEnd: null,
      _resizer: null,
      _responsiveCall: null,
      _goToLoop: null,
      _checkVisibile: null
    }, o.Defaults = {
      items: 3,
      loop: !1,
      center: !1,
      mouseDrag: !0,
      touchDrag: !0,
      pullDrag: !0,
      freeDrag: !1,
      margin: 0,
      stagePadding: 0,
      merge: !1,
      mergeFit: !0,
      autoWidth: !1,
      startPosition: 0,
      rtl: !1,
      smartSpeed: 250,
      fluidSpeed: !1,
      dragEndSpeed: !1,
      responsive: {},
      responsiveRefreshRate: 200,
      responsiveBaseElement: e,
      responsiveClass: !1,
      fallbackEasing: "swing",
      info: !1,
      nestedItemSelector: !1,
      itemElement: "div",
      stageElement: "div",
      themeClass: "owl-theme",
      baseClass: "owl-carousel",
      itemClass: "owl-item",
      centerClass: "center",
      activeClass: "active"
    }, o.Width = {
      Default: "default",
      Inner: "inner",
      Outer: "outer"
    }, o.Plugins = {}, o.Pipe = [{
      filter: ["width", "items", "settings"],
      run: function (t) {
        t.current = this._items && this._items[this.relative(this._current)]
      }
    }, {
      filter: ["items", "settings"],
      run: function () {
        var t = this._clones,
          e = this.$stage.children(".cloned");
        (e.length !== t.length || !this.settings.loop && t.length > 0) && (this.$stage.children(".cloned").remove(), this._clones = [])
      }
    }, {
      filter: ["items", "settings"],
      run: function () {
        var t, e, i = this._clones,
          n = this._items,
          o = this.settings.loop ? i.length - Math.max(2 * this.settings.items, 4) : 0;
        for (t = 0, e = Math.abs(o / 2); t < e; t++) o > 0 ? (this.$stage.children().eq(n.length + i.length - 1).remove(), i.pop(), this.$stage.children().eq(0).remove(), i.pop()) : (i.push(i.length / 2), this.$stage.append(n[i[i.length - 1]].clone().addClass("cloned")), i.push(n.length - 1 - (i.length - 1) / 2), this.$stage.prepend(n[i[i.length - 1]].clone().addClass("cloned")))
      }
    }, {
      filter: ["width", "items", "settings"],
      run: function () {
        var t, e, i, n = this.settings.rtl ? 1 : -1,
          o = (this.width() / this.settings.items).toFixed(3),
          s = 0;
        for (this._coordinates = [], e = 0, i = this._clones.length + this._items.length; e < i; e++) t = this._mergers[this.relative(e)], t = this.settings.mergeFit && Math.min(t, this.settings.items) || t, s += (this.settings.autoWidth ? this._items[this.relative(e)].width() + this.settings.margin : o * t) * n, this._coordinates.push(s)
      }
    }, {
      filter: ["width", "items", "settings"],
      run: function () {
        var e, i, n = (this.width() / this.settings.items).toFixed(3),
          o = {
            width: Math.abs(this._coordinates[this._coordinates.length - 1]) + 2 * this.settings.stagePadding,
            "padding-left": this.settings.stagePadding || "",
            "padding-right": this.settings.stagePadding || ""
          };
        if (this.$stage.css(o), o = {
            width: this.settings.autoWidth ? "auto" : n - this.settings.margin
          }, o[this.settings.rtl ? "margin-left" : "margin-right"] = this.settings.margin, !this.settings.autoWidth && t.grep(this._mergers, function (t) {
            return t > 1
          }).length > 0)
          for (e = 0, i = this._coordinates.length; e < i; e++) o.width = Math.abs(this._coordinates[e]) - Math.abs(this._coordinates[e - 1] || 0) - this.settings.margin, this.$stage.children().eq(e).css(o);
        else this.$stage.children().css(o)
      }
    }, {
      filter: ["width", "items", "settings"],
      run: function (t) {
        t.current && this.reset(this.$stage.children().index(t.current))
      }
    }, {
      filter: ["position"],
      run: function () {
        this.animate(this.coordinates(this._current))
      }
    }, {
      filter: ["width", "position", "items", "settings"],
      run: function () {
        var t, e, i, n, o = this.settings.rtl ? 1 : -1,
          s = 2 * this.settings.stagePadding,
          r = this.coordinates(this.current()) + s,
          a = r + this.width() * o,
          l = [];
        for (i = 0, n = this._coordinates.length; i < n; i++) t = this._coordinates[i - 1] || 0, e = Math.abs(this._coordinates[i]) + s * o, (this.op(t, "<=", r) && this.op(t, ">", a) || this.op(e, "<", r) && this.op(e, ">", a)) && l.push(i);
        this.$stage.children("." + this.settings.activeClass).removeClass(this.settings.activeClass), this.$stage.children(":eq(" + l.join("), :eq(") + ")").addClass(this.settings.activeClass), this.settings.center && (this.$stage.children("." + this.settings.centerClass).removeClass(this.settings.centerClass), this.$stage.children().eq(this.current()).addClass(this.settings.centerClass))
      }
    }], o.prototype.initialize = function () {
      if (this.trigger("initialize"), this.$element.addClass(this.settings.baseClass).addClass(this.settings.themeClass).toggleClass("owl-rtl", this.settings.rtl), this.browserSupport(), this.settings.autoWidth && this.state.imagesLoaded !== !0) {
        var e, i, o;
        if (e = this.$element.find("img"), i = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : n, o = this.$element.children(i).width(), e.length && o <= 0) return this.preloadAutoWidthImages(e), !1
      }
      this.$element.addClass("owl-loading"), this.$stage = t("<" + this.settings.stageElement + ' class="owl-stage"/>').wrap('<div class="owl-stage-outer">'), this.$element.append(this.$stage.parent()), this.replace(this.$element.children().not(this.$stage.parent())), this._width = this.$element.width(), this.refresh(), this.$element.removeClass("owl-loading").addClass("owl-loaded"), this.eventsCall(), this.internalEvents(), this.addTriggerableEvents(), this.trigger("initialized")
    }, o.prototype.setup = function () {
      var e = this.viewport(),
        i = this.options.responsive,
        n = -1,
        o = null;
      i ? (t.each(i, function (t) {
        t <= e && t > n && (n = Number(t))
      }), o = t.extend({}, this.options, i[n]), delete o.responsive, o.responsiveClass && this.$element.attr("class", function (t, e) {
        return e.replace(/\b owl-responsive-\S+/g, "")
      }).addClass("owl-responsive-" + n)) : o = t.extend({}, this.options), null !== this.settings && this._breakpoint === n || (this.trigger("change", {
        property: {
          name: "settings",
          value: o
        }
      }), this._breakpoint = n, this.settings = o, this.invalidate("settings"), this.trigger("changed", {
        property: {
          name: "settings",
          value: this.settings
        }
      }))
    }, o.prototype.optionsLogic = function () {
      this.$element.toggleClass("owl-center", this.settings.center), this.settings.loop && this._items.length < this.settings.items && (this.settings.loop = !1), this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1)
    }, o.prototype.prepare = function (e) {
      var i = this.trigger("prepare", {
        content: e
      });
      return i.data || (i.data = t("<" + this.settings.itemElement + "/>").addClass(this.settings.itemClass).append(e)), this.trigger("prepared", {
        content: i.data
      }), i.data
    }, o.prototype.update = function () {
      for (var e = 0, i = this._pipe.length, n = t.proxy(function (t) {
          return this[t]
        }, this._invalidated), o = {}; e < i;)(this._invalidated.all || t.grep(this._pipe[e].filter, n).length > 0) && this._pipe[e].run(o), e++;
      this._invalidated = {}
    }, o.prototype.width = function (t) {
      switch (t = t || o.Width.Default) {
        case o.Width.Inner:
        case o.Width.Outer:
          return this._width;
        default:
          return this._width - 2 * this.settings.stagePadding + this.settings.margin
      }
    }, o.prototype.refresh = function () {
      if (0 === this._items.length) return !1;
      (new Date).getTime();
      this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$stage.addClass("owl-refresh"), this.update(), this.$stage.removeClass("owl-refresh"), this.state.orientation = e.orientation, this.watchVisibility(), this.trigger("refreshed")
    }, o.prototype.eventsCall = function () {
      this.e._onDragStart = t.proxy(function (t) {
        this.onDragStart(t)
      }, this), this.e._onDragMove = t.proxy(function (t) {
        this.onDragMove(t)
      }, this), this.e._onDragEnd = t.proxy(function (t) {
        this.onDragEnd(t)
      }, this), this.e._onResize = t.proxy(function (t) {
        this.onResize(t)
      }, this), this.e._transitionEnd = t.proxy(function (t) {
        this.transitionEnd(t)
      }, this), this.e._preventClick = t.proxy(function (t) {
        this.preventClick(t)
      }, this)
    }, o.prototype.onThrottledResize = function () {
      e.clearTimeout(this.resizeTimer), this.resizeTimer = e.setTimeout(this.e._onResize, this.settings.responsiveRefreshRate)
    }, o.prototype.onResize = function () {
      return !!this._items.length && (this._width !== this.$element.width() && (!this.trigger("resize").isDefaultPrevented() && (this._width = this.$element.width(), this.invalidate("width"), this.refresh(), void this.trigger("resized"))))
    }, o.prototype.eventsRouter = function (t) {
      var e = t.type;
      "mousedown" === e || "touchstart" === e ? this.onDragStart(t) : "mousemove" === e || "touchmove" === e ? this.onDragMove(t) : "mouseup" === e || "touchend" === e ? this.onDragEnd(t) : "touchcancel" === e && this.onDragEnd(t)
    }, o.prototype.internalEvents = function () {
      var i = (u(), h());
      this.settings.mouseDrag ? (this.$stage.on("mousedown", t.proxy(function (t) {
        this.eventsRouter(t)
      }, this)), this.$stage.on("dragstart", function () {
        return !1
      }), this.$stage.get(0).onselectstart = function () {
        return !1
      }) : this.$element.addClass("owl-text-select-on"), this.settings.touchDrag && !i && this.$stage.on("touchstart touchcancel", t.proxy(function (t) {
        this.eventsRouter(t)
      }, this)), this.transitionEndVendor && this.on(this.$stage.get(0), this.transitionEndVendor, this.e._transitionEnd, !1), this.settings.responsive !== !1 && this.on(e, "resize", t.proxy(this.onThrottledResize, this))
    }, o.prototype.onDragStart = function (n) {
      var o, r, a, l;
      if (o = n.originalEvent || n || e.event, 3 === o.which || this.state.isTouch) return !1;
      if ("mousedown" === o.type && this.$stage.addClass("owl-grab"), this.trigger("drag"), this.drag.startTime = (new Date).getTime(), this.speed(0), this.state.isTouch = !0, this.state.isScrolling = !1, this.state.isSwiping = !1, this.drag.distance = 0, r = s(o).x, a = s(o).y, this.drag.offsetX = this.$stage.position().left, this.drag.offsetY = this.$stage.position().top, this.settings.rtl && (this.drag.offsetX = this.$stage.position().left + this.$stage.width() - this.width() + this.settings.margin), this.state.inMotion && this.support3d) l = this.getTransformProperty(), this.drag.offsetX = l, this.animate(l), this.state.inMotion = !0;
      else if (this.state.inMotion && !this.support3d) return this.state.inMotion = !1, !1;
      this.drag.startX = r - this.drag.offsetX, this.drag.startY = a - this.drag.offsetY, this.drag.start = r - this.drag.startX, this.drag.targetEl = o.target || o.srcElement, this.drag.updatedX = this.drag.start, "IMG" !== this.drag.targetEl.tagName && "A" !== this.drag.targetEl.tagName || (this.drag.targetEl.draggable = !1), t(i).on("mousemove.owl.dragEvents mouseup.owl.dragEvents touchmove.owl.dragEvents touchend.owl.dragEvents", t.proxy(function (t) {
        this.eventsRouter(t)
      }, this))
    }, o.prototype.onDragMove = function (t) {
      var i, o, r, a, l, c;
      this.state.isTouch && (this.state.isScrolling || (i = t.originalEvent || t || e.event, o = s(i).x, r = s(i).y, this.drag.currentX = o - this.drag.startX, this.drag.currentY = r - this.drag.startY, this.drag.distance = this.drag.currentX - this.drag.offsetX, this.drag.distance < 0 ? this.state.direction = this.settings.rtl ? "right" : "left" : this.drag.distance > 0 && (this.state.direction = this.settings.rtl ? "left" : "right"), this.settings.loop ? this.op(this.drag.currentX, ">", this.coordinates(this.minimum())) && "right" === this.state.direction ? this.drag.currentX -= (this.settings.center && this.coordinates(0)) - this.coordinates(this._items.length) : this.op(this.drag.currentX, "<", this.coordinates(this.maximum())) && "left" === this.state.direction && (this.drag.currentX += (this.settings.center && this.coordinates(0)) - this.coordinates(this._items.length)) : (a = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), l = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), c = this.settings.pullDrag ? this.drag.distance / 5 : 0, this.drag.currentX = Math.max(Math.min(this.drag.currentX, a + c), l + c)), (this.drag.distance > 8 || this.drag.distance < -8) && (i.preventDefault !== n ? i.preventDefault() : i.returnValue = !1, this.state.isSwiping = !0), this.drag.updatedX = this.drag.currentX, (this.drag.currentY > 16 || this.drag.currentY < -16) && this.state.isSwiping === !1 && (this.state.isScrolling = !0, this.drag.updatedX = this.drag.start), this.animate(this.drag.updatedX)))
    }, o.prototype.onDragEnd = function (e) {
      var n, o, s;
      if (this.state.isTouch) {
        if ("mouseup" === e.type && this.$stage.removeClass("owl-grab"), this.trigger("dragged"), this.drag.targetEl.removeAttribute("draggable"), this.state.isTouch = !1, this.state.isScrolling = !1, this.state.isSwiping = !1, 0 === this.drag.distance && this.state.inMotion !== !0) return this.state.inMotion = !1, !1;
        this.drag.endTime = (new Date).getTime(), n = this.drag.endTime - this.drag.startTime, o = Math.abs(this.drag.distance), (o > 3 || n > 300) && this.removeClick(this.drag.targetEl), s = this.closest(this.drag.updatedX), this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(s), this.invalidate("position"), this.update(), this.settings.pullDrag || this.drag.updatedX !== this.coordinates(s) || this.transitionEnd(), this.drag.distance = 0, t(i).off(".owl.dragEvents")
      }
    }, o.prototype.removeClick = function (i) {
      this.drag.targetEl = i, t(i).on("click.preventClick", this.e._preventClick), e.setTimeout(function () {
        t(i).off("click.preventClick")
      }, 300)
    }, o.prototype.preventClick = function (e) {
      e.preventDefault ? e.preventDefault() : e.returnValue = !1, e.stopPropagation && e.stopPropagation(), t(e.target).off("click.preventClick")
    }, o.prototype.getTransformProperty = function () {
      var t, i;
      return t = e.getComputedStyle(this.$stage.get(0), null).getPropertyValue(this.vendorName + "transform"), t = t.replace(/matrix(3d)?\(|\)/g, "").split(","), i = 16 === t.length, i !== !0 ? t[4] : t[12]
    }, o.prototype.closest = function (e) {
      var i = -1,
        n = 30,
        o = this.width(),
        s = this.coordinates();
      return this.settings.freeDrag || t.each(s, t.proxy(function (t, r) {
        return e > r - n && e < r + n ? i = t : this.op(e, "<", r) && this.op(e, ">", s[t + 1] || r - o) && (i = "left" === this.state.direction ? t + 1 : t), i === -1
      }, this)), this.settings.loop || (this.op(e, ">", s[this.minimum()]) ? i = e = this.minimum() : this.op(e, "<", s[this.maximum()]) && (i = e = this.maximum())), i
    }, o.prototype.animate = function (e) {
      this.trigger("translate"), this.state.inMotion = this.speed() > 0, this.support3d ? this.$stage.css({
        transform: "translate3d(" + e + "px,0px, 0px)",
        transition: this.speed() / 1e3 + "s"
      }) : this.state.isTouch ? this.$stage.css({
        left: e + "px"
      }) : this.$stage.animate({
        left: e
      }, this.speed() / 1e3, this.settings.fallbackEasing, t.proxy(function () {
        this.state.inMotion && this.transitionEnd()
      }, this))
    }, o.prototype.current = function (t) {
      if (t === n) return this._current;
      if (0 === this._items.length) return n;
      if (t = this.normalize(t), this._current !== t) {
        var e = this.trigger("change", {
          property: {
            name: "position",
            value: t
          }
        });
        e.data !== n && (t = this.normalize(e.data)), this._current = t, this.invalidate("position"), this.trigger("changed", {
          property: {
            name: "position",
            value: this._current
          }
        })
      }
      return this._current
    }, o.prototype.invalidate = function (t) {
      this._invalidated[t] = !0
    }, o.prototype.reset = function (t) {
      t = this.normalize(t), t !== n && (this._speed = 0, this._current = t, this.suppress(["translate", "translated"]), this.animate(this.coordinates(t)), this.release(["translate", "translated"]))
    }, o.prototype.normalize = function (e, i) {
      var o = i ? this._items.length : this._items.length + this._clones.length;
      return !t.isNumeric(e) || o < 1 ? n : e = this._clones.length ? (e % o + o) % o : Math.max(this.minimum(i), Math.min(this.maximum(i), e))
    }, o.prototype.relative = function (t) {
      return t = this.normalize(t), t -= this._clones.length / 2, this.normalize(t, !0)
    }, o.prototype.maximum = function (t) {
      var e, i, n, o = 0,
        s = this.settings;
      if (t) return this._items.length - 1;
      if (!s.loop && s.center) e = this._items.length - 1;
      else if (s.loop || s.center)
        if (s.loop || s.center) e = this._items.length + s.items;
        else {
          if (!s.autoWidth && !s.merge) throw "Can not detect maximum absolute position.";
          for (revert = s.rtl ? 1 : -1, i = this.$stage.width() - this.$element.width();
            (n = this.coordinates(o)) && !(n * revert >= i);) e = ++o
        }
      else e = this._items.length - s.items;
      return e
    }, o.prototype.minimum = function (t) {
      return t ? 0 : this._clones.length / 2
    }, o.prototype.items = function (t) {
      return t === n ? this._items.slice() : (t = this.normalize(t, !0), this._items[t])
    }, o.prototype.mergers = function (t) {
      return t === n ? this._mergers.slice() : (t = this.normalize(t, !0), this._mergers[t])
    }, o.prototype.clones = function (e) {
      var i = this._clones.length / 2,
        o = i + this._items.length,
        s = function (t) {
          return t % 2 === 0 ? o + t / 2 : i - (t + 1) / 2
        };
      return e === n ? t.map(this._clones, function (t, e) {
        return s(e)
      }) : t.map(this._clones, function (t, i) {
        return t === e ? s(i) : null
      })
    }, o.prototype.speed = function (t) {
      return t !== n && (this._speed = t), this._speed
    }, o.prototype.coordinates = function (e) {
      var i = null;
      return e === n ? t.map(this._coordinates, t.proxy(function (t, e) {
        return this.coordinates(e)
      }, this)) : (this.settings.center ? (i = this._coordinates[e], i += (this.width() - i + (this._coordinates[e - 1] || 0)) / 2 * (this.settings.rtl ? -1 : 1)) : i = this._coordinates[e - 1] || 0, i)
    }, o.prototype.duration = function (t, e, i) {
      return Math.min(Math.max(Math.abs(e - t), 1), 6) * Math.abs(i || this.settings.smartSpeed)
    }, o.prototype.to = function (i, n) {
      if (this.settings.loop) {
        var o = i - this.relative(this.current()),
          s = this.current(),
          r = this.current(),
          a = this.current() + o,
          l = r - a < 0,
          c = this._clones.length + this._items.length;
        a < this.settings.items && l === !1 ? (s = r + this._items.length, this.reset(s)) : a >= c - this.settings.items && l === !0 && (s = r - this._items.length, this.reset(s)), e.clearTimeout(this.e._goToLoop), this.e._goToLoop = e.setTimeout(t.proxy(function () {
          this.speed(this.duration(this.current(), s + o, n)), this.current(s + o), this.update()
        }, this), 30)
      } else this.speed(this.duration(this.current(), i, n)), this.current(i), this.update()
    }, o.prototype.next = function (t) {
      t = t || !1, this.to(this.relative(this.current()) + 1, t)
    }, o.prototype.prev = function (t) {
      t = t || !1, this.to(this.relative(this.current()) - 1, t)
    }, o.prototype.transitionEnd = function (t) {
      return (t === n || (t.stopPropagation(), (t.target || t.srcElement || t.originalTarget) === this.$stage.get(0))) && (this.state.inMotion = !1, void this.trigger("translated"))
    }, o.prototype.viewport = function () {
      var n;
      if (this.options.responsiveBaseElement !== e) n = t(this.options.responsiveBaseElement).width();
      else if (e.innerWidth) n = e.innerWidth;
      else {
        if (!i.documentElement || !i.documentElement.clientWidth) throw "Can not detect viewport width.";
        n = i.documentElement.clientWidth
      }
      return n
    }, o.prototype.replace = function (e) {
      this.$stage.empty(), this._items = [], e && (e = e instanceof jQuery ? e : t(e)), this.settings.nestedItemSelector && (e = e.find("." + this.settings.nestedItemSelector)), e.filter(function () {
        return 1 === this.nodeType
      }).each(t.proxy(function (t, e) {
        e = this.prepare(e), this.$stage.append(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").andSelf("[data-merge]").attr("data-merge") || 1)
      }, this)), this.reset(t.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items")
    }, o.prototype.add = function (t, e) {
      e = e === n ? this._items.length : this.normalize(e, !0), this.trigger("add", {
        content: t,
        position: e
      }), 0 === this._items.length || e === this._items.length ? (this.$stage.append(t), this._items.push(t), this._mergers.push(1 * t.find("[data-merge]").andSelf("[data-merge]").attr("data-merge") || 1)) : (this._items[e].before(t), this._items.splice(e, 0, t), this._mergers.splice(e, 0, 1 * t.find("[data-merge]").andSelf("[data-merge]").attr("data-merge") || 1)), this.invalidate("items"), this.trigger("added", {
        content: t,
        position: e
      })
    }, o.prototype.remove = function (t) {
      t = this.normalize(t, !0), t !== n && (this.trigger("remove", {
        content: this._items[t],
        position: t
      }), this._items[t].remove(), this._items.splice(t, 1), this._mergers.splice(t, 1), this.invalidate("items"), this.trigger("removed", {
        content: null,
        position: t
      }))
    }, o.prototype.addTriggerableEvents = function () {
      var e = t.proxy(function (e, i) {
        return t.proxy(function (t) {
          t.relatedTarget !== this && (this.suppress([i]), e.apply(this, [].slice.call(arguments, 1)), this.release([i]))
        }, this)
      }, this);
      t.each({
        next: this.next,
        prev: this.prev,
        to: this.to,
        destroy: this.destroy,
        refresh: this.refresh,
        replace: this.replace,
        add: this.add,
        remove: this.remove
      }, t.proxy(function (t, i) {
        this.$element.on(t + ".owl.carousel", e(i, t + ".owl.carousel"))
      }, this))
    }, o.prototype.watchVisibility = function () {
      function i(t) {
        return t.offsetWidth > 0 && t.offsetHeight > 0
      }

      function n() {
        i(this.$element.get(0)) && (this.$element.removeClass("owl-hidden"), this.refresh(), e.clearInterval(this.e._checkVisibile))
      }
      i(this.$element.get(0)) || (this.$element.addClass("owl-hidden"), e.clearInterval(this.e._checkVisibile), this.e._checkVisibile = e.setInterval(t.proxy(n, this), 500))
    }, o.prototype.preloadAutoWidthImages = function (e) {
      var i, n, o, s;
      i = 0, n = this, e.each(function (r, a) {
        o = t(a), s = new Image, s.onload = function () {
          i++, o.attr("src", s.src), o.css("opacity", 1), i >= e.length && (n.state.imagesLoaded = !0, n.initialize())
        }, s.src = o.attr("src") || o.attr("data-src") || o.attr("data-src-retina")
      })
    }, o.prototype.destroy = function () {
      this.$element.hasClass(this.settings.themeClass) && this.$element.removeClass(this.settings.themeClass), this.settings.responsive !== !1 && t(e).off("resize.owl.carousel"), this.transitionEndVendor && this.off(this.$stage.get(0), this.transitionEndVendor, this.e._transitionEnd);
      for (var n in this._plugins) this._plugins[n].destroy();
      (this.settings.mouseDrag || this.settings.touchDrag) && (this.$stage.off("mousedown touchstart touchcancel"), t(i).off(".owl.dragEvents"), this.$stage.get(0).onselectstart = function () {}, this.$stage.off("dragstart", function () {
        return !1
      })), this.$element.off(".owl"), this.$stage.children(".cloned").remove(), this.e = null, this.$element.removeData("owlCarousel"), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$stage.unwrap()
    }, o.prototype.op = function (t, e, i) {
      var n = this.settings.rtl;
      switch (e) {
        case "<":
          return n ? t > i : t < i;
        case ">":
          return n ? t < i : t > i;
        case ">=":
          return n ? t <= i : t >= i;
        case "<=":
          return n ? t >= i : t <= i
      }
    }, o.prototype.on = function (t, e, i, n) {
      t.addEventListener ? t.addEventListener(e, i, n) : t.attachEvent && t.attachEvent("on" + e, i)
    }, o.prototype.off = function (t, e, i, n) {
      t.removeEventListener ? t.removeEventListener(e, i, n) : t.detachEvent && t.detachEvent("on" + e, i)
    }, o.prototype.trigger = function (e, i, n) {
      var o = {
          item: {
            count: this._items.length,
            index: this.current()
          }
        },
        s = t.camelCase(t.grep(["on", e, n], function (t) {
          return t
        }).join("-").toLowerCase()),
        r = t.Event([e, "owl", n || "carousel"].join(".").toLowerCase(), t.extend({
          relatedTarget: this
        }, o, i));
      return this._supress[e] || (t.each(this._plugins, function (t, e) {
        e.onTrigger && e.onTrigger(r)
      }), this.$element.trigger(r), this.settings && "function" == typeof this.settings[s] && this.settings[s].apply(this, r)), r
    }, o.prototype.suppress = function (e) {
      t.each(e, t.proxy(function (t, e) {
        this._supress[e] = !0
      }, this))
    }, o.prototype.release = function (e) {
      t.each(e, t.proxy(function (t, e) {
        delete this._supress[e]
      }, this))
    }, o.prototype.browserSupport = function () {
      if (this.support3d = c(), this.support3d) {
        this.transformVendor = l();
        var t = ["transitionend", "webkitTransitionEnd", "transitionend", "oTransitionEnd"];
        this.transitionEndVendor = t[a()], this.vendorName = this.transformVendor.replace(/Transform/i, ""), this.vendorName = "" !== this.vendorName ? "-" + this.vendorName.toLowerCase() + "-" : ""
      }
      this.state.orientation = e.orientation
    }, t.fn.owlCarousel = function (e) {
      return this.each(function () {
        t(this).data("owlCarousel") || t(this).data("owlCarousel", new o(this, e))
      })
    }, t.fn.owlCarousel.Constructor = o
  }(window.Zepto || window.jQuery, window, document),
  function (t, e, i, n) {
    var o = function (e) {
      this._core = e, this._loaded = [], this._handlers = {
        "initialized.owl.carousel change.owl.carousel": t.proxy(function (e) {
          if (e.namespace && this._core.settings && this._core.settings.lazyLoad && (e.property && "position" == e.property.name || "initialized" == e.type))
            for (var i = this._core.settings, n = i.center && Math.ceil(i.items / 2) || i.items, o = i.center && n * -1 || 0, s = (e.property && e.property.value || this._core.current()) + o, r = this._core.clones().length, a = t.proxy(function (t, e) {
                this.load(e)
              }, this); o++ < n;) this.load(r / 2 + this._core.relative(s)), r && t.each(this._core.clones(this._core.relative(s++)), a)
        }, this)
      }, this._core.options = t.extend({}, o.Defaults, this._core.options), this._core.$element.on(this._handlers)
    };
    o.Defaults = {
      lazyLoad: !1
    }, o.prototype.load = function (i) {
      var n = this._core.$stage.children().eq(i),
        o = n && n.find(".owl-lazy");
      !o || t.inArray(n.get(0), this._loaded) > -1 || (o.each(t.proxy(function (i, n) {
        var o, s = t(n),
          r = e.devicePixelRatio > 1 && s.attr("data-src-retina") || s.attr("data-src");
        this._core.trigger("load", {
          element: s,
          url: r
        }, "lazy"), s.is("img") ? s.one("load.owl.lazy", t.proxy(function () {
          s.css("opacity", 1), this._core.trigger("loaded", {
            element: s,
            url: r
          }, "lazy")
        }, this)).attr("src", r) : (o = new Image, o.onload = t.proxy(function () {
          s.css({
            "background-image": "url(" + r + ")",
            opacity: "1"
          }), this._core.trigger("loaded", {
            element: s,
            url: r
          }, "lazy")
        }, this), o.src = r)
      }, this)), this._loaded.push(n.get(0)))
    }, o.prototype.destroy = function () {
      var t, e;
      for (t in this.handlers) this._core.$element.off(t, this.handlers[t]);
      for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Lazy = o
  }(window.Zepto || window.jQuery, window, document),
  function (t, e, i, n) {
    var o = function (e) {
      this._core = e, this._handlers = {
        "initialized.owl.carousel": t.proxy(function () {
          this._core.settings.autoHeight && this.update()
        }, this),
        "changed.owl.carousel": t.proxy(function (t) {
          this._core.settings.autoHeight && "position" == t.property.name && this.update()
        }, this),
        "loaded.owl.lazy": t.proxy(function (t) {
          this._core.settings.autoHeight && t.element.closest("." + this._core.settings.itemClass) === this._core.$stage.children().eq(this._core.current()) && this.update()
        }, this)
      }, this._core.options = t.extend({}, o.Defaults, this._core.options), this._core.$element.on(this._handlers)
    };
    o.Defaults = {
      autoHeight: !1,
      autoHeightClass: "owl-height"
    }, o.prototype.update = function () {
      this._core.$stage.parent().height(this._core.$stage.children().eq(this._core.current()).height()).addClass(this._core.settings.autoHeightClass)
    }, o.prototype.destroy = function () {
      var t, e;
      for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
      for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.AutoHeight = o
  }(window.Zepto || window.jQuery, window, document),
  function (t, e, i, n) {
    var o = function (e) {
      this._core = e, this._videos = {}, this._playing = null, this._fullscreen = !1, this._handlers = {
        "resize.owl.carousel": t.proxy(function (t) {
          this._core.settings.video && !this.isInFullScreen() && t.preventDefault()
        }, this),
        "refresh.owl.carousel changed.owl.carousel": t.proxy(function (t) {
          this._playing && this.stop()
        }, this),
        "prepared.owl.carousel": t.proxy(function (e) {
          var i = t(e.content).find(".owl-video");
          i.length && (i.css("display", "none"), this.fetch(i, t(e.content)))
        }, this)
      }, this._core.options = t.extend({}, o.Defaults, this._core.options), this._core.$element.on(this._handlers), this._core.$element.on("click.owl.video", ".owl-video-play-icon", t.proxy(function (t) {
        this.play(t)
      }, this))
    };
    o.Defaults = {
      video: !1,
      videoHeight: !1,
      videoWidth: !1
    }, o.prototype.fetch = function (t, e) {
      var i = t.attr("data-vimeo-id") ? "vimeo" : "youtube",
        n = t.attr("data-vimeo-id") || t.attr("data-youtube-id"),
        o = t.attr("data-width") || this._core.settings.videoWidth,
        s = t.attr("data-height") || this._core.settings.videoHeight,
        r = t.attr("href");
      if (!r) throw new Error("Missing video URL.");
      if (n = r.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/), n[3].indexOf("youtu") > -1) i = "youtube";
      else {
        if (!(n[3].indexOf("vimeo") > -1)) throw new Error("Video URL not supported.");
        i = "vimeo"
      }
      n = n[6], this._videos[r] = {
        type: i,
        id: n,
        width: o,
        height: s
      }, e.attr("data-video", r), this.thumbnail(t, this._videos[r])
    }, o.prototype.thumbnail = function (e, i) {
      var n, o, s, r = i.width && i.height ? 'style="width:' + i.width + "px;height:" + i.height + 'px;"' : "",
        a = e.find("img"),
        l = "src",
        c = "",
        u = this._core.settings,
        h = function (t) {
          o = '<div class="owl-video-play-icon"></div>', n = u.lazyLoad ? '<div class="owl-video-tn ' + c + '" ' + l + '="' + t + '"></div>' : '<div class="owl-video-tn" style="opacity:1;background-image:url(' + t + ')"></div>', e.after(n), e.after(o)
        };
      return e.wrap('<div class="owl-video-wrapper"' + r + "></div>"), this._core.settings.lazyLoad && (l = "data-src", c = "owl-lazy"), a.length ? (h(a.attr(l)), a.remove(), !1) : void("youtube" === i.type ? (s = "http://img.youtube.com/vi/" + i.id + "/hqdefault.jpg", h(s)) : "vimeo" === i.type && t.ajax({
        type: "GET",
        url: "http://vimeo.com/api/v2/video/" + i.id + ".json",
        jsonp: "callback",
        dataType: "jsonp",
        success: function (t) {
          s = t[0].thumbnail_large, h(s)
        }
      }))
    }, o.prototype.stop = function () {
      this._core.trigger("stop", null, "video"), this._playing.find(".owl-video-frame").remove(), this._playing.removeClass("owl-video-playing"), this._playing = null
    }, o.prototype.play = function (e) {
      this._core.trigger("play", null, "video"), this._playing && this.stop();
      var i, n, o = t(e.target || e.srcElement),
        s = o.closest("." + this._core.settings.itemClass),
        r = this._videos[s.attr("data-video")],
        a = r.width || "100%",
        l = r.height || this._core.$stage.height();
      "youtube" === r.type ? i = '<iframe width="' + a + '" height="' + l + '" src="http://www.youtube.com/embed/' + r.id + "?autoplay=1&v=" + r.id + '" frameborder="0" allowfullscreen></iframe>' : "vimeo" === r.type && (i = '<iframe src="http://player.vimeo.com/video/' + r.id + '?autoplay=1" width="' + a + '" height="' + l + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'), s.addClass("owl-video-playing"), this._playing = s, n = t('<div style="height:' + l + "px; width:" + a + 'px" class="owl-video-frame">' + i + "</div>"), o.after(n)
    }, o.prototype.isInFullScreen = function () {
      var n = i.fullscreenElement || i.mozFullScreenElement || i.webkitFullscreenElement;
      return n && t(n).parent().hasClass("owl-video-frame") && (this._core.speed(0), this._fullscreen = !0), !(n && this._fullscreen && this._playing) && (this._fullscreen ? (this._fullscreen = !1, !1) : !this._playing || this._core.state.orientation === e.orientation || (this._core.state.orientation = e.orientation, !1))
    }, o.prototype.destroy = function () {
      var t, e;
      this._core.$element.off("click.owl.video");
      for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
      for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Video = o
  }(window.Zepto || window.jQuery, window, document),
  function (t, e, i, n) {
    var o = function (e) {
      this.core = e, this.core.options = t.extend({}, o.Defaults, this.core.options), this.swapping = !0, this.previous = n, this.next = n, this.handlers = {
        "change.owl.carousel": t.proxy(function (t) {
          "position" == t.property.name && (this.previous = this.core.current(), this.next = t.property.value)
        }, this),
        "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": t.proxy(function (t) {
          this.swapping = "translated" == t.type
        }, this),
        "translate.owl.carousel": t.proxy(function (t) {
          this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap()
        }, this)
      }, this.core.$element.on(this.handlers)
    };
    o.Defaults = {
      animateOut: !1,
      animateIn: !1
    }, o.prototype.swap = function () {
      if (1 === this.core.settings.items && this.core.support3d) {
        this.core.speed(0);
        var e, i = t.proxy(this.clear, this),
          n = this.core.$stage.children().eq(this.previous),
          o = this.core.$stage.children().eq(this.next),
          s = this.core.settings.animateIn,
          r = this.core.settings.animateOut;
        this.core.current() !== this.previous && (r && (e = this.core.coordinates(this.previous) - this.core.coordinates(this.next), n.css({
          left: e + "px"
        }).addClass("animated owl-animated-out").addClass(r).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", i)), s && o.addClass("animated owl-animated-in").addClass(s).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", i))
      }
    }, o.prototype.clear = function (e) {
      t(e.target).css({
        left: ""
      }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.transitionEnd()
    }, o.prototype.destroy = function () {
      var t, e;
      for (t in this.handlers) this.core.$element.off(t, this.handlers[t]);
      for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Animate = o
  }(window.Zepto || window.jQuery, window, document),
  function (t, e, i, n) {
    var o = function (e) {
      this.core = e, this.core.options = t.extend({}, o.Defaults, this.core.options), this.handlers = {
        "translated.owl.carousel refreshed.owl.carousel": t.proxy(function () {
          this.autoplay()
        }, this),
        "play.owl.autoplay": t.proxy(function (t, e, i) {
          this.play(e, i)
        }, this),
        "stop.owl.autoplay": t.proxy(function () {
          this.stop()
        }, this),
        "mouseover.owl.autoplay": t.proxy(function () {
          this.core.settings.autoplayHoverPause && this.pause()
        }, this),
        "mouseleave.owl.autoplay": t.proxy(function () {
          this.core.settings.autoplayHoverPause && this.autoplay()
        }, this)
      }, this.core.$element.on(this.handlers)
    };
    o.Defaults = {
      autoplay: !1,
      autoplayTimeout: 5e3,
      autoplayHoverPause: !1,
      autoplaySpeed: !1
    }, o.prototype.autoplay = function () {
      this.core.settings.autoplay && !this.core.state.videoPlay ? (e.clearInterval(this.interval), this.interval = e.setInterval(t.proxy(function () {
        this.play()
      }, this), this.core.settings.autoplayTimeout)) : e.clearInterval(this.interval)
    }, o.prototype.play = function (t, n) {
      if (i.hidden !== !0 && !(this.core.state.isTouch || this.core.state.isScrolling || this.core.state.isSwiping || this.core.state.inMotion)) return this.core.settings.autoplay === !1 ? void e.clearInterval(this.interval) : void this.core.next(this.core.settings.autoplaySpeed)
    }, o.prototype.stop = function () {
      e.clearInterval(this.interval)
    }, o.prototype.pause = function () {
      e.clearInterval(this.interval)
    }, o.prototype.destroy = function () {
      var t, i;
      e.clearInterval(this.interval);
      for (t in this.handlers) this.core.$element.off(t, this.handlers[t]);
      for (i in Object.getOwnPropertyNames(this)) "function" != typeof this[i] && (this[i] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.autoplay = o
  }(window.Zepto || window.jQuery, window, document),
  function (t, e, i, n) {
    "use strict";
    var o = function (e) {
      this._core = e, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = {
        next: this._core.next,
        prev: this._core.prev,
        to: this._core.to
      }, this._handlers = {
        "prepared.owl.carousel": t.proxy(function (e) {
          this._core.settings.dotsData && this._templates.push(t(e.content).find("[data-dot]").andSelf("[data-dot]").attr("data-dot"))
        }, this),
        "add.owl.carousel": t.proxy(function (e) {
          this._core.settings.dotsData && this._templates.splice(e.position, 0, t(e.content).find("[data-dot]").andSelf("[data-dot]").attr("data-dot"))
        }, this),
        "remove.owl.carousel prepared.owl.carousel": t.proxy(function (t) {
          this._core.settings.dotsData && this._templates.splice(t.position, 1)
        }, this),
        "change.owl.carousel": t.proxy(function (t) {
          if ("position" == t.property.name && !this._core.state.revert && !this._core.settings.loop && this._core.settings.navRewind) {
            var e = this._core.current(),
              i = this._core.maximum(),
              n = this._core.minimum();
            t.data = t.property.value > i ? e >= i ? n : i : t.property.value < n ? i : t.property.value
          }
        }, this),
        "changed.owl.carousel": t.proxy(function (t) {
          "position" == t.property.name && this.draw()
        }, this),
        "refreshed.owl.carousel": t.proxy(function () {
          this._initialized || (this.initialize(), this._initialized = !0), this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation")
        }, this)
      }, this._core.options = t.extend({}, o.Defaults, this._core.options), this.$element.on(this._handlers)
    };
    o.Defaults = {
      nav: !1,
      navRewind: !0,
      navText: ["prev", "next"],
      navSpeed: !1,
      navElement: "div",
      navContainer: !1,
      navContainerClass: "owl-nav",
      navClass: ["owl-prev", "owl-next"],
      slideBy: 1,
      dotClass: "owl-dot",
      dotsClass: "owl-dots",
      dots: !0,
      dotsEach: !1,
      dotData: !1,
      dotsSpeed: !1,
      dotsContainer: !1,
      controlsClass: "owl-controls"
    }, o.prototype.initialize = function () {
      var e, i, n = this._core.settings;
      n.dotsData || (this._templates = [t("<div>").addClass(n.dotClass).append(t("<span>")).prop("outerHTML")]), n.navContainer && n.dotsContainer || (this._controls.$container = t("<div>").addClass(n.controlsClass).appendTo(this.$element)), this._controls.$indicators = n.dotsContainer ? t(n.dotsContainer) : t("<div>").hide().addClass(n.dotsClass).appendTo(this._controls.$container), this._controls.$indicators.on("click", "div", t.proxy(function (e) {
        var i = t(e.target).parent().is(this._controls.$indicators) ? t(e.target).index() : t(e.target).parent().index();
        e.preventDefault(), this.to(i, n.dotsSpeed)
      }, this)), e = n.navContainer ? t(n.navContainer) : t("<div>").addClass(n.navContainerClass).prependTo(this._controls.$container), this._controls.$next = t("<" + n.navElement + ">"), this._controls.$previous = this._controls.$next.clone(), this._controls.$previous.addClass(n.navClass[0]).html(n.navText[0]).hide().prependTo(e).on("click", t.proxy(function (t) {
        this.prev(n.navSpeed)
      }, this)), this._controls.$next.addClass(n.navClass[1]).html(n.navText[1]).hide().appendTo(e).on("click", t.proxy(function (t) {
        this.next(n.navSpeed)
      }, this));
      for (i in this._overrides) this._core[i] = t.proxy(this[i], this)
    }, o.prototype.destroy = function () {
      var t, e, i, n;
      for (t in this._handlers) this.$element.off(t, this._handlers[t]);
      for (e in this._controls) this._controls[e].remove();
      for (n in this.overides) this._core[n] = this._overrides[n];
      for (i in Object.getOwnPropertyNames(this)) "function" != typeof this[i] && (this[i] = null)
    }, o.prototype.update = function () {
      var t, e, i, n = this._core.settings,
        o = this._core.clones().length / 2,
        s = o + this._core.items().length,
        r = n.center || n.autoWidth || n.dotData ? 1 : n.dotsEach || n.items;
      if ("page" !== n.slideBy && (n.slideBy = Math.min(n.slideBy, n.items)), n.dots || "page" == n.slideBy)
        for (this._pages = [], t = o, e = 0, i = 0; t < s; t++)(e >= r || 0 === e) && (this._pages.push({
          start: t - o,
          end: t - o + r - 1
        }), e = 0, ++i), e += this._core.mergers(this._core.relative(t))
    }, o.prototype.draw = function () {
      var e, i, n = "",
        o = this._core.settings,
        s = (this._core.$stage.children(), this._core.relative(this._core.current()));
      if (!o.nav || o.loop || o.navRewind || (this._controls.$previous.toggleClass("disabled", s <= 0), this._controls.$next.toggleClass("disabled", s >= this._core.maximum())), this._controls.$previous.toggle(o.nav), this._controls.$next.toggle(o.nav), o.dots) {
        if (e = this._pages.length - this._controls.$indicators.children().length, o.dotData && 0 !== e) {
          for (i = 0; i < this._controls.$indicators.children().length; i++) n += this._templates[this._core.relative(i)];
          this._controls.$indicators.html(n)
        } else e > 0 ? (n = new Array(e + 1).join(this._templates[0]), this._controls.$indicators.append(n)) : e < 0 && this._controls.$indicators.children().slice(e).remove();
        this._controls.$indicators.find(".active").removeClass("active"), this._controls.$indicators.children().eq(t.inArray(this.current(), this._pages)).addClass("active")
      }
      this._controls.$indicators.toggle(o.dots)
    }, o.prototype.onTrigger = function (e) {
      var i = this._core.settings;
      e.page = {
        index: t.inArray(this.current(), this._pages),
        count: this._pages.length,
        size: i && (i.center || i.autoWidth || i.dotData ? 1 : i.dotsEach || i.items)
      }
    }, o.prototype.current = function () {
      var e = this._core.relative(this._core.current());
      return t.grep(this._pages, function (t) {
        return t.start <= e && t.end >= e
      }).pop()
    }, o.prototype.getPosition = function (e) {
      var i, n, o = this._core.settings;
      return "page" == o.slideBy ? (i = t.inArray(this.current(), this._pages), n = this._pages.length, e ? ++i : --i, i = this._pages[(i % n + n) % n].start) : (i = this._core.relative(this._core.current()), n = this._core.items().length, e ? i += o.slideBy : i -= o.slideBy), i
    }, o.prototype.next = function (e) {
      t.proxy(this._overrides.to, this._core)(this.getPosition(!0), e)
    }, o.prototype.prev = function (e) {
      t.proxy(this._overrides.to, this._core)(this.getPosition(!1), e)
    }, o.prototype.to = function (e, i, n) {
      var o;
      n ? t.proxy(this._overrides.to, this._core)(e, i) : (o = this._pages.length, t.proxy(this._overrides.to, this._core)(this._pages[(e % o + o) % o].start, i))
    }, t.fn.owlCarousel.Constructor.Plugins.Navigation = o
  }(window.Zepto || window.jQuery, window, document),
  function (t, e, i, n) {
    "use strict";
    var o = function (i) {
      this._core = i, this._hashes = {}, this.$element = this._core.$element, this._handlers = {
        "initialized.owl.carousel": t.proxy(function () {
          "URLHash" == this._core.settings.startPosition && t(e).trigger("hashchange.owl.navigation")
        }, this),
        "prepared.owl.carousel": t.proxy(function (e) {
          var i = t(e.content).find("[data-hash]").andSelf("[data-hash]").attr("data-hash");
          this._hashes[i] = e.content
        }, this)
      }, this._core.options = t.extend({}, o.Defaults, this._core.options), this.$element.on(this._handlers), t(e).on("hashchange.owl.navigation", t.proxy(function () {
        var t = e.location.hash.substring(1),
          i = this._core.$stage.children(),
          n = this._hashes[t] && i.index(this._hashes[t]) || 0;
        return !!t && void this._core.to(n, !1, !0)
      }, this))
    };
    o.Defaults = {
      URLhashListener: !1
    }, o.prototype.destroy = function () {
      var i, n;
      t(e).off("hashchange.owl.navigation");
      for (i in this._handlers) this._core.$element.off(i, this._handlers[i]);
      for (n in Object.getOwnPropertyNames(this)) "function" != typeof this[n] && (this[n] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Hash = o
  }(window.Zepto || window.jQuery, window, document), ! function (t) {
    function e(t) {
      var e = document.createElement("input"),
        i = "on" + t,
        n = i in e;
      return n || (e.setAttribute(i, "return;"), n = "function" == typeof e[i]), e = null, n
    }

    function i(t) {
      var e = "text" == t || "tel" == t;
      if (!e) {
        var i = document.createElement("input");
        i.setAttribute("type", t), e = "text" === i.type, i = null
      }
      return e
    }

    function n(e, i, o) {
      var s = o.aliases[e];
      return !!s && (s.alias && n(s.alias, void 0, o), t.extend(!0, o, s), t.extend(!0, o, i), !0)
    }

    function o(e) {
      function i(i) {
        function n(t, e, i, n) {
          this.matches = [], this.isGroup = t || !1, this.isOptional = e || !1, this.isQuantifier = i || !1, this.isAlternator = n || !1, this.quantifier = {
            min: 1,
            max: 1
          }
        }

        function o(i, n, o) {
          var s = e.definitions[n],
            r = 0 == i.matches.length;
          if (o = void 0 != o ? o : i.matches.length, s && !d) {
            s.placeholder = t.isFunction(s.placeholder) ? s.placeholder.call(this, e) : s.placeholder;
            for (var a = s.prevalidator, l = a ? a.length : 0, c = 1; c < s.cardinality; c++) {
              var u = l >= c ? a[c - 1] : [],
                h = u.validator,
                p = u.cardinality;
              i.matches.splice(o++, 0, {
                fn: h ? "string" == typeof h ? new RegExp(h) : new function () {
                  this.test = h
                } : new RegExp("."),
                cardinality: p ? p : 1,
                optionality: i.isOptional,
                newBlockMarker: r,
                casing: s.casing,
                def: s.definitionSymbol || n,
                placeholder: s.placeholder,
                mask: n
              })
            }
            i.matches.splice(o++, 0, {
              fn: s.validator ? "string" == typeof s.validator ? new RegExp(s.validator) : new function () {
                this.test = s.validator
              } : new RegExp("."),
              cardinality: s.cardinality,
              optionality: i.isOptional,
              newBlockMarker: r,
              casing: s.casing,
              def: s.definitionSymbol || n,
              placeholder: s.placeholder,
              mask: n
            })
          } else i.matches.splice(o++, 0, {
            fn: null,
            cardinality: 0,
            optionality: i.isOptional,
            newBlockMarker: r,
            casing: null,
            def: n,
            placeholder: void 0,
            mask: n
          }), d = !1
        }
        for (var s, r, a, l, c, u, h = /(?:[?*+]|\{[0-9\+\*]+(?:,[0-9\+\*]*)?\})\??|[^.?*+^${[]()|\\]+|./g, d = !1, p = new n, f = [], g = []; s = h.exec(i);) switch (r = s[0], r.charAt(0)) {
          case e.optionalmarker.end:
          case e.groupmarker.end:
            if (a = f.pop(), f.length > 0) {
              if (l = f[f.length - 1], l.matches.push(a), l.isAlternator) {
                c = f.pop();
                for (var m = 0; m < c.matches.length; m++) c.matches[m].isGroup = !1;
                f.length > 0 ? (l = f[f.length - 1], l.matches.push(c)) : p.matches.push(c)
              }
            } else p.matches.push(a);
            break;
          case e.optionalmarker.start:
            f.push(new n(!1, !0));
            break;
          case e.groupmarker.start:
            f.push(new n(!0));
            break;
          case e.quantifiermarker.start:
            var _ = new n(!1, !1, !0);
            r = r.replace(/[{}]/g, "");
            var v = r.split(","),
              y = isNaN(v[0]) ? v[0] : parseInt(v[0]),
              w = 1 == v.length ? y : isNaN(v[1]) ? v[1] : parseInt(v[1]);
            if (("*" == w || "+" == w) && (y = "*" == w ? 0 : 1), _.quantifier = {
                min: y,
                max: w
              }, f.length > 0) {
              var b = f[f.length - 1].matches;
              if (s = b.pop(), !s.isGroup) {
                var x = new n(!0);
                x.matches.push(s), s = x
              }
              b.push(s), b.push(_)
            } else {
              if (s = p.matches.pop(), !s.isGroup) {
                var x = new n(!0);
                x.matches.push(s), s = x
              }
              p.matches.push(s), p.matches.push(_)
            }
            break;
          case e.escapeChar:
            d = !0;
            break;
          case e.alternatormarker:
            f.length > 0 ? (l = f[f.length - 1], u = l.matches.pop()) : u = p.matches.pop(), u.isAlternator ? f.push(u) : (c = new n(!1, !1, !1, !0), c.matches.push(u), f.push(c));
            break;
          default:
            if (f.length > 0) {
              if (l = f[f.length - 1], l.matches.length > 0 && (u = l.matches[l.matches.length - 1], u.isGroup && (u.isGroup = !1, o(u, e.groupmarker.start, 0), o(u, e.groupmarker.end))), o(l, r), l.isAlternator) {
                c = f.pop();
                for (var m = 0; m < c.matches.length; m++) c.matches[m].isGroup = !1;
                f.length > 0 ? (l = f[f.length - 1], l.matches.push(c)) : p.matches.push(c)
              }
            } else p.matches.length > 0 && (u = p.matches[p.matches.length - 1], u.isGroup && (u.isGroup = !1, o(u, e.groupmarker.start, 0), o(u, e.groupmarker.end))), o(p, r)
        }
        return p.matches.length > 0 && (u = p.matches[p.matches.length - 1], u.isGroup && (u.isGroup = !1, o(u, e.groupmarker.start, 0), o(u, e.groupmarker.end)), g.push(p)), g
      }

      function n(n, o) {
        if (void 0 != n && "" != n) {
          if (1 == n.length && 0 == e.greedy && 0 != e.repeat && (e.placeholder = ""), e.repeat > 0 || "*" == e.repeat || "+" == e.repeat) {
            var s = "*" == e.repeat ? 0 : "+" == e.repeat ? 1 : e.repeat;
            n = e.groupmarker.start + n + e.groupmarker.end + e.quantifiermarker.start + s + "," + e.repeat + e.quantifiermarker.end
          }
          return void 0 == t.inputmask.masksCache[n] && (t.inputmask.masksCache[n] = {
            mask: n,
            maskToken: i(n),
            validPositions: {},
            _buffer: void 0,
            buffer: void 0,
            tests: {},
            metadata: o
          }), t.extend(!0, {}, t.inputmask.masksCache[n])
        }
      }

      function o(t) {
        if (t = t.toString(), e.numericInput) {
          t = t.split("").reverse();
          for (var i = 0; i < t.length; i++) t[i] == e.optionalmarker.start ? t[i] = e.optionalmarker.end : t[i] == e.optionalmarker.end ? t[i] = e.optionalmarker.start : t[i] == e.groupmarker.start ? t[i] = e.groupmarker.end : t[i] == e.groupmarker.end && (t[i] = e.groupmarker.start);
          t = t.join("")
        }
        return t
      }
      var s = void 0;
      if (t.isFunction(e.mask) && (e.mask = e.mask.call(this, e)), t.isArray(e.mask)) {
        if (e.mask.length > 1) {
          e.keepStatic = void 0 == e.keepStatic || e.keepStatic;
          var r = "(";
          return t.each(e.mask, function (e, i) {
            r.length > 1 && (r += ")|("), r += o(void 0 == i.mask || t.isFunction(i.mask) ? i : i.mask)
          }), r += ")", n(r, e.mask)
        }
        e.mask = e.mask.pop()
      }
      return e.mask && (s = void 0 == e.mask.mask || t.isFunction(e.mask.mask) ? n(o(e.mask), e.mask) : n(o(e.mask.mask), e.mask)), s
    }

    function s(n, o, s) {
      function r(t, e, i) {
        e = e || 0;
        var n, o, s, r = [],
          a = 0;
        do {
          if (t === !0 && u().validPositions[a]) {
            var l = u().validPositions[a];
            o = l.match, n = l.locator.slice(), r.push(i === !0 ? l.input : P(a, o))
          } else s = g(a, n, a - 1), o = s.match, n = s.locator.slice(), r.push(P(a, o));
          a++
        } while ((void 0 == it || it > a - 1) && null != o.fn || null == o.fn && "" != o.def || e >= a);
        return r.pop(), r
      }

      function u() {
        return o
      }

      function h(t) {
        var e = u();
        e.buffer = void 0, e.tests = {}, t !== !0 && (e._buffer = void 0, e.validPositions = {}, e.p = 0)
      }

      function d(t) {
        var e = u(),
          i = -1,
          n = e.validPositions;
        void 0 == t && (t = -1);
        var o = i,
          s = i;
        for (var r in n) {
          var a = parseInt(r);
          (-1 == t || null != n[a].match.fn) && (t >= a && (o = a), a >= t && (s = a))
        }
        return i = -1 != o && t - o > 1 || t > s ? o : s
      }

      function p(e, i, n) {
        if (s.insertMode && void 0 != u().validPositions[e] && void 0 == n) {
          var o, r = t.extend(!0, {}, u().validPositions),
            a = d();
          for (o = e; a >= o; o++) delete u().validPositions[o];
          u().validPositions[e] = i;
          var l, c = !0;
          for (o = e; a >= o; o++) {
            var h = r[o];
            if (void 0 != h) {
              var p = u().validPositions;
              l = !s.keepStatic && p[o] && (void 0 != p[o + 1] && v(o + 1, p[o].locator.slice(), o).length > 1 || void 0 != p[o].alternation) ? o + 1 : E(o), c = _(l, h.match.def) ? c && C(l, h.input, !0, !0) !== !1 : null == h.match.fn
            }
            if (!c) break
          }
          if (!c) return u().validPositions = t.extend(!0, {}, r), !1
        } else u().validPositions[e] = i;
        return !0
      }

      function f(t, e, i, n) {
        var o, r = t;
        for (u().p = t, void 0 != u().validPositions[t] && u().validPositions[t].input == s.radixPoint && (e++, r++), o = r; e > o; o++) void 0 != u().validPositions[o] && (i === !0 || 0 != s.canClearPosition(u(), o, d(), n, s)) && delete u().validPositions[o];
        for (h(!0), o = r + 1; o <= d();) {
          for (; void 0 != u().validPositions[r];) r++;
          var a = u().validPositions[r];
          r > o && (o = r + 1);
          var l = u().validPositions[o];
          void 0 != l && void 0 == a ? (_(r, l.match.def) && C(r, l.input, !0) !== !1 && (delete u().validPositions[o], o++), r++) : o++
        }
        var c = d();
        c >= t && void 0 != u().validPositions[c] && u().validPositions[c].input == s.radixPoint && delete u().validPositions[c], h(!0)
      }

      function g(t, e, i) {
        for (var n, o = v(t, e, i), r = d(), a = u().validPositions[r] || v(0)[0], l = void 0 != a.alternation ? a.locator[a.alternation].split(",") : [], c = 0; c < o.length && (n = o[c], !(n.match && (s.greedy && n.match.optionalQuantifier !== !0 || (n.match.optionality === !1 || n.match.newBlockMarker === !1) && n.match.optionalQuantifier !== !0) && (void 0 == a.alternation || void 0 != n.locator[a.alternation] && k(n.locator[a.alternation].toString().split(","), l)))); c++);
        return n
      }

      function m(t) {
        return u().validPositions[t] ? u().validPositions[t].match : v(t)[0].match
      }

      function _(t, e) {
        for (var i = !1, n = v(t), o = 0; o < n.length; o++)
          if (n[o].match && n[o].match.def == e) {
            i = !0;
            break
          }
        return i
      }

      function v(e, i, n) {
        function o(i, n, s, a) {
          function h(s, a, p) {
            if (r > 1e4) return alert("jquery.inputmask: There is probably an error in your mask definition or in the code. Create an issue on github with an example of the mask you are using. " + u().mask), !0;
            if (r == e && void 0 == s.matches) return l.push({
              match: s,
              locator: a.reverse()
            }), !0;
            if (void 0 != s.matches) {
              if (s.isGroup && p !== !0) {
                if (s = h(i.matches[d + 1], a)) return !0
              } else if (s.isOptional) {
                var f = s;
                if (s = o(s, n, a, p)) {
                  var g = l[l.length - 1].match,
                    m = 0 == t.inArray(g, f.matches);
                  m && (c = !0), r = e
                }
              } else if (s.isAlternator) {
                var _, v = s,
                  y = [],
                  w = l.slice(),
                  b = a.length,
                  x = n.length > 0 ? n.shift() : -1;
                if (-1 == x || "string" == typeof x) {
                  var k, C = r,
                    $ = n.slice();
                  "string" == typeof x && (k = x.split(","));
                  for (var T = 0; T < v.matches.length; T++) {
                    l = [], s = h(v.matches[T], [T].concat(a), p) || s, _ = l.slice(), r = C, l = [];
                    for (var E = 0; E < $.length; E++) n[E] = $[E];
                    for (var j = 0; j < _.length; j++)
                      for (var S = _[j], A = 0; A < y.length; A++) {
                        var P = y[A];
                        if (S.match.mask == P.match.mask && ("string" != typeof x || -1 != t.inArray(S.locator[b].toString(), k))) {
                          _.splice(j, 1), P.locator[b] = P.locator[b] + "," + S.locator[b], P.alternation = b;
                          break
                        }
                      }
                    y = y.concat(_)
                  }
                  "string" == typeof x && (y = t.map(y, function (e, i) {
                    if (isFinite(i)) {
                      var n, o = e.locator[b].toString().split(",");
                      e.locator[b] = void 0, e.alternation = void 0;
                      for (var s = 0; s < o.length; s++) n = -1 != t.inArray(o[s], k), n && (void 0 != e.locator[b] ? (e.locator[b] += ",", e.alternation = b, e.locator[b] += o[s]) : e.locator[b] = parseInt(o[s]));
                      if (void 0 != e.locator[b]) return e
                    }
                  })), l = w.concat(y), c = !0
                } else s = h(v.matches[x], [x].concat(a), p);
                if (s) return !0
              } else if (s.isQuantifier && p !== !0)
                for (var D = s, O = n.length > 0 && p !== !0 ? n.shift() : 0; O < (isNaN(D.quantifier.max) ? O + 1 : D.quantifier.max) && e >= r; O++) {
                  var I = i.matches[t.inArray(D, i.matches) - 1];
                  if (s = h(I, [O].concat(a), !0)) {
                    var g = l[l.length - 1].match;
                    g.optionalQuantifier = O > D.quantifier.min - 1;
                    var m = 0 == t.inArray(g, I.matches);
                    if (m) {
                      if (O > D.quantifier.min - 1) {
                        c = !0, r = e;
                        break
                      }
                      return !0
                    }
                    return !0
                  }
                } else if (s = o(s, n, a, p)) return !0
            } else r++
          }
          for (var d = n.length > 0 ? n.shift() : 0; d < i.matches.length; d++)
            if (i.matches[d].isQuantifier !== !0) {
              var p = h(i.matches[d], [d].concat(s), a);
              if (p && r == e) return p;
              if (r > e) break
            }
        }
        var s = u().maskToken,
          r = i ? n : 0,
          a = i || [0],
          l = [],
          c = !1;
        if (void 0 == i) {
          for (var h, d = e - 1; void 0 == (h = u().validPositions[d]) && d > -1;) d--;
          if (void 0 != h && d > -1) r = d, a = h.locator.slice();
          else {
            for (d = e - 1; void 0 == (h = u().tests[d]) && d > -1;) d--;
            void 0 != h && d > -1 && (r = d, a = h[0].locator.slice())
          }
        }
        for (var p = a.shift(); p < s.length; p++) {
          var f = o(s[p], a, [p]);
          if (f && r == e || r > e) break
        }
        return (0 == l.length || c) && l.push({
          match: {
            fn: null,
            cardinality: 0,
            optionality: !0,
            casing: null,
            def: ""
          },
          locator: []
        }), u().tests[e] = t.extend(!0, [], l), u().tests[e]
      }

      function y() {
        return void 0 == u()._buffer && (u()._buffer = r(!1, 1)), u()._buffer
      }

      function w() {
        return void 0 == u().buffer && (u().buffer = r(!0, d(), !0)), u().buffer
      }

      function b(t, e, i) {
        if (i = i || w().slice(), t === !0) h(), t = 0, e = i.length;
        else
          for (var n = t; e > n; n++) delete u().validPositions[n], delete u().tests[n];
        for (var n = t; e > n; n++) i[n] != s.skipOptionalPartCharacter && C(n, i[n], !0, !0)
      }

      function x(t, e) {
        switch (e.casing) {
          case "upper":
            t = t.toUpperCase();
            break;
          case "lower":
            t = t.toLowerCase()
        }
        return t
      }

      function k(e, i) {
        for (var n = s.greedy ? i : i.slice(0, 1), o = !1, r = 0; r < e.length; r++)
          if (-1 != t.inArray(e[r], n)) {
            o = !0;
            break
          }
        return o
      }

      function C(e, i, n, o) {
        function r(e, i, n, o) {
          var r = !1;
          return t.each(v(e), function (a, l) {
            for (var c = l.match, g = i ? 1 : 0, m = "", _ = (w(), c.cardinality); _ > g; _--) m += S(e - (_ - 1));
            if (i && (m += i), r = null != c.fn ? c.fn.test(m, u(), e, n, s) : (i == c.def || i == s.skipOptionalPartCharacter) && "" != c.def && {
                c: c.def,
                pos: e
              }, r !== !1) {
              var v = void 0 != r.c ? r.c : i;
              v = v == s.skipOptionalPartCharacter && null === c.fn ? c.def : v;
              var y = e;
              if (void 0 != r.remove && f(r.remove, r.remove + 1, !0), r.refreshFromBuffer) {
                var k = r.refreshFromBuffer;
                if (n = !0, b(k === !0 ? k : k.start, k.end), void 0 == r.pos && void 0 == r.c) return r.pos = d(), !1;
                if (y = void 0 != r.pos ? r.pos : e, y != e) return r = t.extend(r, C(y, v, !0)), !1
              } else if (r !== !0 && void 0 != r.pos && r.pos != e && (y = r.pos, b(e, y), y != e)) return r = t.extend(r, C(y, v, !0)), !1;
              return (1 == r || void 0 != r.pos || void 0 != r.c) && (a > 0 && h(!0), p(y, t.extend({}, l, {
                input: x(v, c)
              }), o) || (r = !1), !1)
            }
          }), r
        }

        function a(e, i, n, o) {
          var r, a, l = t.extend(!0, {}, u().validPositions);
          for (r = d(); r >= 0; r--)
            if (u().validPositions[r] && void 0 != u().validPositions[r].alternation) {
              a = u().validPositions[r].alternation;
              break
            }
          if (void 0 != a)
            for (var c in u().validPositions)
              if (parseInt(c) > parseInt(r) && void 0 === u().validPositions[c].alternation) {
                for (var p = u().validPositions[c], f = p.locator[a], g = u().validPositions[r].locator[a].split(","), m = 0; m < g.length; m++)
                  if (f < g[m]) {
                    for (var _, v, y = c - 1; y >= 0; y--)
                      if (_ = u().validPositions[y], void 0 != _) {
                        v = _.locator[a], _.locator[a] = g[m];
                        break
                      }
                    if (f != _.locator[a]) {
                      for (var b = w().slice(), x = c; x < d() + 1; x++) delete u().validPositions[x], delete u().tests[x];
                      h(!0), s.keepStatic = !s.keepStatic;
                      for (var x = c; x < b.length; x++) b[x] != s.skipOptionalPartCharacter && C(d() + 1, b[x], !1, !0);
                      _.locator[a] = v;
                      var k = C(e, i, n, o);
                      if (s.keepStatic = !s.keepStatic, k) return k;
                      h(), u().validPositions = t.extend(!0, {}, l)
                    }
                  }
                break
              }
          return !1
        }

        function l(e, i) {
          for (var n = u().validPositions[i], o = n.locator, s = o.length, r = e; i > r; r++)
            if (!$(r)) {
              var a = v(r),
                l = a[0],
                c = -1;
              t.each(a, function (t, e) {
                for (var i = 0; s > i; i++) e.locator[i] && k(e.locator[i].toString().split(","), o[i].toString().split(",")) && i > c && (c = i, l = e)
              }), p(r, t.extend({}, l, {
                input: l.match.def
              }), !0)
            }
        }
        n = n === !0;
        for (var c = w(), g = e - 1; g > -1 && !u().validPositions[g]; g--);
        for (g++; e > g; g++) void 0 == u().validPositions[g] && ((!$(g) || c[g] != P(g)) && v(g).length > 1 || c[g] == s.radixPoint || "0" == c[g] && t.inArray(s.radixPoint, c) < g) && r(g, c[g], !0);
        var m = e,
          _ = !1,
          y = t.extend(!0, {}, u().validPositions);
        if (m < T() && (_ = r(m, i, n, o), !n && _ === !1)) {
          var j = u().validPositions[m];
          if (!j || null != j.match.fn || j.match.def != i && i != s.skipOptionalPartCharacter) {
            if ((s.insertMode || void 0 == u().validPositions[E(m)]) && !$(m))
              for (var A = m + 1, D = E(m); D >= A; A++)
                if (_ = r(A, i, n, o), _ !== !1) {
                  l(m, A), m = A;
                  break
                }
          } else _ = {
            caret: E(m)
          }
        }
        if (_ === !1 && s.keepStatic && H(c) && (_ = a(e, i, n, o)), _ === !0 && (_ = {
            pos: m
          }), t.isFunction(s.postValidation) && 0 != _ && !n) {
          h(!0);
          var O = s.postValidation(w(), s);
          if (!O) return h(!0), u().validPositions = t.extend(!0, {}, y), !1
        }
        return _
      }

      function $(t) {
        var e = m(t);
        return null != e.fn && e.fn
      }

      function T() {
        var t;
        it = et.prop("maxLength"), -1 == it && (it = void 0);
        var e, i = d(),
          n = u().validPositions[i],
          o = void 0 != n ? n.locator.slice() : void 0;
        for (e = i + 1; void 0 == n || null != n.match.fn || null == n.match.fn && "" != n.match.def; e++) n = g(e, o, e - 1), o = n.locator.slice();
        return t = e, void 0 == it || it > t ? t : it
      }

      function E(t) {
        var e = T();
        if (t >= e) return e;
        for (var i = t; ++i < e && !$(i) && (s.nojumps !== !0 || s.nojumpsThreshold > i););
        return i
      }

      function j(t) {
        var e = t;
        if (0 >= e) return 0;
        for (; --e > 0 && !$(e););
        return e
      }

      function S(t) {
        return void 0 == u().validPositions[t] ? P(t) : u().validPositions[t].input
      }

      function A(e, i, n, o, r) {
        if (o && t.isFunction(s.onBeforeWrite)) {
          var a = s.onBeforeWrite.call(e, o, i, n, s);
          if (a) {
            if (a.refreshFromBuffer) {
              var l = a.refreshFromBuffer;
              b(l === !0 ? l : l.start, l.end, a.buffer), h(!0), i = w()
            }
            n = a.caret || n
          }
        }
        e._valueSet(i.join("")), void 0 != n && M(e, n), r === !0 && (st = !0, t(e).trigger("input"))
      }

      function P(t, e) {
        return e = e || m(t), void 0 != e.placeholder ? e.placeholder : null == e.fn ? e.def : s.placeholder.charAt(t % s.placeholder.length)
      }

      function D(e, i, n, o) {
        function s() {
          var t = !1,
            e = y().slice(p, E(p)).join("").indexOf(c);
          if (-1 != e && !$(p)) {
            t = !0;
            for (var i = y().slice(p, p + e), n = 0; n < i.length; n++)
              if (" " != i[n]) {
                t = !1;
                break
              }
          }
          return t
        }
        var r = void 0 != o ? o.slice() : e._valueGet().split("");
        h(), u().p = E(-1), i && e._valueSet("");
        var a = y().slice(0, E(-1)).join(""),
          l = r.join("").match(new RegExp(O(a), "g"));
        l && l.length > 0 && r.splice(0, a.length * l.length);
        var c = "",
          p = 0;
        t.each(r, function (i, o) {
          var r = t.Event("keypress");
          r.which = o.charCodeAt(0), c += o;
          var a = d(),
            l = u().validPositions[a],
            h = g(a + 1, l ? l.locator.slice() : void 0, a);
          if (!s() || n) {
            var f = n ? i : null == h.match.fn && h.match.optionality && a + 1 < u().p ? a + 1 : u().p;
            G.call(e, r, !0, !1, n, f), p = f + 1, c = ""
          } else G.call(e, r, !0, !1, !0, a + 1)
        }), i && A(e, w(), t(e).is(":focus") ? E(d(0)) : void 0, t.Event("checkval"))
      }

      function O(e) {
        return t.inputmask.escapeRegex.call(this, e)
      }

      function I(e) {
        if (e.data("_inputmask") && !e.hasClass("hasDatepicker")) {
          var i = [],
            n = u().validPositions;
          for (var o in n) n[o].match && null != n[o].match.fn && i.push(n[o].input);
          var r = (nt ? i.reverse() : i).join(""),
            a = (nt ? w().slice().reverse() : w()).join("");
          return t.isFunction(s.onUnMask) && (r = s.onUnMask.call(e, a, r, s) || r), r
        }
        return e[0]._valueGet()
      }

      function N(t) {
        if (nt && "number" == typeof t && (!s.greedy || "" != s.placeholder)) {
          var e = w().length;
          t = e - t
        }
        return t
      }

      function M(e, i, n) {
        var o, r = e.jquery && e.length > 0 ? e[0] : e;
        if ("number" != typeof i) return r.setSelectionRange ? (i = r.selectionStart, n = r.selectionEnd) : document.selection && document.selection.createRange && (o = document.selection.createRange(), i = 0 - o.duplicate().moveStart("character", -1e5), n = i + o.text.length), {
          begin: N(i),
          end: N(n)
        };
        if (i = N(i), n = N(n), n = "number" == typeof n ? n : i, t(r).is(":visible")) {
          var a = t(r).css("font-size").replace("px", "") * n;
          r.scrollLeft = a > r.scrollWidth ? a : 0, 0 == s.insertMode && i == n && n++, r.setSelectionRange ? (r.selectionStart = i, r.selectionEnd = n) : r.createTextRange && (o = r.createTextRange(), o.collapse(!0), o.moveEnd("character", n), o.moveStart("character", i), o.select())
        }
      }

      function L(e) {
        var i, n, o = w(),
          s = o.length,
          r = d(),
          a = {},
          l = u().validPositions[r],
          c = void 0 != l ? l.locator.slice() : void 0;
        for (i = r + 1; i < o.length; i++) n = g(i, c, i - 1), c = n.locator.slice(), a[i] = t.extend(!0, {}, n);
        var h = l && void 0 != l.alternation ? l.locator[l.alternation].split(",") : [];
        for (i = s - 1; i > r && (n = a[i].match, (n.optionality || n.optionalQuantifier || l && void 0 != l.alternation && void 0 != a[i].locator[l.alternation] && -1 != t.inArray(a[i].locator[l.alternation].toString(), h)) && o[i] == P(i, n)); i--) s--;
        return e ? {
          l: s,
          def: a[s] ? a[s].match : void 0
        } : s
      }

      function z(t) {
        for (var e = L(), i = t.length - 1; i > e && !$(i); i--);
        t.splice(e, i + 1 - e)
      }

      function H(e) {
        if (t.isFunction(s.isComplete)) return s.isComplete.call(et, e, s);
        if ("*" != s.repeat) {
          var i = !1,
            n = L(!0),
            o = j(n.l),
            r = d();
          if (r == o && (void 0 == n.def || n.def.newBlockMarker || n.def.optionalQuantifier)) {
            i = !0;
            for (var a = 0; o >= a; a++) {
              var l = $(a);
              if (l && (void 0 == e[a] || e[a] == P(a)) || !l && e[a] != P(a)) {
                i = !1;
                break
              }
            }
          }
          return i
        }
      }

      function R(t, e) {
        return nt ? t - e > 1 || t - e == 1 && s.insertMode : e - t > 1 || e - t == 1 && s.insertMode
      }

      function F(e) {
        var i = t._data(e).events;
        t.each(i, function (e, i) {
          t.each(i, function (t, e) {
            if ("inputmask" == e.namespace && "setvalue" != e.type) {
              var i = e.handler;
              e.handler = function (t) {
                if (!this.disabled && (!this.readOnly || "keydown" == t.type && t.ctrlKey && 67 == t.keyCode)) {
                  switch (t.type) {
                    case "input":
                      if (st === !0) return st = !1, t.preventDefault();
                      break;
                    case "keydown":
                      ot = !1;
                      break;
                    case "keypress":
                      if (ot === !0) return t.preventDefault();
                      ot = !0;
                      break;
                    case "compositionstart":
                      break;
                    case "compositionupdate":
                      st = !0;
                      break;
                    case "compositionend":
                  }
                  return i.apply(this, arguments)
                }
                t.preventDefault()
              }
            }
          })
        })
      }

      function B(e) {
        function i(e) {
          if (void 0 == t.valHooks[e] || 1 != t.valHooks[e].inputmaskpatch) {
            var i = t.valHooks[e] && t.valHooks[e].get ? t.valHooks[e].get : function (t) {
                return t.value
              },
              n = t.valHooks[e] && t.valHooks[e].set ? t.valHooks[e].set : function (t, e) {
                return t.value = e, t
              };
            t.valHooks[e] = {
              get: function (e) {
                var n = t(e);
                if (n.data("_inputmask")) {
                  if (n.data("_inputmask").opts.autoUnmask) return n.inputmask("unmaskedvalue");
                  var o = i(e),
                    s = n.data("_inputmask"),
                    r = s.maskset,
                    a = r._buffer;
                  return a = a ? a.join("") : "", o != a ? o : ""
                }
                return i(e)
              },
              set: function (e, i) {
                var o, s = t(e),
                  r = s.data("_inputmask");
                return r ? (o = n(e, t.isFunction(r.opts.onBeforeMask) ? r.opts.onBeforeMask.call(dt, i, r.opts) || i : i), s.triggerHandler("setvalue.inputmask")) : o = n(e, i), o
              },
              inputmaskpatch: !0
            }
          }
        }

        function n() {
          var e = t(this),
            i = t(this).data("_inputmask");
          return i ? i.opts.autoUnmask ? e.inputmask("unmaskedvalue") : a.call(this) != y().join("") ? a.call(this) : "" : a.call(this)
        }

        function o(e) {
          var i = t(this).data("_inputmask");
          i ? (l.call(this, t.isFunction(i.opts.onBeforeMask) ? i.opts.onBeforeMask.call(dt, e, i.opts) || e : e), t(this).triggerHandler("setvalue.inputmask")) : l.call(this, e)
        }

        function r(e) {
          t(e).bind("mouseenter.inputmask", function () {
            var e = t(this),
              i = this,
              n = i._valueGet();
            "" != n && n != w().join("") && (this._valueSet(t.isFunction(s.onBeforeMask) ? s.onBeforeMask.call(dt, n, s) || n : n), e.triggerHandler("setvalue.inputmask"))
          });
          var i = t._data(e).events,
            n = i.mouseover;
          if (n) {
            for (var o = n[n.length - 1], r = n.length - 1; r > 0; r--) n[r] = n[r - 1];
            n[0] = o
          }
        }
        var a, l;
        e._valueGet || (Object.getOwnPropertyDescriptor && Object.getOwnPropertyDescriptor(e, "value"), document.__lookupGetter__ && e.__lookupGetter__("value") ? (a = e.__lookupGetter__("value"), l = e.__lookupSetter__("value"), e.__defineGetter__("value", n), e.__defineSetter__("value", o)) : (a = function () {
          return e.value
        }, l = function (t) {
          e.value = t
        }, i(e.type), r(e)), e._valueGet = function (t) {
          return nt && t !== !0 ? a.call(this).split("").reverse().join("") : a.call(this)
        }, e._valueSet = function (t) {
          l.call(this, nt ? t.split("").reverse().join("") : t)
        })
      }

      function q(e, i, n, o) {
        function r() {
          if (s.keepStatic) {
            h(!0);
            var i, n = [];
            for (i = d(); i >= 0; i--)
              if (u().validPositions[i]) {
                if (void 0 != u().validPositions[i].alternation) break;
                n.push(u().validPositions[i].input), delete u().validPositions[i]
              }
            if (i > 0)
              for (; n.length > 0;) {
                u().p = E(d());
                var o = t.Event("keypress");
                o.which = n.pop().charCodeAt(0), G.call(e, o, !0, !1, !1, u().p)
              }
          }
        }
        if ((s.numericInput || nt) && (i == t.inputmask.keyCode.BACKSPACE ? i = t.inputmask.keyCode.DELETE : i == t.inputmask.keyCode.DELETE && (i = t.inputmask.keyCode.BACKSPACE), nt)) {
          var a = n.end;
          n.end = n.begin, n.begin = a
        }
        if (i == t.inputmask.keyCode.BACKSPACE && (n.end - n.begin < 1 || 0 == s.insertMode) ? n.begin = j(n.begin) : i == t.inputmask.keyCode.DELETE && n.begin == n.end && (n.end = $(n.end) ? n.end + 1 : E(n.end) + 1), f(n.begin, n.end, !1, o), o !== !0) {
          r();
          var l = d(n.begin);
          l < n.begin ? (-1 == l && h(), u().p = E(l)) : u().p = n.begin
        }
      }

      function W(i) {
        var n = this,
          o = t(n),
          r = i.keyCode,
          l = M(n);
        r == t.inputmask.keyCode.BACKSPACE || r == t.inputmask.keyCode.DELETE || a && 127 == r || i.ctrlKey && 88 == r && !e("cut") ? (i.preventDefault(), 88 == r && (Z = w().join("")), q(n, r, l), A(n, w(), u().p, i, Z != w().join("")), n._valueGet() == y().join("") ? o.trigger("cleared") : H(w()) === !0 && o.trigger("complete"), s.showTooltip && o.prop("title", u().mask)) : r == t.inputmask.keyCode.END || r == t.inputmask.keyCode.PAGE_DOWN ? setTimeout(function () {
          var t = E(d());
          s.insertMode || t != T() || i.shiftKey || t--, M(n, i.shiftKey ? l.begin : t, t)
        }, 0) : r == t.inputmask.keyCode.HOME && !i.shiftKey || r == t.inputmask.keyCode.PAGE_UP ? M(n, 0, i.shiftKey ? l.begin : 0) : (s.undoOnEscape && r == t.inputmask.keyCode.ESCAPE || 90 == r && i.ctrlKey) && i.altKey !== !0 ? (D(n, !0, !1, Z.split("")), o.click()) : r != t.inputmask.keyCode.INSERT || i.shiftKey || i.ctrlKey ? 0 != s.insertMode || i.shiftKey || (r == t.inputmask.keyCode.RIGHT ? setTimeout(function () {
          var t = M(n);
          M(n, t.begin)
        }, 0) : r == t.inputmask.keyCode.LEFT && setTimeout(function () {
          var t = M(n);
          M(n, nt ? t.begin + 1 : t.begin - 1)
        }, 0)) : (s.insertMode = !s.insertMode, M(n, s.insertMode || l.begin != T() ? l.begin : l.begin - 1)), s.onKeyDown.call(this, i, w(), M(n).begin, s), rt = -1 != t.inArray(r, s.ignorables)
      }

      function G(e, i, n, o, r) {
        var a = this,
          l = t(a),
          c = e.which || e.charCode || e.keyCode;
        if (!(i === !0 || e.ctrlKey && e.altKey) && (e.ctrlKey || e.metaKey || rt)) return !0;
        if (c) {
          46 == c && 0 == e.shiftKey && "," == s.radixPoint && (c = 44);
          var d, f = i ? {
              begin: r,
              end: r
            } : M(a),
            g = String.fromCharCode(c),
            m = R(f.begin, f.end);
          m && (u().undoPositions = t.extend(!0, {}, u().validPositions), q(a, t.inputmask.keyCode.DELETE, f, !0), f.begin = u().p, s.insertMode || (s.insertMode = !s.insertMode, p(f.begin, o), s.insertMode = !s.insertMode), m = !s.multi), u().writeOutBuffer = !0;
          var _ = nt && !m ? f.end : f.begin,
            y = C(_, g, o);
          if (y !== !1) {
            if (y !== !0 && (_ = void 0 != y.pos ? y.pos : _, g = void 0 != y.c ? y.c : g), h(!0), void 0 != y.caret) d = y.caret;
            else {
              var x = u().validPositions;
              d = !s.keepStatic && (void 0 != x[_ + 1] && v(_ + 1, x[_].locator.slice(), _).length > 1 || void 0 != x[_].alternation) ? _ + 1 : E(_)
            }
            u().p = d
          }
          if (n !== !1) {
            var k = this;
            if (setTimeout(function () {
                s.onKeyValidation.call(k, y, s)
              }, 0), u().writeOutBuffer && y !== !1) {
              var $ = w();
              A(a, $, i ? void 0 : s.numericInput ? j(d) : d, e, i !== !0), i !== !0 && setTimeout(function () {
                H($) === !0 && l.trigger("complete")
              }, 0)
            } else m && (u().buffer = void 0, u().validPositions = u().undoPositions)
          } else m && (u().buffer = void 0, u().validPositions = u().undoPositions);
          if (s.showTooltip && l.prop("title", u().mask), i && t.isFunction(s.onBeforeWrite)) {
            var T = s.onBeforeWrite.call(this, e, w(), d, s);
            if (T && T.refreshFromBuffer) {
              var S = T.refreshFromBuffer;
              b(S === !0 ? S : S.start, S.end, T.buffer), h(!0), T.caret && (u().p = T.caret)
            }
          }
          e.preventDefault()
        }
      }

      function X(e) {
        var i = this,
          n = t(i),
          o = i._valueGet(!0),
          r = M(i);
        if ("propertychange" == e.type && i._valueGet().length <= T()) return !0;
        if ("paste" == e.type) {
          var a = o.substr(0, r.begin),
            l = o.substr(r.end, o.length);
          a == y().slice(0, r.begin).join("") && (a = ""), l == y().slice(r.end).join("") && (l = ""), window.clipboardData && window.clipboardData.getData ? o = a + window.clipboardData.getData("Text") + l : e.originalEvent && e.originalEvent.clipboardData && e.originalEvent.clipboardData.getData && (o = a + e.originalEvent.clipboardData.getData("text/plain") + l)
        }
        var c = o;
        if (t.isFunction(s.onBeforePaste)) {
          if (c = s.onBeforePaste.call(i, o, s), c === !1) return e.preventDefault(), !1;
          c || (c = o)
        }
        return D(i, !0, !1, nt ? c.split("").reverse() : c.split("")), n.click(), H(w()) === !0 && n.trigger("complete"), !1
      }

      function U(e) {
        var i = this;
        D(i, !0, !1), H(w()) === !0 && t(i).trigger("complete"), e.preventDefault()
      }

      function Q(t) {
        var e = this;
        Z = w().join(""), ("" == tt || 0 != t.originalEvent.data.indexOf(tt)) && (J = M(e))
      }

      function V(e) {
        var i = this,
          n = J || M(i);
        0 == e.originalEvent.data.indexOf(tt) && (h(), n = {
          begin: 0,
          end: 0
        });
        var o = e.originalEvent.data;
        M(i, n.begin, n.end);
        for (var r = 0; r < o.length; r++) {
          var a = t.Event("keypress");
          a.which = o.charCodeAt(r), ot = !1, rt = !1, G.call(i, a)
        }
        setTimeout(function () {
          var t = u().p;
          A(i, w(), s.numericInput ? j(t) : t)
        }, 0), tt = e.originalEvent.data
      }

      function K() {}

      function Y(e) {
        if (et = t(e), et.is(":input") && i(et.attr("type"))) {
          if (et.data("_inputmask", {
              maskset: o,
              opts: s,
              isRTL: !1
            }), s.showTooltip && et.prop("title", u().mask), ("rtl" == e.dir || s.rightAlign) && et.css("text-align", "right"), "rtl" == e.dir || s.numericInput) {
            e.dir = "ltr", et.removeAttr("dir");
            var n = et.data("_inputmask");
            n.isRTL = !0, et.data("_inputmask", n), nt = !0
          }
          et.unbind(".inputmask"), et.closest("form").bind("submit", function () {
            Z != w().join("") && et.change(), et[0]._valueGet && et[0]._valueGet() == y().join("") && et[0]._valueSet(""), s.removeMaskOnSubmit && et.inputmask("remove")
          }).bind("reset", function () {
            setTimeout(function () {
              et.triggerHandler("setvalue.inputmask")
            }, 0)
          }), et.bind("mouseenter.inputmask", function () {
            var e = t(this),
              i = this;
            !e.is(":focus") && s.showMaskOnHover && i._valueGet() != w().join("") && A(i, w())
          }).bind("blur.inputmask", function (e) {
            var i = t(this),
              n = this;
            if (i.data("_inputmask")) {
              var o = n._valueGet(),
                r = w().slice();
              at = !0, Z != r.join("") && setTimeout(function () {
                i.change(), Z = r.join("")
              }, 0), "" != o && (s.clearMaskOnLostFocus && (o == y().join("") ? r = [] : z(r)), H(r) === !1 && (i.trigger("incomplete"), s.clearIncomplete && (h(), r = s.clearMaskOnLostFocus ? [] : y().slice())), A(n, r, void 0, e))
            }
          }).bind("focus.inputmask", function () {
            var e = (t(this), this),
              i = e._valueGet();
            s.showMaskOnFocus && (!s.showMaskOnHover || s.showMaskOnHover && "" == i) && e._valueGet() != w().join("") && A(e, w(), E(d())), Z = w().join("")
          }).bind("mouseleave.inputmask", function () {
            var e = t(this),
              i = this;
            if (s.clearMaskOnLostFocus) {
              var n = w().slice(),
                o = i._valueGet();
              e.is(":focus") || o == e.attr("placeholder") || "" == o || (o == y().join("") ? n = [] : z(n), A(i, n))
            }
          }).bind("click.inputmask", function () {
            var e = t(this),
              i = this;
            if (e.is(":focus")) {
              var n = M(i);
              if (n.begin == n.end)
                if (s.radixFocus && "" != s.radixPoint && -1 != t.inArray(s.radixPoint, w()) && (at || w().join("") == y().join(""))) M(i, t.inArray(s.radixPoint, w())), at = !1;
                else {
                  var o = nt ? N(n.begin) : n.begin,
                    r = E(d(o));
                  r > o ? M(i, $(o) ? o : E(o)) : M(i, r)
                }
            }
          }).bind("dblclick.inputmask", function () {
            var t = this;
            setTimeout(function () {
              M(t, 0, E(d()))
            }, 0)
          }).bind(c + ".inputmask dragdrop.inputmask drop.inputmask", X).bind("setvalue.inputmask", function () {
            var t = this;
            D(t, !0, !1), Z = w().join(""), (s.clearMaskOnLostFocus || s.clearIncomplete) && t._valueGet() == y().join("") && t._valueSet("")
          }).bind("cut.inputmask", function (e) {
            st = !0;
            var i = this,
              n = t(i),
              o = M(i);
            q(i, t.inputmask.keyCode.DELETE, o), A(i, w(), u().p, e, Z != w().join("")), i._valueGet() == y().join("") && n.trigger("cleared"), s.showTooltip && n.prop("title", u().mask)
          }).bind("complete.inputmask", s.oncomplete).bind("incomplete.inputmask", s.onincomplete).bind("cleared.inputmask", s.oncleared), et.bind("keydown.inputmask", W).bind("keypress.inputmask", G), l || et.bind("compositionstart.inputmask", Q).bind("compositionupdate.inputmask", V).bind("compositionend.inputmask", K), "paste" === c && et.bind("input.inputmask", U), B(e);
          var r = t.isFunction(s.onBeforeMask) ? s.onBeforeMask.call(e, e._valueGet(), s) || e._valueGet() : e._valueGet();
          D(e, !0, !1, r.split(""));
          var a = w().slice();
          Z = a.join("");
          var p;
          try {
            p = document.activeElement
          } catch (t) {}
          H(a) === !1 && s.clearIncomplete && h(), s.clearMaskOnLostFocus && (a.join("") == y().join("") ? a = [] : z(a)), A(e, a), p === e && M(e, E(d())), F(e)
        }
      }
      var Z, J, tt, et, it, nt = !1,
        ot = !1,
        st = !1,
        rt = !1,
        at = !0;
      if (void 0 != n) switch (n.action) {
        case "isComplete":
          return et = t(n.el), o = et.data("_inputmask").maskset, s = et.data("_inputmask").opts, H(n.buffer);
        case "unmaskedvalue":
          return et = n.$input, o = et.data("_inputmask").maskset, s = et.data("_inputmask").opts, nt = n.$input.data("_inputmask").isRTL, I(n.$input);
        case "mask":
          Z = w().join(""), Y(n.el);
          break;
        case "format":
          et = t({}), et.data("_inputmask", {
            maskset: o,
            opts: s,
            isRTL: s.numericInput
          }), s.numericInput && (nt = !0);
          var lt = (t.isFunction(s.onBeforeMask) ? s.onBeforeMask.call(et, n.value, s) || n.value : n.value).split("");
          return D(et, !1, !1, nt ? lt.reverse() : lt), t.isFunction(s.onBeforeWrite) && s.onBeforeWrite.call(this, void 0, w(), 0, s), n.metadata ? {
            value: nt ? w().slice().reverse().join("") : w().join(""),
            metadata: et.inputmask("getmetadata")
          } : nt ? w().slice().reverse().join("") : w().join("");
        case "isValid":
          et = t({}), et.data("_inputmask", {
            maskset: o,
            opts: s,
            isRTL: s.numericInput
          }), s.numericInput && (nt = !0);
          var lt = n.value.split("");
          D(et, !1, !0, nt ? lt.reverse() : lt);
          for (var ct = w(), ut = L(), ht = ct.length - 1; ht > ut && !$(ht); ht--);
          return ct.splice(ut, ht + 1 - ut), H(ct) && n.value == ct.join("");
        case "getemptymask":
          return et = t(n.el), o = et.data("_inputmask").maskset, s = et.data("_inputmask").opts, y();
        case "remove":
          var dt = n.el;
          et = t(dt), o = et.data("_inputmask").maskset, s = et.data("_inputmask").opts, dt._valueSet(I(et)), et.unbind(".inputmask"), et.removeData("_inputmask");
          var pt;
          Object.getOwnPropertyDescriptor && (pt = Object.getOwnPropertyDescriptor(dt, "value")), pt && pt.get ? dt._valueGet && Object.defineProperty(dt, "value", {
            get: dt._valueGet,
            set: dt._valueSet
          }) : document.__lookupGetter__ && dt.__lookupGetter__("value") && dt._valueGet && (dt.__defineGetter__("value", dt._valueGet), dt.__defineSetter__("value", dt._valueSet));
          try {
            delete dt._valueGet, delete dt._valueSet
          } catch (t) {
            dt._valueGet = void 0, dt._valueSet = void 0
          }
          break;
        case "getmetadata":
          if (et = t(n.el), o = et.data("_inputmask").maskset, s = et.data("_inputmask").opts, t.isArray(o.metadata)) {
            for (var ft, gt = d(), mt = gt; mt >= 0; mt--)
              if (u().validPositions[mt] && void 0 != u().validPositions[mt].alternation) {
                ft = u().validPositions[mt].alternation;
                break
              }
            return void 0 != ft ? o.metadata[u().validPositions[gt].locator[ft]] : o.metadata[0]
          }
          return o.metadata
      }
    }
    if (void 0 === t.fn.inputmask) {
      var r = navigator.userAgent,
        a = null !== r.match(new RegExp("iphone", "i")),
        l = (null !== r.match(new RegExp("android.*safari.*", "i")), null !== r.match(new RegExp("android.*chrome.*", "i")), null !== r.match(new RegExp("android.*firefox.*", "i"))),
        c = (/Kindle/i.test(r) || /Silk/i.test(r) || /KFTT/i.test(r) || /KFOT/i.test(r) || /KFJWA/i.test(r) || /KFJWI/i.test(r) || /KFSOWI/i.test(r) || /KFTHWA/i.test(r) || /KFTHWI/i.test(r) || /KFAPWA/i.test(r) || /KFAPWI/i.test(r), e("paste") ? "paste" : e("input") ? "input" : "propertychange");
      t.inputmask = {
        defaults: {
          placeholder: "_",
          optionalmarker: {
            start: "[",
            end: "]"
          },
          quantifiermarker: {
            start: "{",
            end: "}"
          },
          groupmarker: {
            start: "(",
            end: ")"
          },
          alternatormarker: "|",
          escapeChar: "\\",
          mask: null,
          oncomplete: t.noop,
          onincomplete: t.noop,
          oncleared: t.noop,
          repeat: 0,
          greedy: !0,
          autoUnmask: !1,
          removeMaskOnSubmit: !1,
          clearMaskOnLostFocus: !0,
          insertMode: !0,
          clearIncomplete: !1,
          aliases: {},
          alias: null,
          onKeyDown: t.noop,
          onBeforeMask: void 0,
          onBeforePaste: void 0,
          onBeforeWrite: void 0,
          onUnMask: void 0,
          showMaskOnFocus: !0,
          showMaskOnHover: !0,
          onKeyValidation: t.noop,
          skipOptionalPartCharacter: " ",
          showTooltip: !1,
          numericInput: !1,
          rightAlign: !1,
          undoOnEscape: !0,
          radixPoint: "",
          radixFocus: !1,
          nojumps: !1,
          nojumpsThreshold: 0,
          keepStatic: void 0,
          definitions: {
            9: {
              validator: "[0-9]",
              cardinality: 1,
              definitionSymbol: "*"
            },
            a: {
              validator: "[A-Za-zА-яЁёÀ-ÿµ]",
              cardinality: 1,
              definitionSymbol: "*"
            },
            "*": {
              validator: "[0-9A-Za-zА-яЁёÀ-ÿµ]",
              cardinality: 1
            }
          },
          ignorables: [8, 9, 13, 19, 27, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 93, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123],
          isComplete: void 0,
          canClearPosition: t.noop,
          postValidation: void 0
        },
        keyCode: {
          ALT: 18,
          BACKSPACE: 8,
          CAPS_LOCK: 20,
          COMMA: 188,
          COMMAND: 91,
          COMMAND_LEFT: 91,
          COMMAND_RIGHT: 93,
          CONTROL: 17,
          DELETE: 46,
          DOWN: 40,
          END: 35,
          ENTER: 13,
          ESCAPE: 27,
          HOME: 36,
          INSERT: 45,
          LEFT: 37,
          MENU: 93,
          NUMPAD_ADD: 107,
          NUMPAD_DECIMAL: 110,
          NUMPAD_DIVIDE: 111,
          NUMPAD_ENTER: 108,
          NUMPAD_MULTIPLY: 106,
          NUMPAD_SUBTRACT: 109,
          PAGE_DOWN: 34,
          PAGE_UP: 33,
          PERIOD: 190,
          RIGHT: 39,
          SHIFT: 16,
          SPACE: 32,
          TAB: 9,
          UP: 38,
          WINDOWS: 91
        },
        masksCache: {},
        escapeRegex: function (t) {
          var e = ["/", ".", "*", "+", "?", "|", "(", ")", "[", "]", "{", "}", "\\", "$", "^"];
          return t.replace(new RegExp("(\\" + e.join("|\\") + ")", "gim"), "\\$1")
        },
        format: function (e, i, r) {
          var a = t.extend(!0, {}, t.inputmask.defaults, i);
          return n(a.alias, i, a), s({
            action: "format",
            value: e,
            metadata: r
          }, o(a), a)
        },
        isValid: function (e, i) {
          var r = t.extend(!0, {}, t.inputmask.defaults, i);
          return n(r.alias, i, r), s({
            action: "isValid",
            value: e
          }, o(r), r)
        }
      }, t.fn.inputmask = function (e, i) {
        function r(e, i, o) {
          var s = t(e);
          s.data("inputmask-alias") && n(s.data("inputmask-alias"), {}, i);
          for (var r in i) {
            var a = s.data("inputmask-" + r.toLowerCase());
            void 0 != a && ("mask" == r && 0 == a.indexOf("[") ? (i[r] = a.replace(/[\s[\]]/g, "").split("','"), i[r][0] = i[r][0].replace("'", ""), i[r][i[r].length - 1] = i[r][i[r].length - 1].replace("'", "")) : i[r] = "boolean" == typeof a ? a : a.toString(), o && (o[r] = i[r]))
          }
          return i
        }
        var a, l = t.extend(!0, {}, t.inputmask.defaults, i);
        if ("string" == typeof e) switch (e) {
          case "mask":
            return n(l.alias, i, l), a = o(l), void 0 == a ? this : this.each(function () {
              s({
                action: "mask",
                el: this
              }, t.extend(!0, {}, a), r(this, l))
            });
          case "unmaskedvalue":
            var c = t(this);
            return c.data("_inputmask") ? s({
              action: "unmaskedvalue",
              $input: c
            }) : c.val();
          case "remove":
            return this.each(function () {
              var e = t(this);
              e.data("_inputmask") && s({
                action: "remove",
                el: this
              })
            });
          case "getemptymask":
            return this.data("_inputmask") ? s({
              action: "getemptymask",
              el: this
            }) : "";
          case "hasMaskedValue":
            return !!this.data("_inputmask") && !this.data("_inputmask").opts.autoUnmask;
          case "isComplete":
            return !this.data("_inputmask") || s({
              action: "isComplete",
              buffer: this[0]._valueGet().split(""),
              el: this
            });
          case "getmetadata":
            return this.data("_inputmask") ? s({
              action: "getmetadata",
              el: this
            }) : void 0;
          default:
            return n(l.alias, i, l), n(e, i, l) || (l.mask = e), a = o(l), void 0 == a ? this : this.each(function () {
              s({
                action: "mask",
                el: this
              }, t.extend(!0, {}, a), r(this, l))
            })
        } else {
          if ("object" == typeof e) return l = t.extend(!0, {}, t.inputmask.defaults, e), n(l.alias, e, l), a = o(l), void 0 == a ? this : this.each(function () {
            s({
              action: "mask",
              el: this
            }, t.extend(!0, {}, a), r(this, l))
          });
          if (void 0 == e) return this.each(function () {
            var e = t(this).attr("data-inputmask");
            if (e && "" != e) try {
              e = e.replace(new RegExp("'", "g"), '"');
              var o = t.parseJSON("{" + e + "}");
              t.extend(!0, o, i), l = t.extend(!0, {}, t.inputmask.defaults, o), l = r(this, l), n(l.alias, o, l), l.alias = void 0, t(this).inputmask("mask", l)
            } catch (t) {}
            if (t(this).attr("data-inputmask-mask") || t(this).attr("data-inputmask-alias")) {
              l = t.extend(!0, {}, t.inputmask.defaults, {});
              var s = {};
              l = r(this, l, s), n(l.alias, s, l), l.alias = void 0, t(this).inputmask("mask", l)
            }
          })
        }
      }
    }
    return t.fn.inputmask
  }(jQuery),
  function (t, e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e(require("jquery")) : t.lightbox = e(t.jQuery)
  }(this, function (t) {
    function e(e) {
      this.album = [], this.currentImageIndex = void 0, this.init(), this.options = t.extend({}, this.constructor.defaults), this.option(e)
    }
    return e.defaults = {
      albumLabel: "Image %1 of %2",
      alwaysShowNavOnTouchDevices: !1,
      fadeDuration: 600,
      fitImagesInViewport: !0,
      imageFadeDuration: 600,
      positionFromTop: 50,
      resizeDuration: 700,
      showImageNumberLabel: !0,
      wrapAround: !1,
      disableScrolling: !1,
      sanitizeTitle: !1
    }, e.prototype.option = function (e) {
      t.extend(this.options, e)
    }, e.prototype.imageCountLabel = function (t, e) {
      return this.options.albumLabel.replace(/%1/g, t).replace(/%2/g, e)
    }, e.prototype.init = function () {
      var e = this;
      t(document).ready(function () {
        e.enable(), e.build()
      })
    }, e.prototype.enable = function () {
      var e = this;
      t("body").on("click", "a[rel^=lightbox], area[rel^=lightbox], a[data-lightbox], area[data-lightbox]", function (i) {
        return e.start(t(i.currentTarget)), !1
      })
    }, e.prototype.build = function () {
      var e = this;
      t('<div id="lightboxOverlay" class="lightboxOverlay"></div><div id="lightbox" class="lightbox"><div class="lb-outerContainer"><div class="lb-container"><img class="lb-image" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" /><div class="lb-nav"><a class="lb-prev" href="" ></a><a class="lb-next" href="" ></a></div><div class="lb-loader"><a class="lb-cancel"></a></div></div></div><div class="lb-dataContainer"><div class="lb-data"><div class="lb-details"><span class="lb-caption"></span><span class="lb-number"></span></div><div class="lb-closeContainer"><a class="lb-close"></a></div></div></div></div>').appendTo(t("body")), this.$lightbox = t("#lightbox"), this.$overlay = t("#lightboxOverlay"), this.$outerContainer = this.$lightbox.find(".lb-outerContainer"), this.$container = this.$lightbox.find(".lb-container"), this.$image = this.$lightbox.find(".lb-image"), this.$nav = this.$lightbox.find(".lb-nav"), this.containerPadding = {
        top: parseInt(this.$container.css("padding-top"), 10),
        right: parseInt(this.$container.css("padding-right"), 10),
        bottom: parseInt(this.$container.css("padding-bottom"), 10),
        left: parseInt(this.$container.css("padding-left"), 10)
      }, this.imageBorderWidth = {
        top: parseInt(this.$image.css("border-top-width"), 10),
        right: parseInt(this.$image.css("border-right-width"), 10),
        bottom: parseInt(this.$image.css("border-bottom-width"), 10),
        left: parseInt(this.$image.css("border-left-width"), 10)
      }, this.$overlay.hide().on("click", function () {
        return e.end(), !1
      }), this.$lightbox.hide().on("click", function (i) {
        return "lightbox" === t(i.target).attr("id") && e.end(), !1
      }), this.$outerContainer.on("click", function (i) {
        return "lightbox" === t(i.target).attr("id") && e.end(), !1
      }), this.$lightbox.find(".lb-prev").on("click", function () {
        return 0 === e.currentImageIndex ? e.changeImage(e.album.length - 1) : e.changeImage(e.currentImageIndex - 1), !1
      }), this.$lightbox.find(".lb-next").on("click", function () {
        return e.currentImageIndex === e.album.length - 1 ? e.changeImage(0) : e.changeImage(e.currentImageIndex + 1), !1
      }), this.$nav.on("mousedown", function (t) {
        3 === t.which && (e.$nav.css("pointer-events", "none"), e.$lightbox.one("contextmenu", function () {
          setTimeout(function () {
            this.$nav.css("pointer-events", "auto")
          }.bind(e), 0)
        }))
      }), this.$lightbox.find(".lb-loader, .lb-close").on("click", function () {
        return e.end(), !1
      })
    }, e.prototype.start = function (e) {
      function i(t) {
        n.album.push({
          link: t.attr("href"),
          title: t.attr("data-title") || t.attr("title")
        })
      }
      var n = this,
        o = t(window);
      o.on("resize", t.proxy(this.sizeOverlay, this)), t("select, object, embed").css({
        visibility: "hidden"
      }), this.sizeOverlay(), this.album = [];
      var s, r = 0,
        a = e.attr("data-lightbox");
      if (a) {
        s = t(e.prop("tagName") + '[data-lightbox="' + a + '"]');
        for (var l = 0; l < s.length; l = ++l) i(t(s[l])), s[l] === e[0] && (r = l)
      } else if ("lightbox" === e.attr("rel")) i(e);
      else {
        s = t(e.prop("tagName") + '[rel="' + e.attr("rel") + '"]');
        for (var c = 0; c < s.length; c = ++c) i(t(s[c])), s[c] === e[0] && (r = c)
      }
      var u = o.scrollTop() + this.options.positionFromTop,
        h = o.scrollLeft();
      this.$lightbox.css({
        top: u + "px",
        left: h + "px"
      }).fadeIn(this.options.fadeDuration), this.options.disableScrolling && t("body").addClass("lb-disable-scrolling"), this.changeImage(r)
    }, e.prototype.changeImage = function (e) {
      var i = this;
      this.disableKeyboardNav();
      var n = this.$lightbox.find(".lb-image");
      this.$overlay.fadeIn(this.options.fadeDuration), t(".lb-loader").fadeIn("slow"), this.$lightbox.find(".lb-image, .lb-nav, .lb-prev, .lb-next, .lb-dataContainer, .lb-numbers, .lb-caption").hide(), this.$outerContainer.addClass("animating");
      var o = new Image;
      o.onload = function () {
        var s, r, a, l, c, u, h;
        n.attr("src", i.album[e].link), s = t(o), n.width(o.width), n.height(o.height), i.options.fitImagesInViewport && (h = t(window).width(), u = t(window).height(), c = h - i.containerPadding.left - i.containerPadding.right - i.imageBorderWidth.left - i.imageBorderWidth.right - 20, l = u - i.containerPadding.top - i.containerPadding.bottom - i.imageBorderWidth.top - i.imageBorderWidth.bottom - 120, i.options.maxWidth && i.options.maxWidth < c && (c = i.options.maxWidth), i.options.maxHeight && i.options.maxHeight < c && (l = i.options.maxHeight), (o.width > c || o.height > l) && (o.width / c > o.height / l ? (a = c, r = parseInt(o.height / (o.width / a), 10), n.width(a), n.height(r)) : (r = l, a = parseInt(o.width / (o.height / r), 10), n.width(a), n.height(r)))), i.sizeContainer(n.width(), n.height())
      }, o.src = this.album[e].link, this.currentImageIndex = e
    }, e.prototype.sizeOverlay = function () {
      this.$overlay.width(t(document).width()).height(t(document).height())
    }, e.prototype.sizeContainer = function (t, e) {
      function i() {
        n.$lightbox.find(".lb-dataContainer").width(r), n.$lightbox.find(".lb-prevLink").height(a), n.$lightbox.find(".lb-nextLink").height(a), n.showImage()
      }
      var n = this,
        o = this.$outerContainer.outerWidth(),
        s = this.$outerContainer.outerHeight(),
        r = t + this.containerPadding.left + this.containerPadding.right + this.imageBorderWidth.left + this.imageBorderWidth.right,
        a = e + this.containerPadding.top + this.containerPadding.bottom + this.imageBorderWidth.top + this.imageBorderWidth.bottom;
      o !== r || s !== a ? this.$outerContainer.animate({
        width: r,
        height: a
      }, this.options.resizeDuration, "swing", function () {
        i()
      }) : i()
    }, e.prototype.showImage = function () {
      this.$lightbox.find(".lb-loader").stop(!0).hide(), this.$lightbox.find(".lb-image").fadeIn(this.options.imageFadeDuration), this.updateNav(), this.updateDetails(), this.preloadNeighboringImages(), this.enableKeyboardNav()
    }, e.prototype.updateNav = function () {
      var t = !1;
      try {
        document.createEvent("TouchEvent"), t = !!this.options.alwaysShowNavOnTouchDevices
      } catch (t) {}
      this.$lightbox.find(".lb-nav").show(), this.album.length > 1 && (this.options.wrapAround ? (t && this.$lightbox.find(".lb-prev, .lb-next").css("opacity", "1"), this.$lightbox.find(".lb-prev, .lb-next").show()) : (this.currentImageIndex > 0 && (this.$lightbox.find(".lb-prev").show(), t && this.$lightbox.find(".lb-prev").css("opacity", "1")), this.currentImageIndex < this.album.length - 1 && (this.$lightbox.find(".lb-next").show(), t && this.$lightbox.find(".lb-next").css("opacity", "1"))))
    }, e.prototype.updateDetails = function () {
      var e = this;
      if ("undefined" != typeof this.album[this.currentImageIndex].title && "" !== this.album[this.currentImageIndex].title) {
        var i = this.$lightbox.find(".lb-caption");
        this.options.sanitizeTitle ? i.text(this.album[this.currentImageIndex].title) : i.html(this.album[this.currentImageIndex].title), i.fadeIn("fast").find("a").on("click", function (e) {
          void 0 !== t(this).attr("target") ? window.open(t(this).attr("href"), t(this).attr("target")) : location.href = t(this).attr("href")
        })
      }
      if (this.album.length > 1 && this.options.showImageNumberLabel) {
        var n = this.imageCountLabel(this.currentImageIndex + 1, this.album.length);
        this.$lightbox.find(".lb-number").text(n).fadeIn("fast")
      } else this.$lightbox.find(".lb-number").hide();
      this.$outerContainer.removeClass("animating"), this.$lightbox.find(".lb-dataContainer").fadeIn(this.options.resizeDuration, function () {
        return e.sizeOverlay()
      })
    }, e.prototype.preloadNeighboringImages = function () {
      if (this.album.length > this.currentImageIndex + 1) {
        var t = new Image;
        t.src = this.album[this.currentImageIndex + 1].link
      }
      if (this.currentImageIndex > 0) {
        var e = new Image;
        e.src = this.album[this.currentImageIndex - 1].link
      }
    }, e.prototype.enableKeyboardNav = function () {
      t(document).on("keyup.keyboard", t.proxy(this.keyboardAction, this))
    }, e.prototype.disableKeyboardNav = function () {
      t(document).off(".keyboard")
    }, e.prototype.keyboardAction = function (t) {
      var e = 27,
        i = 37,
        n = 39,
        o = t.keyCode,
        s = String.fromCharCode(o).toLowerCase();
      o === e || s.match(/x|o|c/) ? this.end() : "p" === s || o === i ? 0 !== this.currentImageIndex ? this.changeImage(this.currentImageIndex - 1) : this.options.wrapAround && this.album.length > 1 && this.changeImage(this.album.length - 1) : "n" !== s && o !== n || (this.currentImageIndex !== this.album.length - 1 ? this.changeImage(this.currentImageIndex + 1) : this.options.wrapAround && this.album.length > 1 && this.changeImage(0))
    }, e.prototype.end = function () {
      this.disableKeyboardNav(), t(window).off("resize", this.sizeOverlay), this.$lightbox.fadeOut(this.options.fadeDuration), this.$overlay.fadeOut(this.options.fadeDuration), t("select, object, embed").css({
        visibility: "visible"
      }), this.options.disableScrolling && t("body").removeClass("lb-disable-scrolling")
    }, new e
  }), ! function (t, e) {
    "function" == typeof define && define.amd ? define(["jquery"], function (t) {
      return e(t)
    }) : "object" == typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
  }(this, function (t) {
    function e(t) {
      this.$container, this.constraints = null, this.__$tooltip, this.__init(t)
    }

    function i(e, i) {
      var n = !0;
      return t.each(e, function (t, o) {
        return void 0 === i[t] || e[t] !== i[t] ? (n = !1, !1) : void 0
      }), n
    }

    function n(e) {
      var i = e.attr("id"),
        n = i ? a.window.document.getElementById(i) : null;
      return n ? n === e[0] : t.contains(a.window.document.body, e[0])
    }

    function o() {
      if (!r) return !1;
      var t = r.document.body || r.document.documentElement,
        e = t.style,
        i = "transition",
        n = ["Moz", "Webkit", "Khtml", "O", "ms"];
      if ("string" == typeof e[i]) return !0;
      i = i.charAt(0).toUpperCase() + i.substr(1);
      for (var o = 0; o < n.length; o++)
        if ("string" == typeof e[n[o] + i]) return !0;
      return !1
    }
    var s = {
        animation: "fade",
        animationDuration: 350,
        content: null,
        contentAsHTML: !1,
        contentCloning: !1,
        debug: !0,
        delay: 300,
        delayTouch: [300, 500],
        functionInit: null,
        functionBefore: null,
        functionReady: null,
        functionAfter: null,
        functionFormat: null,
        IEmin: 6,
        interactive: !1,
        multiple: !1,
        parent: "body",
        plugins: ["sideTip"],
        repositionOnScroll: !1,
        restoration: "none",
        selfDestruction: !0,
        theme: [],
        timer: 0,
        trackerInterval: 500,
        trackOrigin: !1,
        trackTooltip: !1,
        trigger: "hover",
        triggerClose: {
          click: !1,
          mouseleave: !1,
          originClick: !1,
          scroll: !1,
          tap: !1,
          touchleave: !1
        },
        triggerOpen: {
          click: !1,
          mouseenter: !1,
          tap: !1,
          touchstart: !1
        },
        updateAnimation: "rotate",
        zIndex: 9999999
      },
      r = "undefined" != typeof window ? window : null,
      a = {
        hasTouchCapability: !(!r || !("ontouchstart" in r || r.DocumentTouch && r.document instanceof r.DocumentTouch || r.navigator.maxTouchPoints)),
        hasTransitions: o(),
        IE: !1,
        semVer: "4.1.6",
        window: r
      },
      l = function () {
        this.__$emitterPrivate = t({}), this.__$emitterPublic = t({}), this.__instancesLatestArr = [], this.__plugins = {}, this._env = a
      };
    l.prototype = {
      __bridge: function (e, i, n) {
        if (!i[n]) {
          var o = function () {};
          o.prototype = e;
          var r = new o;
          r.__init && r.__init(i), t.each(e, function (t, e) {
            0 != t.indexOf("__") && (i[t] ? s.debug && console.log("The " + t + " method of the " + n + " plugin conflicts with another plugin or native methods") : (i[t] = function () {
              return r[t].apply(r, Array.prototype.slice.apply(arguments))
            }, i[t].bridged = r))
          }), i[n] = r
        }
        return this
      },
      __setWindow: function (t) {
        return a.window = t, this
      },
      _getRuler: function (t) {
        return new e(t)
      },
      _off: function () {
        return this.__$emitterPrivate.off.apply(this.__$emitterPrivate, Array.prototype.slice.apply(arguments)), this
      },
      _on: function () {
        return this.__$emitterPrivate.on.apply(this.__$emitterPrivate, Array.prototype.slice.apply(arguments)), this
      },
      _one: function () {
        return this.__$emitterPrivate.one.apply(this.__$emitterPrivate, Array.prototype.slice.apply(arguments)),
          this
      },
      _plugin: function (e) {
        var i = this;
        if ("string" == typeof e) {
          var n = e,
            o = null;
          return n.indexOf(".") > 0 ? o = i.__plugins[n] : t.each(i.__plugins, function (t, e) {
            return e.name.substring(e.name.length - n.length - 1) == "." + n ? (o = e, !1) : void 0
          }), o
        }
        if (e.name.indexOf(".") < 0) throw new Error("Plugins must be namespaced");
        return i.__plugins[e.name] = e, e.core && i.__bridge(e.core, i, e.name), this
      },
      _trigger: function () {
        var t = Array.prototype.slice.apply(arguments);
        return "string" == typeof t[0] && (t[0] = {
          type: t[0]
        }), this.__$emitterPrivate.trigger.apply(this.__$emitterPrivate, t), this.__$emitterPublic.trigger.apply(this.__$emitterPublic, t), this
      },
      instances: function (e) {
        var i = [],
          n = e || ".tooltipstered";
        return t(n).each(function () {
          var e = t(this),
            n = e.data("tooltipster-ns");
          n && t.each(n, function (t, n) {
            i.push(e.data(n))
          })
        }), i
      },
      instancesLatest: function () {
        return this.__instancesLatestArr
      },
      off: function () {
        return this.__$emitterPublic.off.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
      },
      on: function () {
        return this.__$emitterPublic.on.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
      },
      one: function () {
        return this.__$emitterPublic.one.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
      },
      origins: function (e) {
        var i = e ? e + " " : "";
        return t(i + ".tooltipstered").toArray()
      },
      setDefaults: function (e) {
        return t.extend(s, e), this
      },
      triggerHandler: function () {
        return this.__$emitterPublic.triggerHandler.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
      }
    }, t.tooltipster = new l, t.Tooltipster = function (e, i) {
      this.__callbacks = {
        close: [],
        open: []
      }, this.__closingTime, this.__Content, this.__contentBcr, this.__destroyed = !1, this.__destroying = !1, this.__$emitterPrivate = t({}), this.__$emitterPublic = t({}), this.__enabled = !0, this.__garbageCollector, this.__Geometry, this.__lastPosition, this.__namespace = "tooltipster-" + Math.round(1e6 * Math.random()), this.__options, this.__$originParents, this.__pointerIsOverOrigin = !1, this.__previousThemes = [], this.__state = "closed", this.__timeouts = {
        close: [],
        open: null
      }, this.__touchEvents = [], this.__tracker = null, this._$origin, this._$tooltip, this.__init(e, i)
    }, t.Tooltipster.prototype = {
      __init: function (e, i) {
        var n = this;
        if (n._$origin = t(e), n.__options = t.extend(!0, {}, s, i), n.__optionsFormat(), !a.IE || a.IE >= n.__options.IEmin) {
          var o = null;
          if (void 0 === n._$origin.data("tooltipster-initialTitle") && (o = n._$origin.attr("title"), void 0 === o && (o = null), n._$origin.data("tooltipster-initialTitle", o)), null !== n.__options.content) n.__contentSet(n.__options.content);
          else {
            var r, l = n._$origin.attr("data-tooltip-content");
            l && (r = t(l)), r && r[0] ? n.__contentSet(r.first()) : n.__contentSet(o)
          }
          n._$origin.removeAttr("title").addClass("tooltipstered"), n.__prepareOrigin(), n.__prepareGC(), t.each(n.__options.plugins, function (t, e) {
            n._plug(e)
          }), a.hasTouchCapability && t("body").on("touchmove." + n.__namespace + "-triggerOpen", function (t) {
            n._touchRecordEvent(t)
          }), n._on("created", function () {
            n.__prepareTooltip()
          })._on("repositioned", function (t) {
            n.__lastPosition = t.position
          })
        } else n.__options.disabled = !0
      },
      __contentInsert: function () {
        var t = this,
          e = t._$tooltip.find(".tooltipster-content"),
          i = t.__Content,
          n = function (t) {
            i = t
          };
        return t._trigger({
          type: "format",
          content: t.__Content,
          format: n
        }), t.__options.functionFormat && (i = t.__options.functionFormat.call(t, t, {
          origin: t._$origin[0]
        }, t.__Content)), "string" != typeof i || t.__options.contentAsHTML ? e.empty().append(i) : e.text(i), t
      },
      __contentSet: function (e) {
        return e instanceof t && this.__options.contentCloning && (e = e.clone(!0)), this.__Content = e, this._trigger({
          type: "updated",
          content: e
        }), this
      },
      __destroyError: function () {
        throw new Error("This tooltip has been destroyed and cannot execute your method call.")
      },
      __geometry: function () {
        var e = this,
          i = e._$origin,
          n = e._$origin.is("area");
        if (n) {
          var o = e._$origin.parent().attr("name");
          i = t('img[usemap="#' + o + '"]')
        }
        var s = i[0].getBoundingClientRect(),
          r = t(a.window.document),
          l = t(a.window),
          c = i,
          u = {
            available: {
              document: null,
              window: null
            },
            document: {
              size: {
                height: r.height(),
                width: r.width()
              }
            },
            window: {
              scroll: {
                left: a.window.scrollX || a.window.document.documentElement.scrollLeft,
                top: a.window.scrollY || a.window.document.documentElement.scrollTop
              },
              size: {
                height: l.height(),
                width: l.width()
              }
            },
            origin: {
              fixedLineage: !1,
              offset: {},
              size: {
                height: s.bottom - s.top,
                width: s.right - s.left
              },
              usemapImage: n ? i[0] : null,
              windowOffset: {
                bottom: s.bottom,
                left: s.left,
                right: s.right,
                top: s.top
              }
            }
          };
        if (n) {
          var h = e._$origin.attr("shape"),
            d = e._$origin.attr("coords");
          if (d && (d = d.split(","), t.map(d, function (t, e) {
              d[e] = parseInt(t)
            })), "default" != h) switch (h) {
            case "circle":
              var p = d[0],
                f = d[1],
                g = d[2],
                m = f - g,
                _ = p - g;
              u.origin.size.height = 2 * g, u.origin.size.width = u.origin.size.height, u.origin.windowOffset.left += _, u.origin.windowOffset.top += m;
              break;
            case "rect":
              var v = d[0],
                y = d[1],
                w = d[2],
                b = d[3];
              u.origin.size.height = b - y, u.origin.size.width = w - v, u.origin.windowOffset.left += v, u.origin.windowOffset.top += y;
              break;
            case "poly":
              for (var x = 0, k = 0, C = 0, $ = 0, T = "even", E = 0; E < d.length; E++) {
                var j = d[E];
                "even" == T ? (j > C && (C = j, 0 === E && (x = C)), x > j && (x = j), T = "odd") : (j > $ && ($ = j, 1 == E && (k = $)), k > j && (k = j), T = "even")
              }
              u.origin.size.height = $ - k, u.origin.size.width = C - x, u.origin.windowOffset.left += x, u.origin.windowOffset.top += k
          }
        }
        var S = function (t) {
          u.origin.size.height = t.height, u.origin.windowOffset.left = t.left, u.origin.windowOffset.top = t.top, u.origin.size.width = t.width
        };
        for (e._trigger({
            type: "geometry",
            edit: S,
            geometry: {
              height: u.origin.size.height,
              left: u.origin.windowOffset.left,
              top: u.origin.windowOffset.top,
              width: u.origin.size.width
            }
          }), u.origin.windowOffset.right = u.origin.windowOffset.left + u.origin.size.width, u.origin.windowOffset.bottom = u.origin.windowOffset.top + u.origin.size.height, u.origin.offset.left = u.origin.windowOffset.left + u.window.scroll.left, u.origin.offset.top = u.origin.windowOffset.top + u.window.scroll.top, u.origin.offset.bottom = u.origin.offset.top + u.origin.size.height, u.origin.offset.right = u.origin.offset.left + u.origin.size.width, u.available.document = {
            bottom: {
              height: u.document.size.height - u.origin.offset.bottom,
              width: u.document.size.width
            },
            left: {
              height: u.document.size.height,
              width: u.origin.offset.left
            },
            right: {
              height: u.document.size.height,
              width: u.document.size.width - u.origin.offset.right
            },
            top: {
              height: u.origin.offset.top,
              width: u.document.size.width
            }
          }, u.available.window = {
            bottom: {
              height: Math.max(u.window.size.height - Math.max(u.origin.windowOffset.bottom, 0), 0),
              width: u.window.size.width
            },
            left: {
              height: u.window.size.height,
              width: Math.max(u.origin.windowOffset.left, 0)
            },
            right: {
              height: u.window.size.height,
              width: Math.max(u.window.size.width - Math.max(u.origin.windowOffset.right, 0), 0)
            },
            top: {
              height: Math.max(u.origin.windowOffset.top, 0),
              width: u.window.size.width
            }
          };
          "html" != c[0].tagName.toLowerCase();) {
          if ("fixed" == c.css("position")) {
            u.origin.fixedLineage = !0;
            break
          }
          c = c.parent()
        }
        return u
      },
      __optionsFormat: function () {
        return "number" == typeof this.__options.animationDuration && (this.__options.animationDuration = [this.__options.animationDuration, this.__options.animationDuration]), "number" == typeof this.__options.delay && (this.__options.delay = [this.__options.delay, this.__options.delay]), "number" == typeof this.__options.delayTouch && (this.__options.delayTouch = [this.__options.delayTouch, this.__options.delayTouch]), "string" == typeof this.__options.theme && (this.__options.theme = [this.__options.theme]), "string" == typeof this.__options.parent && (this.__options.parent = t(this.__options.parent)), "hover" == this.__options.trigger ? (this.__options.triggerOpen = {
          mouseenter: !0,
          touchstart: !0
        }, this.__options.triggerClose = {
          mouseleave: !0,
          originClick: !0,
          touchleave: !0
        }) : "click" == this.__options.trigger && (this.__options.triggerOpen = {
          click: !0,
          tap: !0
        }, this.__options.triggerClose = {
          click: !0,
          tap: !0
        }), this._trigger("options"), this
      },
      __prepareGC: function () {
        var e = this;
        return e.__options.selfDestruction ? e.__garbageCollector = setInterval(function () {
          var i = (new Date).getTime();
          e.__touchEvents = t.grep(e.__touchEvents, function (t, e) {
            return i - t.time > 6e4
          }), n(e._$origin) || e.destroy()
        }, 2e4) : clearInterval(e.__garbageCollector), e
      },
      __prepareOrigin: function () {
        var t = this;
        if (t._$origin.off("." + t.__namespace + "-triggerOpen"), a.hasTouchCapability && t._$origin.on("touchstart." + t.__namespace + "-triggerOpen touchend." + t.__namespace + "-triggerOpen touchcancel." + t.__namespace + "-triggerOpen", function (e) {
            t._touchRecordEvent(e)
          }), t.__options.triggerOpen.click || t.__options.triggerOpen.tap && a.hasTouchCapability) {
          var e = "";
          t.__options.triggerOpen.click && (e += "click." + t.__namespace + "-triggerOpen "), t.__options.triggerOpen.tap && a.hasTouchCapability && (e += "touchend." + t.__namespace + "-triggerOpen"), t._$origin.on(e, function (e) {
            t._touchIsMeaningfulEvent(e) && t._open(e)
          })
        }
        if (t.__options.triggerOpen.mouseenter || t.__options.triggerOpen.touchstart && a.hasTouchCapability) {
          var e = "";
          t.__options.triggerOpen.mouseenter && (e += "mouseenter." + t.__namespace + "-triggerOpen "), t.__options.triggerOpen.touchstart && a.hasTouchCapability && (e += "touchstart." + t.__namespace + "-triggerOpen"), t._$origin.on(e, function (e) {
            !t._touchIsTouchEvent(e) && t._touchIsEmulatedEvent(e) || (t.__pointerIsOverOrigin = !0, t._openShortly(e))
          })
        }
        if (t.__options.triggerClose.mouseleave || t.__options.triggerClose.touchleave && a.hasTouchCapability) {
          var e = "";
          t.__options.triggerClose.mouseleave && (e += "mouseleave." + t.__namespace + "-triggerOpen "), t.__options.triggerClose.touchleave && a.hasTouchCapability && (e += "touchend." + t.__namespace + "-triggerOpen touchcancel." + t.__namespace + "-triggerOpen"), t._$origin.on(e, function (e) {
            t._touchIsMeaningfulEvent(e) && (t.__pointerIsOverOrigin = !1)
          })
        }
        return t
      },
      __prepareTooltip: function () {
        var e = this,
          i = e.__options.interactive ? "auto" : "";
        return e._$tooltip.attr("id", e.__namespace).css({
          "pointer-events": i,
          zIndex: e.__options.zIndex
        }), t.each(e.__previousThemes, function (t, i) {
          e._$tooltip.removeClass(i)
        }), t.each(e.__options.theme, function (t, i) {
          e._$tooltip.addClass(i)
        }), e.__previousThemes = t.merge([], e.__options.theme), e
      },
      __scrollHandler: function (e) {
        var i = this;
        if (i.__options.triggerClose.scroll) i._close(e);
        else {
          if (e.target === a.window.document) i.__Geometry.origin.fixedLineage || i.__options.repositionOnScroll && i.reposition(e);
          else {
            var n = i.__geometry(),
              o = !1;
            if ("fixed" != i._$origin.css("position") && i.__$originParents.each(function (e, i) {
                var s = t(i),
                  r = s.css("overflow-x"),
                  a = s.css("overflow-y");
                if ("visible" != r || "visible" != a) {
                  var l = i.getBoundingClientRect();
                  if ("visible" != r && (n.origin.windowOffset.left < l.left || n.origin.windowOffset.right > l.right)) return o = !0, !1;
                  if ("visible" != a && (n.origin.windowOffset.top < l.top || n.origin.windowOffset.bottom > l.bottom)) return o = !0, !1
                }
                return "fixed" != s.css("position") && void 0
              }), o) i._$tooltip.css("visibility", "hidden");
            else if (i._$tooltip.css("visibility", "visible"), i.__options.repositionOnScroll) i.reposition(e);
            else {
              var s = n.origin.offset.left - i.__Geometry.origin.offset.left,
                r = n.origin.offset.top - i.__Geometry.origin.offset.top;
              i._$tooltip.css({
                left: i.__lastPosition.coord.left + s,
                top: i.__lastPosition.coord.top + r
              })
            }
          }
          i._trigger({
            type: "scroll",
            event: e
          })
        }
        return i
      },
      __stateSet: function (t) {
        return this.__state = t, this._trigger({
          type: "state",
          state: t
        }), this
      },
      __timeoutsClear: function () {
        return clearTimeout(this.__timeouts.open), this.__timeouts.open = null, t.each(this.__timeouts.close, function (t, e) {
          clearTimeout(e)
        }), this.__timeouts.close = [], this
      },
      __trackerStart: function () {
        var t = this,
          e = t._$tooltip.find(".tooltipster-content");
        return t.__options.trackTooltip && (t.__contentBcr = e[0].getBoundingClientRect()), t.__tracker = setInterval(function () {
          if (n(t._$origin) && n(t._$tooltip)) {
            if (t.__options.trackOrigin) {
              var o = t.__geometry(),
                s = !1;
              i(o.origin.size, t.__Geometry.origin.size) && (t.__Geometry.origin.fixedLineage ? i(o.origin.windowOffset, t.__Geometry.origin.windowOffset) && (s = !0) : i(o.origin.offset, t.__Geometry.origin.offset) && (s = !0)), s || (t.__options.triggerClose.mouseleave ? t._close() : t.reposition())
            }
            if (t.__options.trackTooltip) {
              var r = e[0].getBoundingClientRect();
              r.height === t.__contentBcr.height && r.width === t.__contentBcr.width || (t.reposition(), t.__contentBcr = r)
            }
          } else t._close()
        }, t.__options.trackerInterval), t
      },
      _close: function (e, i) {
        var n = this,
          o = !0;
        if (n._trigger({
            type: "close",
            event: e,
            stop: function () {
              o = !1
            }
          }), o || n.__destroying) {
          i && n.__callbacks.close.push(i), n.__callbacks.open = [], n.__timeoutsClear();
          var s = function () {
            t.each(n.__callbacks.close, function (t, i) {
              i.call(n, n, {
                event: e,
                origin: n._$origin[0]
              })
            }), n.__callbacks.close = []
          };
          if ("closed" != n.__state) {
            var r = !0,
              l = new Date,
              c = l.getTime(),
              u = c + n.__options.animationDuration[1];
            if ("disappearing" == n.__state && u > n.__closingTime && (r = !1), r) {
              n.__closingTime = u, "disappearing" != n.__state && n.__stateSet("disappearing");
              var h = function () {
                clearInterval(n.__tracker), n._trigger({
                  type: "closing",
                  event: e
                }), n._$tooltip.off("." + n.__namespace + "-triggerClose").removeClass("tooltipster-dying"), t(a.window).off("." + n.__namespace + "-triggerClose"), n.__$originParents.each(function (e, i) {
                  t(i).off("scroll." + n.__namespace + "-triggerClose")
                }), n.__$originParents = null, t("body").off("." + n.__namespace + "-triggerClose"), n._$origin.off("." + n.__namespace + "-triggerClose"), n._off("dismissable"), n.__stateSet("closed"), n._trigger({
                  type: "after",
                  event: e
                }), n.__options.functionAfter && n.__options.functionAfter.call(n, n, {
                  event: e,
                  origin: n._$origin[0]
                }), s()
              };
              a.hasTransitions ? (n._$tooltip.css({
                "-moz-animation-duration": n.__options.animationDuration[1] + "ms",
                "-ms-animation-duration": n.__options.animationDuration[1] + "ms",
                "-o-animation-duration": n.__options.animationDuration[1] + "ms",
                "-webkit-animation-duration": n.__options.animationDuration[1] + "ms",
                "animation-duration": n.__options.animationDuration[1] + "ms",
                "transition-duration": n.__options.animationDuration[1] + "ms"
              }), n._$tooltip.clearQueue().removeClass("tooltipster-show").addClass("tooltipster-dying"), n.__options.animationDuration[1] > 0 && n._$tooltip.delay(n.__options.animationDuration[1]), n._$tooltip.queue(h)) : n._$tooltip.stop().fadeOut(n.__options.animationDuration[1], h)
            }
          } else s()
        }
        return n
      },
      _off: function () {
        return this.__$emitterPrivate.off.apply(this.__$emitterPrivate, Array.prototype.slice.apply(arguments)), this
      },
      _on: function () {
        return this.__$emitterPrivate.on.apply(this.__$emitterPrivate, Array.prototype.slice.apply(arguments)), this
      },
      _one: function () {
        return this.__$emitterPrivate.one.apply(this.__$emitterPrivate, Array.prototype.slice.apply(arguments)), this
      },
      _open: function (e, i) {
        var o = this;
        if (!o.__destroying && n(o._$origin) && o.__enabled) {
          var s = !0;
          if ("closed" == o.__state && (o._trigger({
              type: "before",
              event: e,
              stop: function () {
                s = !1
              }
            }), s && o.__options.functionBefore && (s = o.__options.functionBefore.call(o, o, {
              event: e,
              origin: o._$origin[0]
            }))), s !== !1 && null !== o.__Content) {
            i && o.__callbacks.open.push(i), o.__callbacks.close = [], o.__timeoutsClear();
            var r, l = function () {
              "stable" != o.__state && o.__stateSet("stable"), t.each(o.__callbacks.open, function (t, e) {
                e.call(o, o, {
                  origin: o._$origin[0],
                  tooltip: o._$tooltip[0]
                })
              }), o.__callbacks.open = []
            };
            if ("closed" !== o.__state) r = 0, "disappearing" === o.__state ? (o.__stateSet("appearing"), a.hasTransitions ? (o._$tooltip.clearQueue().removeClass("tooltipster-dying").addClass("tooltipster-show"), o.__options.animationDuration[0] > 0 && o._$tooltip.delay(o.__options.animationDuration[0]), o._$tooltip.queue(l)) : o._$tooltip.stop().fadeIn(l)) : "stable" == o.__state && l();
            else {
              if (o.__stateSet("appearing"), r = o.__options.animationDuration[0], o.__contentInsert(), o.reposition(e, !0), a.hasTransitions ? (o._$tooltip.addClass("tooltipster-" + o.__options.animation).addClass("tooltipster-initial").css({
                  "-moz-animation-duration": o.__options.animationDuration[0] + "ms",
                  "-ms-animation-duration": o.__options.animationDuration[0] + "ms",
                  "-o-animation-duration": o.__options.animationDuration[0] + "ms",
                  "-webkit-animation-duration": o.__options.animationDuration[0] + "ms",
                  "animation-duration": o.__options.animationDuration[0] + "ms",
                  "transition-duration": o.__options.animationDuration[0] + "ms"
                }), setTimeout(function () {
                  "closed" != o.__state && (o._$tooltip.addClass("tooltipster-show").removeClass("tooltipster-initial"), o.__options.animationDuration[0] > 0 && o._$tooltip.delay(o.__options.animationDuration[0]), o._$tooltip.queue(l))
                }, 0)) : o._$tooltip.css("display", "none").fadeIn(o.__options.animationDuration[0], l), o.__trackerStart(), t(a.window).on("resize." + o.__namespace + "-triggerClose", function (e) {
                  var i = t(document.activeElement);
                  (i.is("input") || i.is("textarea")) && t.contains(o._$tooltip[0], i[0]) || o.reposition(e)
                }).on("scroll." + o.__namespace + "-triggerClose", function (t) {
                  o.__scrollHandler(t)
                }), o.__$originParents = o._$origin.parents(), o.__$originParents.each(function (e, i) {
                  t(i).on("scroll." + o.__namespace + "-triggerClose", function (t) {
                    o.__scrollHandler(t)
                  })
                }), o.__options.triggerClose.mouseleave || o.__options.triggerClose.touchleave && a.hasTouchCapability) {
                o._on("dismissable", function (t) {
                  t.dismissable ? t.delay ? (d = setTimeout(function () {
                    o._close(t.event)
                  }, t.delay), o.__timeouts.close.push(d)) : o._close(t) : clearTimeout(d)
                });
                var c = o._$origin,
                  u = "",
                  h = "",
                  d = null;
                o.__options.interactive && (c = c.add(o._$tooltip)), o.__options.triggerClose.mouseleave && (u += "mouseenter." + o.__namespace + "-triggerClose ", h += "mouseleave." + o.__namespace + "-triggerClose "), o.__options.triggerClose.touchleave && a.hasTouchCapability && (u += "touchstart." + o.__namespace + "-triggerClose", h += "touchend." + o.__namespace + "-triggerClose touchcancel." + o.__namespace + "-triggerClose"), c.on(h, function (t) {
                  if (o._touchIsTouchEvent(t) || !o._touchIsEmulatedEvent(t)) {
                    var e = "mouseleave" == t.type ? o.__options.delay : o.__options.delayTouch;
                    o._trigger({
                      delay: e[1],
                      dismissable: !0,
                      event: t,
                      type: "dismissable"
                    })
                  }
                }).on(u, function (t) {
                  !o._touchIsTouchEvent(t) && o._touchIsEmulatedEvent(t) || o._trigger({
                    dismissable: !1,
                    event: t,
                    type: "dismissable"
                  })
                })
              }
              o.__options.triggerClose.originClick && o._$origin.on("click." + o.__namespace + "-triggerClose", function (t) {
                o._touchIsTouchEvent(t) || o._touchIsEmulatedEvent(t) || o._close(t)
              }), (o.__options.triggerClose.click || o.__options.triggerClose.tap && a.hasTouchCapability) && setTimeout(function () {
                if ("closed" != o.__state) {
                  var e = "";
                  o.__options.triggerClose.click && (e += "click." + o.__namespace + "-triggerClose "), o.__options.triggerClose.tap && a.hasTouchCapability && (e += "touchend." + o.__namespace + "-triggerClose"), t("body").on(e, function (e) {
                    o._touchIsMeaningfulEvent(e) && (o._touchRecordEvent(e), o.__options.interactive && t.contains(o._$tooltip[0], e.target) || o._close(e))
                  }), o.__options.triggerClose.tap && a.hasTouchCapability && t("body").on("touchstart." + o.__namespace + "-triggerClose", function (t) {
                    o._touchRecordEvent(t)
                  })
                }
              }, 0), o._trigger("ready"), o.__options.functionReady && o.__options.functionReady.call(o, o, {
                origin: o._$origin[0],
                tooltip: o._$tooltip[0]
              })
            }
            if (o.__options.timer > 0) {
              var d = setTimeout(function () {
                o._close()
              }, o.__options.timer + r);
              o.__timeouts.close.push(d)
            }
          }
        }
        return o
      },
      _openShortly: function (t) {
        var e = this,
          i = !0;
        if ("stable" != e.__state && "appearing" != e.__state && !e.__timeouts.open && (e._trigger({
            type: "start",
            event: t,
            stop: function () {
              i = !1
            }
          }), i)) {
          var n = 0 == t.type.indexOf("touch") ? e.__options.delayTouch : e.__options.delay;
          n[0] ? e.__timeouts.open = setTimeout(function () {
            e.__timeouts.open = null, e.__pointerIsOverOrigin && e._touchIsMeaningfulEvent(t) ? (e._trigger("startend"), e._open(t)) : e._trigger("startcancel")
          }, n[0]) : (e._trigger("startend"), e._open(t))
        }
        return e
      },
      _optionsExtract: function (e, i) {
        var n = this,
          o = t.extend(!0, {}, i),
          s = n.__options[e];
        return s || (s = {}, t.each(i, function (t, e) {
          var i = n.__options[t];
          void 0 !== i && (s[t] = i)
        })), t.each(o, function (e, i) {
          void 0 !== s[e] && ("object" != typeof i || i instanceof Array || null == i || "object" != typeof s[e] || s[e] instanceof Array || null == s[e] ? o[e] = s[e] : t.extend(o[e], s[e]))
        }), o
      },
      _plug: function (e) {
        var i = t.tooltipster._plugin(e);
        if (!i) throw new Error('The "' + e + '" plugin is not defined');
        return i.instance && t.tooltipster.__bridge(i.instance, this, i.name), this
      },
      _touchIsEmulatedEvent: function (t) {
        for (var e = !1, i = (new Date).getTime(), n = this.__touchEvents.length - 1; n >= 0; n--) {
          var o = this.__touchEvents[n];
          if (!(i - o.time < 500)) break;
          o.target === t.target && (e = !0)
        }
        return e
      },
      _touchIsMeaningfulEvent: function (t) {
        return this._touchIsTouchEvent(t) && !this._touchSwiped(t.target) || !this._touchIsTouchEvent(t) && !this._touchIsEmulatedEvent(t)
      },
      _touchIsTouchEvent: function (t) {
        return 0 == t.type.indexOf("touch")
      },
      _touchRecordEvent: function (t) {
        return this._touchIsTouchEvent(t) && (t.time = (new Date).getTime(), this.__touchEvents.push(t)), this
      },
      _touchSwiped: function (t) {
        for (var e = !1, i = this.__touchEvents.length - 1; i >= 0; i--) {
          var n = this.__touchEvents[i];
          if ("touchmove" == n.type) {
            e = !0;
            break
          }
          if ("touchstart" == n.type && t === n.target) break
        }
        return e
      },
      _trigger: function () {
        var e = Array.prototype.slice.apply(arguments);
        return "string" == typeof e[0] && (e[0] = {
          type: e[0]
        }), e[0].instance = this, e[0].origin = this._$origin ? this._$origin[0] : null, e[0].tooltip = this._$tooltip ? this._$tooltip[0] : null, this.__$emitterPrivate.trigger.apply(this.__$emitterPrivate, e), t.tooltipster._trigger.apply(t.tooltipster, e), this.__$emitterPublic.trigger.apply(this.__$emitterPublic, e), this
      },
      _unplug: function (e) {
        var i = this;
        if (i[e]) {
          var n = t.tooltipster._plugin(e);
          n.instance && t.each(n.instance, function (t, n) {
            i[t] && i[t].bridged === i[e] && delete i[t]
          }), i[e].__destroy && i[e].__destroy(), delete i[e]
        }
        return i
      },
      close: function (t) {
        return this.__destroyed ? this.__destroyError() : this._close(null, t), this
      },
      content: function (t) {
        var e = this;
        if (void 0 === t) return e.__Content;
        if (e.__destroyed) e.__destroyError();
        else if (e.__contentSet(t), null !== e.__Content) {
          if ("closed" !== e.__state && (e.__contentInsert(), e.reposition(), e.__options.updateAnimation))
            if (a.hasTransitions) {
              var i = e.__options.updateAnimation;
              e._$tooltip.addClass("tooltipster-update-" + i), setTimeout(function () {
                "closed" != e.__state && e._$tooltip.removeClass("tooltipster-update-" + i)
              }, 1e3)
            } else e._$tooltip.fadeTo(200, .5, function () {
              "closed" != e.__state && e._$tooltip.fadeTo(200, 1)
            })
        } else e._close();
        return e
      },
      destroy: function () {
        var e = this;
        return e.__destroyed ? e.__destroyError() : e.__destroying || (e.__destroying = !0, e._close(null, function () {
          e._trigger("destroy"), e.__destroying = !1, e.__destroyed = !0, e._$origin.removeData(e.__namespace).off("." + e.__namespace + "-triggerOpen"), t("body").off("." + e.__namespace + "-triggerOpen");
          var i = e._$origin.data("tooltipster-ns");
          if (i)
            if (1 === i.length) {
              var n = null;
              "previous" == e.__options.restoration ? n = e._$origin.data("tooltipster-initialTitle") : "current" == e.__options.restoration && (n = "string" == typeof e.__Content ? e.__Content : t("<div></div>").append(e.__Content).html()), n && e._$origin.attr("title", n), e._$origin.removeClass("tooltipstered"), e._$origin.removeData("tooltipster-ns").removeData("tooltipster-initialTitle")
            } else i = t.grep(i, function (t, i) {
              return t !== e.__namespace
            }), e._$origin.data("tooltipster-ns", i);
          e._trigger("destroyed"), e._off(), e.off(), e.__Content = null, e.__$emitterPrivate = null, e.__$emitterPublic = null, e.__options.parent = null, e._$origin = null, e._$tooltip = null, t.tooltipster.__instancesLatestArr = t.grep(t.tooltipster.__instancesLatestArr, function (t, i) {
            return e !== t
          }), clearInterval(e.__garbageCollector)
        })), e
      },
      disable: function () {
        return this.__destroyed ? (this.__destroyError(), this) : (this._close(), this.__enabled = !1, this)
      },
      elementOrigin: function () {
        return this.__destroyed ? void this.__destroyError() : this._$origin[0]
      },
      elementTooltip: function () {
        return this._$tooltip ? this._$tooltip[0] : null
      },
      enable: function () {
        return this.__enabled = !0, this
      },
      hide: function (t) {
        return this.close(t)
      },
      instance: function () {
        return this
      },
      off: function () {
        return this.__destroyed || this.__$emitterPublic.off.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
      },
      on: function () {
        return this.__destroyed ? this.__destroyError() : this.__$emitterPublic.on.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
      },
      one: function () {
        return this.__destroyed ? this.__destroyError() : this.__$emitterPublic.one.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
      },
      open: function (t) {
        return this.__destroyed || this.__destroying ? this.__destroyError() : this._open(null, t), this
      },
      option: function (e, i) {
        return void 0 === i ? this.__options[e] : (this.__destroyed ? this.__destroyError() : (this.__options[e] = i, this.__optionsFormat(), t.inArray(e, ["trigger", "triggerClose", "triggerOpen"]) >= 0 && this.__prepareOrigin(), "selfDestruction" === e && this.__prepareGC()), this)
      },
      reposition: function (t, e) {
        var i = this;
        return i.__destroyed ? i.__destroyError() : "closed" != i.__state && n(i._$origin) && (e || n(i._$tooltip)) && (e || i._$tooltip.detach(), i.__Geometry = i.__geometry(), i._trigger({
          type: "reposition",
          event: t,
          helper: {
            geo: i.__Geometry
          }
        })), i
      },
      show: function (t) {
        return this.open(t)
      },
      status: function () {
        return {
          destroyed: this.__destroyed,
          destroying: this.__destroying,
          enabled: this.__enabled,
          open: "closed" !== this.__state,
          state: this.__state
        }
      },
      triggerHandler: function () {
        return this.__destroyed ? this.__destroyError() : this.__$emitterPublic.triggerHandler.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
      }
    }, t.fn.tooltipster = function () {
      var e = Array.prototype.slice.apply(arguments),
        i = "You are using a single HTML element as content for several tooltips. You probably want to set the contentCloning option to TRUE.";
      if (0 === this.length) return this;
      if ("string" == typeof e[0]) {
        var n = "#*$~&";
        return this.each(function () {
          var o = t(this).data("tooltipster-ns"),
            s = o ? t(this).data(o[0]) : null;
          if (!s) throw new Error("You called Tooltipster's \"" + e[0] + '" method on an uninitialized element');
          if ("function" != typeof s[e[0]]) throw new Error('Unknown method "' + e[0] + '"');
          this.length > 1 && "content" == e[0] && (e[1] instanceof t || "object" == typeof e[1] && null != e[1] && e[1].tagName) && !s.__options.contentCloning && s.__options.debug && console.log(i);
          var r = s[e[0]](e[1], e[2]);
          return r !== s || "instance" === e[0] ? (n = r, !1) : void 0
        }), "#*$~&" !== n ? n : this
      }
      t.tooltipster.__instancesLatestArr = [];
      var o = e[0] && void 0 !== e[0].multiple,
        r = o && e[0].multiple || !o && s.multiple,
        a = e[0] && void 0 !== e[0].content,
        l = a && e[0].content || !a && s.content,
        c = e[0] && void 0 !== e[0].contentCloning,
        u = c && e[0].contentCloning || !c && s.contentCloning,
        h = e[0] && void 0 !== e[0].debug,
        d = h && e[0].debug || !h && s.debug;
      return this.length > 1 && (l instanceof t || "object" == typeof l && null != l && l.tagName) && !u && d && console.log(i), this.each(function () {
        var i = !1,
          n = t(this),
          o = n.data("tooltipster-ns"),
          s = null;
        o ? r ? i = !0 : d && (console.log("Tooltipster: one or more tooltips are already attached to the element below. Ignoring."), console.log(this)) : i = !0, i && (s = new t.Tooltipster(this, e[0]), o || (o = []), o.push(s.__namespace), n.data("tooltipster-ns", o), n.data(s.__namespace, s), s.__options.functionInit && s.__options.functionInit.call(s, s, {
          origin: this
        }), s._trigger("init")), t.tooltipster.__instancesLatestArr.push(s)
      }), this
    }, e.prototype = {
      __init: function (e) {
        this.__$tooltip = e, this.__$tooltip.css({
          left: 0,
          overflow: "hidden",
          position: "absolute",
          top: 0
        }).find(".tooltipster-content").css("overflow", "auto"), this.$container = t('<div class="tooltipster-ruler"></div>').append(this.__$tooltip).appendTo("body")
      },
      __forceRedraw: function () {
        var t = this.__$tooltip.parent();
        this.__$tooltip.detach(), this.__$tooltip.appendTo(t)
      },
      constrain: function (t, e) {
        return this.constraints = {
          width: t,
          height: e
        }, this.__$tooltip.css({
          display: "block",
          height: "",
          overflow: "auto",
          width: t
        }), this
      },
      destroy: function () {
        this.__$tooltip.detach().find(".tooltipster-content").css({
          display: "",
          overflow: ""
        }), this.$container.remove()
      },
      free: function () {
        return this.constraints = null, this.__$tooltip.css({
          display: "",
          height: "",
          overflow: "visible",
          width: ""
        }), this
      },
      measure: function () {
        this.__forceRedraw();
        var t = this.__$tooltip[0].getBoundingClientRect(),
          e = {
            size: {
              height: t.height || t.bottom,
              width: t.width || t.right
            }
          };
        if (this.constraints) {
          var i = this.__$tooltip.find(".tooltipster-content"),
            n = this.__$tooltip.outerHeight(),
            o = i[0].getBoundingClientRect(),
            s = {
              height: n <= this.constraints.height,
              width: t.width <= this.constraints.width && o.width >= i[0].scrollWidth - 1
            };
          e.fits = s.height && s.width
        }
        return a.IE && a.IE <= 11 && e.size.width !== a.window.document.documentElement.clientWidth && (e.size.width = Math.ceil(e.size.width) + 1), e
      }
    };
    var c = navigator.userAgent.toLowerCase(); - 1 != c.indexOf("msie") ? a.IE = parseInt(c.split("msie")[1]) : -1 !== c.toLowerCase().indexOf("trident") && -1 !== c.indexOf(" rv:11") ? a.IE = 11 : -1 != c.toLowerCase().indexOf("edge/") && (a.IE = parseInt(c.toLowerCase().split("edge/")[1]));
    var u = "tooltipster.sideTip";
    return t.tooltipster._plugin({
      name: u,
      instance: {
        __defaults: function () {
          return {
            arrow: !0,
            distance: 6,
            functionPosition: null,
            maxWidth: null,
            minIntersection: 16,
            minWidth: 0,
            position: null,
            side: "top",
            viewportAware: !0
          }
        },
        __init: function (t) {
          var e = this;
          e.__instance = t, e.__namespace = "tooltipster-sideTip-" + Math.round(1e6 * Math.random()), e.__previousState = "closed", e.__options, e.__optionsFormat(), e.__instance._on("state." + e.__namespace, function (t) {
            "closed" == t.state ? e.__close() : "appearing" == t.state && "closed" == e.__previousState && e.__create(), e.__previousState = t.state
          }), e.__instance._on("options." + e.__namespace, function () {
            e.__optionsFormat()
          }), e.__instance._on("reposition." + e.__namespace, function (t) {
            e.__reposition(t.event, t.helper)
          })
        },
        __close: function () {
          this.__instance.content() instanceof t && this.__instance.content().detach(), this.__instance._$tooltip.remove(), this.__instance._$tooltip = null
        },
        __create: function () {
          var e = t('<div class="tooltipster-base tooltipster-sidetip"><div class="tooltipster-box"><div class="tooltipster-content"></div></div><div class="tooltipster-arrow"><div class="tooltipster-arrow-uncropped"><div class="tooltipster-arrow-border"></div><div class="tooltipster-arrow-background"></div></div></div></div>');
          this.__options.arrow || e.find(".tooltipster-box").css("margin", 0).end().find(".tooltipster-arrow").hide(), this.__options.minWidth && e.css("min-width", this.__options.minWidth + "px"), this.__options.maxWidth && e.css("max-width", this.__options.maxWidth + "px"), this.__instance._$tooltip = e, this.__instance._trigger("created")
        },
        __destroy: function () {
          this.__instance._off("." + self.__namespace)
        },
        __optionsFormat: function () {
          var e = this;
          if (e.__options = e.__instance._optionsExtract(u, e.__defaults()), e.__options.position && (e.__options.side = e.__options.position), "object" != typeof e.__options.distance && (e.__options.distance = [e.__options.distance]), e.__options.distance.length < 4 && (void 0 === e.__options.distance[1] && (e.__options.distance[1] = e.__options.distance[0]), void 0 === e.__options.distance[2] && (e.__options.distance[2] = e.__options.distance[0]), void 0 === e.__options.distance[3] && (e.__options.distance[3] = e.__options.distance[1]), e.__options.distance = {
              top: e.__options.distance[0],
              right: e.__options.distance[1],
              bottom: e.__options.distance[2],
              left: e.__options.distance[3]
            }), "string" == typeof e.__options.side) {
            var i = {
              top: "bottom",
              right: "left",
              bottom: "top",
              left: "right"
            };
            e.__options.side = [e.__options.side, i[e.__options.side]], "left" == e.__options.side[0] || "right" == e.__options.side[0] ? e.__options.side.push("top", "bottom") : e.__options.side.push("right", "left")
          }
          6 === t.tooltipster._env.IE && e.__options.arrow !== !0 && (e.__options.arrow = !1)
        },
        __reposition: function (e, i) {
          var n, o = this,
            s = o.__targetFind(i),
            r = [];
          o.__instance._$tooltip.detach();
          var a = o.__instance._$tooltip.clone(),
            l = t.tooltipster._getRuler(a),
            c = !1,
            u = o.__instance.option("animation");
          switch (u && a.removeClass("tooltipster-" + u), t.each(["window", "document"], function (n, u) {
            var h = null;
            if (o.__instance._trigger({
                container: u,
                helper: i,
                satisfied: c,
                takeTest: function (t) {
                  h = t
                },
                results: r,
                type: "positionTest"
              }), 1 == h || 0 != h && 0 == c && ("window" != u || o.__options.viewportAware))
              for (var n = 0; n < o.__options.side.length; n++) {
                var d = {
                    horizontal: 0,
                    vertical: 0
                  },
                  p = o.__options.side[n];
                "top" == p || "bottom" == p ? d.vertical = o.__options.distance[p] : d.horizontal = o.__options.distance[p], o.__sideChange(a, p), t.each(["natural", "constrained"], function (t, n) {
                  if (h = null, o.__instance._trigger({
                      container: u,
                      event: e,
                      helper: i,
                      mode: n,
                      results: r,
                      satisfied: c,
                      side: p,
                      takeTest: function (t) {
                        h = t
                      },
                      type: "positionTest"
                    }), 1 == h || 0 != h && 0 == c) {
                    var a = {
                        container: u,
                        distance: d,
                        fits: null,
                        mode: n,
                        outerSize: null,
                        side: p,
                        size: null,
                        target: s[p],
                        whole: null
                      },
                      f = "natural" == n ? l.free() : l.constrain(i.geo.available[u][p].width - d.horizontal, i.geo.available[u][p].height - d.vertical),
                      g = f.measure();
                    if (a.size = g.size, a.outerSize = {
                        height: g.size.height + d.vertical,
                        width: g.size.width + d.horizontal
                      }, "natural" == n ? i.geo.available[u][p].width >= a.outerSize.width && i.geo.available[u][p].height >= a.outerSize.height ? a.fits = !0 : a.fits = !1 : a.fits = g.fits, "window" == u && (a.fits ? "top" == p || "bottom" == p ? a.whole = i.geo.origin.windowOffset.right >= o.__options.minIntersection && i.geo.window.size.width - i.geo.origin.windowOffset.left >= o.__options.minIntersection : a.whole = i.geo.origin.windowOffset.bottom >= o.__options.minIntersection && i.geo.window.size.height - i.geo.origin.windowOffset.top >= o.__options.minIntersection : a.whole = !1),
                      r.push(a), a.whole) c = !0;
                    else if ("natural" == a.mode && (a.fits || a.size.width <= i.geo.available[u][p].width)) return !1
                  }
                })
              }
          }), o.__instance._trigger({
            edit: function (t) {
              r = t
            },
            event: e,
            helper: i,
            results: r,
            type: "positionTested"
          }), r.sort(function (t, e) {
            if (t.whole && !e.whole) return -1;
            if (!t.whole && e.whole) return 1;
            if (t.whole && e.whole) {
              var i = o.__options.side.indexOf(t.side),
                n = o.__options.side.indexOf(e.side);
              return n > i ? -1 : i > n ? 1 : "natural" == t.mode ? -1 : 1
            }
            if (t.fits && !e.fits) return -1;
            if (!t.fits && e.fits) return 1;
            if (t.fits && e.fits) {
              var i = o.__options.side.indexOf(t.side),
                n = o.__options.side.indexOf(e.side);
              return n > i ? -1 : i > n ? 1 : "natural" == t.mode ? -1 : 1
            }
            return "document" == t.container && "bottom" == t.side && "natural" == t.mode ? -1 : 1
          }), n = r[0], n.coord = {}, n.side) {
            case "left":
            case "right":
              n.coord.top = Math.floor(n.target - n.size.height / 2);
              break;
            case "bottom":
            case "top":
              n.coord.left = Math.floor(n.target - n.size.width / 2)
          }
          switch (n.side) {
            case "left":
              n.coord.left = i.geo.origin.windowOffset.left - n.outerSize.width;
              break;
            case "right":
              n.coord.left = i.geo.origin.windowOffset.right + n.distance.horizontal;
              break;
            case "top":
              n.coord.top = i.geo.origin.windowOffset.top - n.outerSize.height;
              break;
            case "bottom":
              n.coord.top = i.geo.origin.windowOffset.bottom + n.distance.vertical
          }
          "window" == n.container ? "top" == n.side || "bottom" == n.side ? n.coord.left < 0 ? i.geo.origin.windowOffset.right - this.__options.minIntersection >= 0 ? n.coord.left = 0 : n.coord.left = i.geo.origin.windowOffset.right - this.__options.minIntersection - 1 : n.coord.left > i.geo.window.size.width - n.size.width && (i.geo.origin.windowOffset.left + this.__options.minIntersection <= i.geo.window.size.width ? n.coord.left = i.geo.window.size.width - n.size.width : n.coord.left = i.geo.origin.windowOffset.left + this.__options.minIntersection + 1 - n.size.width) : n.coord.top < 0 ? i.geo.origin.windowOffset.bottom - this.__options.minIntersection >= 0 ? n.coord.top = 0 : n.coord.top = i.geo.origin.windowOffset.bottom - this.__options.minIntersection - 1 : n.coord.top > i.geo.window.size.height - n.size.height && (i.geo.origin.windowOffset.top + this.__options.minIntersection <= i.geo.window.size.height ? n.coord.top = i.geo.window.size.height - n.size.height : n.coord.top = i.geo.origin.windowOffset.top + this.__options.minIntersection + 1 - n.size.height) : (n.coord.left > i.geo.window.size.width - n.size.width && (n.coord.left = i.geo.window.size.width - n.size.width), n.coord.left < 0 && (n.coord.left = 0)), o.__sideChange(a, n.side), i.tooltipClone = a[0], i.tooltipParent = o.__instance.option("parent").parent[0], i.mode = n.mode, i.whole = n.whole, i.origin = o.__instance._$origin[0], i.tooltip = o.__instance._$tooltip[0], delete n.container, delete n.fits, delete n.mode, delete n.outerSize, delete n.whole, n.distance = n.distance.horizontal || n.distance.vertical;
          var h = t.extend(!0, {}, n);
          if (o.__instance._trigger({
              edit: function (t) {
                n = t
              },
              event: e,
              helper: i,
              position: h,
              type: "position"
            }), o.__options.functionPosition) {
            var d = o.__options.functionPosition.call(o, o.__instance, i, h);
            d && (n = d)
          }
          l.destroy();
          var p, f;
          "top" == n.side || "bottom" == n.side ? (p = {
            prop: "left",
            val: n.target - n.coord.left
          }, f = n.size.width - this.__options.minIntersection) : (p = {
            prop: "top",
            val: n.target - n.coord.top
          }, f = n.size.height - this.__options.minIntersection), p.val < this.__options.minIntersection ? p.val = this.__options.minIntersection : p.val > f && (p.val = f);
          var g;
          g = i.geo.origin.fixedLineage ? i.geo.origin.windowOffset : {
            left: i.geo.origin.windowOffset.left + i.geo.window.scroll.left,
            top: i.geo.origin.windowOffset.top + i.geo.window.scroll.top
          }, n.coord = {
            left: g.left + (n.coord.left - i.geo.origin.windowOffset.left),
            top: g.top + (n.coord.top - i.geo.origin.windowOffset.top)
          }, o.__sideChange(o.__instance._$tooltip, n.side), i.geo.origin.fixedLineage ? o.__instance._$tooltip.css("position", "fixed") : o.__instance._$tooltip.css("position", ""), o.__instance._$tooltip.css({
            left: n.coord.left,
            top: n.coord.top,
            height: n.size.height,
            width: n.size.width
          }).find(".tooltipster-arrow").css({
            left: "",
            top: ""
          }).css(p.prop, p.val), o.__instance._$tooltip.appendTo(o.__instance.option("parent")), o.__instance._trigger({
            type: "repositioned",
            event: e,
            position: n
          })
        },
        __sideChange: function (t, e) {
          t.removeClass("tooltipster-bottom").removeClass("tooltipster-left").removeClass("tooltipster-right").removeClass("tooltipster-top").addClass("tooltipster-" + e)
        },
        __targetFind: function (t) {
          var e = {},
            i = this.__instance._$origin[0].getClientRects();
          if (i.length > 1) {
            var n = this.__instance._$origin.css("opacity");
            1 == n && (this.__instance._$origin.css("opacity", .99), i = this.__instance._$origin[0].getClientRects(), this.__instance._$origin.css("opacity", 1))
          }
          if (i.length < 2) e.top = Math.floor(t.geo.origin.windowOffset.left + t.geo.origin.size.width / 2), e.bottom = e.top, e.left = Math.floor(t.geo.origin.windowOffset.top + t.geo.origin.size.height / 2), e.right = e.left;
          else {
            var o = i[0];
            e.top = Math.floor(o.left + (o.right - o.left) / 2), o = i.length > 2 ? i[Math.ceil(i.length / 2) - 1] : i[0], e.right = Math.floor(o.top + (o.bottom - o.top) / 2), o = i[i.length - 1], e.bottom = Math.floor(o.left + (o.right - o.left) / 2), o = i.length > 2 ? i[Math.ceil((i.length + 1) / 2) - 1] : i[i.length - 1], e.left = Math.floor(o.top + (o.bottom - o.top) / 2)
          }
          return e
        }
      }
    }), t
  }),
  function () {
    var t, e, i, n, o, s, r, a, l;
    window.device = {}, e = window.document.documentElement, l = window.navigator.userAgent.toLowerCase(), device.ios = function () {
      return device.iphone() || device.ipod() || device.ipad()
    }, device.iphone = function () {
      return i("iphone")
    }, device.ipod = function () {
      return i("ipod")
    }, device.ipad = function () {
      return i("ipad")
    }, device.android = function () {
      return i("android")
    }, device.androidPhone = function () {
      return device.android() && i("mobile")
    }, device.androidTablet = function () {
      return device.android() && !i("mobile")
    }, device.blackberry = function () {
      return i("blackberry") || i("bb10") || i("rim")
    }, device.blackberryPhone = function () {
      return device.blackberry() && !i("tablet")
    }, device.blackberryTablet = function () {
      return device.blackberry() && i("tablet")
    }, device.windows = function () {
      return i("windows")
    }, device.windowsPhone = function () {
      return device.windows() && i("phone")
    }, device.windowsTablet = function () {
      return device.windows() && i("touch")
    }, device.fxos = function () {
      return i("(mobile; rv:") || i("(tablet; rv:")
    }, device.fxosPhone = function () {
      return device.fxos() && i("mobile")
    }, device.fxosTablet = function () {
      return device.fxos() && i("tablet")
    }, device.mobile = function () {
      return device.androidPhone() || device.iphone() || device.ipod() || device.windowsPhone() || device.blackberryPhone() || device.fxosPhone()
    }, device.tablet = function () {
      return device.ipad() || device.androidTablet() || device.blackberryTablet() || device.windowsTablet() || device.fxosTablet()
    }, device.portrait = function () {
      return 90 !== Math.abs(window.orientation)
    }, device.landscape = function () {
      return 90 === Math.abs(window.orientation)
    }, i = function (t) {
      return -1 !== l.indexOf(t)
    }, o = function (t) {
      var i;
      return i = new RegExp(t, "i"), e.className.match(i)
    }, t = function (t) {
      return o(t) ? void 0 : e.className += " " + t
    }, r = function (t) {
      return o(t) ? e.className = e.className.replace(t, "") : void 0
    }, device.ios() ? device.ipad() ? t("ios ipad tablet") : device.iphone() ? t("ios iphone mobile") : device.ipod() && t("ios ipod mobile") : t(device.android() ? device.androidTablet() ? "android tablet" : "android mobile" : device.blackberry() ? device.blackberryTablet() ? "blackberry tablet" : "blackberry mobile" : device.windows() ? device.windowsTablet() ? "windows tablet" : device.windowsPhone() ? "windows mobile" : "desktop" : device.fxos() ? device.fxosTablet() ? "fxos tablet" : "fxos mobile" : "desktop"), n = function () {
      return device.landscape() ? (r("portrait"), t("landscape")) : (r("landscape"), t("portrait"))
    }, a = "onorientationchange" in window, s = a ? "orientationchange" : "resize", window.addEventListener ? window.addEventListener(s, n, !1) : window.attachEvent ? window.attachEvent(s, n) : window[s] = n, n()
  }.call(this),
  function () {
    var t, e, i, n, o, s = {}.hasOwnProperty,
      r = function (t, e) {
        function i() {
          this.constructor = t
        }
        for (var n in e) s.call(e, n) && (t[n] = e[n]);
        return i.prototype = e.prototype, t.prototype = new i, t.__super__ = e.prototype, t
      };
    n = function () {
      function t() {
        this.options_index = 0, this.parsed = []
      }
      return t.prototype.add_node = function (t) {
        return "OPTGROUP" === t.nodeName.toUpperCase() ? this.add_group(t) : this.add_option(t)
      }, t.prototype.add_group = function (t) {
        var e, i, n, o, s, r;
        for (e = this.parsed.length, this.parsed.push({
            array_index: e,
            group: !0,
            label: this.escapeExpression(t.label),
            title: t.title ? t.title : void 0,
            children: 0,
            disabled: t.disabled,
            classes: t.className
          }), s = t.childNodes, r = [], n = 0, o = s.length; o > n; n++) i = s[n], r.push(this.add_option(i, e, t.disabled));
        return r
      }, t.prototype.add_option = function (t, e, i) {
        return "OPTION" === t.nodeName.toUpperCase() ? ("" !== t.text ? (null != e && (this.parsed[e].children += 1), this.parsed.push({
          array_index: this.parsed.length,
          options_index: this.options_index,
          value: t.value,
          text: t.text,
          html: t.innerHTML,
          title: t.title ? t.title : void 0,
          selected: t.selected,
          disabled: i === !0 ? i : t.disabled,
          group_array_index: e,
          group_label: null != e ? this.parsed[e].label : null,
          classes: t.className,
          style: t.style.cssText
        })) : this.parsed.push({
          array_index: this.parsed.length,
          options_index: this.options_index,
          empty: !0
        }), this.options_index += 1) : void 0
      }, t.prototype.escapeExpression = function (t) {
        var e, i;
        return null == t || t === !1 ? "" : /[\&\<\>\"\'\`]/.test(t) ? (e = {
          "<": "&lt;",
          ">": "&gt;",
          '"': "&quot;",
          "'": "&#x27;",
          "`": "&#x60;"
        }, i = /&(?!\w+;)|[\<\>\"\'\`]/g, t.replace(i, function (t) {
          return e[t] || "&amp;"
        })) : t
      }, t
    }(), n.select_to_array = function (t) {
      var e, i, o, s, r;
      for (i = new n, r = t.childNodes, o = 0, s = r.length; s > o; o++) e = r[o], i.add_node(e);
      return i.parsed
    }, e = function () {
      function t(e, i) {
        this.form_field = e, this.options = null != i ? i : {}, t.browser_is_supported() && (this.is_multiple = this.form_field.multiple, this.set_default_text(), this.set_default_values(), this.setup(), this.set_up_html(), this.register_observers(), this.on_ready())
      }
      return t.prototype.set_default_values = function () {
        var t = this;
        return this.click_test_action = function (e) {
          return t.test_active_click(e)
        }, this.activate_action = function (e) {
          return t.activate_field(e)
        }, this.active_field = !1, this.mouse_on_container = !1, this.results_showing = !1, this.result_highlighted = null, this.allow_single_deselect = null != this.options.allow_single_deselect && null != this.form_field.options[0] && "" === this.form_field.options[0].text && this.options.allow_single_deselect, this.disable_search_threshold = this.options.disable_search_threshold || 0, this.disable_search = this.options.disable_search || !1, this.enable_split_word_search = null == this.options.enable_split_word_search || this.options.enable_split_word_search, this.group_search = null == this.options.group_search || this.options.group_search, this.search_contains = this.options.search_contains || !1, this.single_backstroke_delete = null == this.options.single_backstroke_delete || this.options.single_backstroke_delete, this.max_selected_options = this.options.max_selected_options || 1 / 0, this.inherit_select_classes = this.options.inherit_select_classes || !1, this.display_selected_options = null == this.options.display_selected_options || this.options.display_selected_options, this.display_disabled_options = null == this.options.display_disabled_options || this.options.display_disabled_options, this.include_group_label_in_selected = this.options.include_group_label_in_selected || !1, this.max_shown_results = this.options.max_shown_results || Number.POSITIVE_INFINITY
      }, t.prototype.set_default_text = function () {
        return this.form_field.getAttribute("data-placeholder") ? this.default_text = this.form_field.getAttribute("data-placeholder") : this.is_multiple ? this.default_text = this.options.placeholder_text_multiple || this.options.placeholder_text || t.default_multiple_text : this.default_text = this.options.placeholder_text_single || this.options.placeholder_text || t.default_single_text, this.results_none_found = this.form_field.getAttribute("data-no_results_text") || this.options.no_results_text || t.default_no_result_text
      }, t.prototype.choice_label = function (t) {
        return this.include_group_label_in_selected && null != t.group_label ? "<b class='group-name'>" + t.group_label + "</b>" + t.html : t.html
      }, t.prototype.mouse_enter = function () {
        return this.mouse_on_container = !0
      }, t.prototype.mouse_leave = function () {
        return this.mouse_on_container = !1
      }, t.prototype.input_focus = function (t) {
        var e = this;
        if (this.is_multiple) {
          if (!this.active_field) return setTimeout(function () {
            return e.container_mousedown()
          }, 50)
        } else if (!this.active_field) return this.activate_field()
      }, t.prototype.input_blur = function (t) {
        var e = this;
        return this.mouse_on_container ? void 0 : (this.active_field = !1, setTimeout(function () {
          return e.blur_test()
        }, 100))
      }, t.prototype.results_option_build = function (t) {
        var e, i, n, o, s, r, a;
        for (e = "", o = 0, a = this.results_data, s = 0, r = a.length; r > s && (i = a[s], n = "", n = i.group ? this.result_add_group(i) : this.result_add_option(i), "" !== n && (o++, e += n), (null != t ? t.first : void 0) && (i.selected && this.is_multiple ? this.choice_build(i) : i.selected && !this.is_multiple && this.single_set_selected_text(this.choice_label(i))), !(o >= this.max_shown_results)); s++);
        return e
      }, t.prototype.result_add_option = function (t) {
        var e, i;
        return t.search_match && this.include_option_in_results(t) ? (e = [], t.disabled || t.selected && this.is_multiple || e.push("active-result"), !t.disabled || t.selected && this.is_multiple || e.push("disabled-result"), t.selected && e.push("result-selected"), null != t.group_array_index && e.push("group-option"), "" !== t.classes && e.push(t.classes), i = document.createElement("li"), i.className = e.join(" "), i.style.cssText = t.style, i.setAttribute("data-option-array-index", t.array_index), i.innerHTML = t.search_text, t.title && (i.title = t.title), this.outerHTML(i)) : ""
      }, t.prototype.result_add_group = function (t) {
        var e, i;
        return (t.search_match || t.group_match) && t.active_options > 0 ? (e = [], e.push("group-result"), t.classes && e.push(t.classes), i = document.createElement("li"), i.className = e.join(" "), i.innerHTML = t.search_text, t.title && (i.title = t.title), this.outerHTML(i)) : ""
      }, t.prototype.results_update_field = function () {
        return this.set_default_text(), this.is_multiple || this.results_reset_cleanup(), this.result_clear_highlight(), this.results_build(), this.results_showing ? this.winnow_results() : void 0
      }, t.prototype.reset_single_select_options = function () {
        var t, e, i, n, o;
        for (n = this.results_data, o = [], e = 0, i = n.length; i > e; e++) t = n[e], t.selected ? o.push(t.selected = !1) : o.push(void 0);
        return o
      }, t.prototype.results_toggle = function () {
        return this.results_showing ? this.results_hide() : this.results_show()
      }, t.prototype.results_search = function (t) {
        return this.results_showing ? this.winnow_results() : this.results_show()
      }, t.prototype.winnow_results = function () {
        var t, e, i, n, o, s, r, a, l, c, u, h;
        for (this.no_results_clear(), n = 0, s = this.get_search_text(), t = s.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), l = new RegExp(t, "i"), i = this.get_search_regex(t), h = this.results_data, c = 0, u = h.length; u > c; c++) e = h[c], e.search_match = !1, o = null, this.include_option_in_results(e) && (e.group && (e.group_match = !1, e.active_options = 0), null != e.group_array_index && this.results_data[e.group_array_index] && (o = this.results_data[e.group_array_index], 0 === o.active_options && o.search_match && (n += 1), o.active_options += 1), e.search_text = e.group ? e.label : e.html, (!e.group || this.group_search) && (e.search_match = this.search_string_match(e.search_text, i), e.search_match && !e.group && (n += 1), e.search_match ? (s.length && (r = e.search_text.search(l), a = e.search_text.substr(0, r + s.length) + "</em>" + e.search_text.substr(r + s.length), e.search_text = a.substr(0, r) + "<em>" + a.substr(r)), null != o && (o.group_match = !0)) : null != e.group_array_index && this.results_data[e.group_array_index].search_match && (e.search_match = !0)));
        return this.result_clear_highlight(), 1 > n && s.length ? (this.update_results_content(""), this.no_results(s)) : (this.update_results_content(this.results_option_build()), this.winnow_results_set_highlight())
      }, t.prototype.get_search_regex = function (t) {
        var e;
        return e = this.search_contains ? "" : "^", new RegExp(e + t, "i")
      }, t.prototype.search_string_match = function (t, e) {
        var i, n, o, s;
        if (e.test(t)) return !0;
        if (this.enable_split_word_search && (t.indexOf(" ") >= 0 || 0 === t.indexOf("[")) && (n = t.replace(/\[|\]/g, "").split(" "), n.length))
          for (o = 0, s = n.length; s > o; o++)
            if (i = n[o], e.test(i)) return !0
      }, t.prototype.choices_count = function () {
        var t, e, i, n;
        if (null != this.selected_option_count) return this.selected_option_count;
        for (this.selected_option_count = 0, n = this.form_field.options, e = 0, i = n.length; i > e; e++) t = n[e], t.selected && (this.selected_option_count += 1);
        return this.selected_option_count
      }, t.prototype.choices_click = function (t) {
        return t.preventDefault(), this.results_showing || this.is_disabled ? void 0 : this.results_show()
      }, t.prototype.keyup_checker = function (t) {
        var e, i;
        switch (e = null != (i = t.which) ? i : t.keyCode, this.search_field_scale(), e) {
          case 8:
            if (this.is_multiple && this.backstroke_length < 1 && this.choices_count() > 0) return this.keydown_backstroke();
            if (!this.pending_backstroke) return this.result_clear_highlight(), this.results_search();
            break;
          case 13:
            if (t.preventDefault(), this.results_showing) return this.result_select(t);
            break;
          case 27:
            return this.results_showing && this.results_hide(), !0;
          case 9:
          case 38:
          case 40:
          case 16:
          case 91:
          case 17:
          case 18:
            break;
          default:
            return this.results_search()
        }
      }, t.prototype.clipboard_event_checker = function (t) {
        var e = this;
        return setTimeout(function () {
          return e.results_search()
        }, 50)
      }, t.prototype.container_width = function () {
        return null != this.options.width ? this.options.width : "" + this.form_field.offsetWidth + "px"
      }, t.prototype.include_option_in_results = function (t) {
        return !(this.is_multiple && !this.display_selected_options && t.selected) && (!(!this.display_disabled_options && t.disabled) && !t.empty)
      }, t.prototype.search_results_touchstart = function (t) {
        return this.touch_started = !0, this.search_results_mouseover(t)
      }, t.prototype.search_results_touchmove = function (t) {
        return this.touch_started = !1, this.search_results_mouseout(t)
      }, t.prototype.search_results_touchend = function (t) {
        return this.touch_started ? this.search_results_mouseup(t) : void 0
      }, t.prototype.outerHTML = function (t) {
        var e;
        return t.outerHTML ? t.outerHTML : (e = document.createElement("div"), e.appendChild(t), e.innerHTML)
      }, t.browser_is_supported = function () {
        return !0
      }, t.default_multiple_text = "Select Some Options", t.default_single_text = "Select an Option", t.default_no_result_text = "No results match", t
    }(), t = jQuery, t.fn.extend({
      chosen: function (n) {
        return e.browser_is_supported() ? this.each(function (e) {
          var o, s;
          return o = t(this), s = o.data("chosen"), "destroy" === n ? void(s instanceof i && s.destroy()) : void(s instanceof i || o.data("chosen", new i(this, n)))
        }) : this
      }
    }), i = function (e) {
      function i() {
        return o = i.__super__.constructor.apply(this, arguments)
      }
      return r(i, e), i.prototype.setup = function () {
        return this.form_field_jq = t(this.form_field), this.current_selectedIndex = this.form_field.selectedIndex, this.is_rtl = this.form_field_jq.hasClass("chosen-rtl")
      }, i.prototype.set_up_html = function () {
        var e, i;
        return e = ["chosen-container"], e.push("chosen-container-" + (this.is_multiple ? "multi" : "single")), this.inherit_select_classes && this.form_field.className && e.push(this.form_field.className), this.is_rtl && e.push("chosen-rtl"), i = {
          class: e.join(" "),
          style: "width: " + this.container_width() + ";",
          title: this.form_field.title
        }, this.form_field.id.length && (i.id = this.form_field.id.replace(/[^\w]/g, "_") + "_chosen"), this.container = t("<div />", i), this.is_multiple ? this.container.html('<ul class="chosen-choices"><li class="search-field"><input type="text" value="' + this.default_text + '" class="default" autocomplete="off" style="width:25px;" /></li></ul><div class="chosen-drop"><ul class="chosen-results"></ul></div>') : this.container.html('<a class="chosen-single chosen-default"><span>' + this.default_text + '</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off" /></div><ul class="chosen-results"></ul></div>'), this.form_field_jq.hide().after(this.container), this.dropdown = this.container.find("div.chosen-drop").first(), this.search_field = this.container.find("input").first(), this.search_results = this.container.find("ul.chosen-results").first(), this.search_field_scale(), this.search_no_results = this.container.find("li.no-results").first(), this.is_multiple ? (this.search_choices = this.container.find("ul.chosen-choices").first(), this.search_container = this.container.find("li.search-field").first()) : (this.search_container = this.container.find("div.chosen-search").first(), this.selected_item = this.container.find(".chosen-single").first()), this.results_build(), this.set_tab_index(), this.set_label_behavior()
      }, i.prototype.on_ready = function () {
        return this.form_field_jq.trigger("chosen:ready", {
          chosen: this
        })
      }, i.prototype.register_observers = function () {
        var t = this;
        return this.container.bind("touchstart.chosen", function (e) {
          return t.container_mousedown(e), e.preventDefault()
        }), this.container.bind("touchend.chosen", function (e) {
          return t.container_mouseup(e), e.preventDefault()
        }), this.container.bind("mousedown.chosen", function (e) {
          t.container_mousedown(e)
        }), this.container.bind("mouseup.chosen", function (e) {
          t.container_mouseup(e)
        }), this.container.bind("mouseenter.chosen", function (e) {
          t.mouse_enter(e)
        }), this.container.bind("mouseleave.chosen", function (e) {
          t.mouse_leave(e)
        }), this.search_results.bind("mouseup.chosen", function (e) {
          t.search_results_mouseup(e)
        }), this.search_results.bind("mouseover.chosen", function (e) {
          t.search_results_mouseover(e)
        }), this.search_results.bind("mouseout.chosen", function (e) {
          t.search_results_mouseout(e)
        }), this.search_results.bind("mousewheel.chosen DOMMouseScroll.chosen", function (e) {
          t.search_results_mousewheel(e)
        }), this.search_results.bind("touchstart.chosen", function (e) {
          t.search_results_touchstart(e)
        }), this.search_results.bind("touchmove.chosen", function (e) {
          t.search_results_touchmove(e)
        }), this.search_results.bind("touchend.chosen", function (e) {
          t.search_results_touchend(e)
        }), this.form_field_jq.bind("chosen:updated.chosen", function (e) {
          t.results_update_field(e)
        }), this.form_field_jq.bind("chosen:activate.chosen", function (e) {
          t.activate_field(e)
        }), this.form_field_jq.bind("chosen:open.chosen", function (e) {
          t.container_mousedown(e)
        }), this.form_field_jq.bind("chosen:close.chosen", function (e) {
          t.input_blur(e)
        }), this.search_field.bind("blur.chosen", function (e) {
          t.input_blur(e)
        }), this.search_field.bind("keyup.chosen", function (e) {
          t.keyup_checker(e)
        }), this.search_field.bind("keydown.chosen", function (e) {
          t.keydown_checker(e)
        }), this.search_field.bind("focus.chosen", function (e) {
          t.input_focus(e)
        }), this.search_field.bind("cut.chosen", function (e) {
          t.clipboard_event_checker(e)
        }), this.search_field.bind("paste.chosen", function (e) {
          t.clipboard_event_checker(e)
        }), this.is_multiple ? this.search_choices.bind("click.chosen", function (e) {
          t.choices_click(e)
        }) : this.container.bind("click.chosen", function (t) {
          t.preventDefault()
        })
      }, i.prototype.destroy = function () {
        return t(this.container[0].ownerDocument).unbind("click.chosen", this.click_test_action), this.search_field[0].tabIndex && (this.form_field_jq[0].tabIndex = this.search_field[0].tabIndex), this.container.remove(), this.form_field_jq.removeData("chosen"), this.form_field_jq.show()
      }, i.prototype.search_field_disabled = function () {
        return this.is_disabled = this.form_field_jq[0].disabled, this.is_disabled ? (this.container.addClass("chosen-disabled"), this.search_field[0].disabled = !0, this.is_multiple || this.selected_item.unbind("focus.chosen", this.activate_action), this.close_field()) : (this.container.removeClass("chosen-disabled"), this.search_field[0].disabled = !1, this.is_multiple ? void 0 : this.selected_item.bind("focus.chosen", this.activate_action))
      }, i.prototype.container_mousedown = function (e) {
        return this.is_disabled || (e && "mousedown" === e.type && !this.results_showing && e.preventDefault(), null != e && t(e.target).hasClass("search-choice-close")) ? void 0 : (this.active_field ? this.is_multiple || !e || t(e.target)[0] !== this.selected_item[0] && !t(e.target).parents("a.chosen-single").length || (e.preventDefault(), this.results_toggle()) : (this.is_multiple && this.search_field.val(""), t(this.container[0].ownerDocument).bind("click.chosen", this.click_test_action), this.results_show()), this.activate_field())
      }, i.prototype.container_mouseup = function (t) {
        return "ABBR" !== t.target.nodeName || this.is_disabled ? void 0 : this.results_reset(t)
      }, i.prototype.search_results_mousewheel = function (t) {
        var e;
        return t.originalEvent && (e = t.originalEvent.deltaY || -t.originalEvent.wheelDelta || t.originalEvent.detail), null != e ? (t.preventDefault(), "DOMMouseScroll" === t.type && (e *= 40), this.search_results.scrollTop(e + this.search_results.scrollTop())) : void 0
      }, i.prototype.blur_test = function (t) {
        return !this.active_field && this.container.hasClass("chosen-container-active") ? this.close_field() : void 0
      }, i.prototype.close_field = function () {
        return t(this.container[0].ownerDocument).unbind("click.chosen", this.click_test_action), this.active_field = !1, this.results_hide(), this.container.removeClass("chosen-container-active"), this.clear_backstroke(), this.show_search_field_default(), this.search_field_scale()
      }, i.prototype.activate_field = function () {
        return this.container.addClass("chosen-container-active"), this.active_field = !0, this.search_field.val(this.search_field.val()), this.search_field.focus()
      }, i.prototype.test_active_click = function (e) {
        var i;
        return i = t(e.target).closest(".chosen-container"), i.length && this.container[0] === i[0] ? this.active_field = !0 : this.close_field()
      }, i.prototype.results_build = function () {
        return this.parsing = !0, this.selected_option_count = null, this.results_data = n.select_to_array(this.form_field), this.is_multiple ? this.search_choices.find("li.search-choice").remove() : this.is_multiple || (this.single_set_selected_text(), this.disable_search || this.form_field.options.length <= this.disable_search_threshold ? (this.search_field[0].readOnly = !0, this.container.addClass("chosen-container-single-nosearch")) : (this.search_field[0].readOnly = !1, this.container.removeClass("chosen-container-single-nosearch"))), this.update_results_content(this.results_option_build({
          first: !0
        })), this.search_field_disabled(), this.show_search_field_default(), this.search_field_scale(), this.parsing = !1
      }, i.prototype.result_do_highlight = function (t) {
        var e, i, n, o, s;
        if (t.length) {
          if (this.result_clear_highlight(), this.result_highlight = t, this.result_highlight.addClass("highlighted"), n = parseInt(this.search_results.css("maxHeight"), 10), s = this.search_results.scrollTop(), o = n + s, i = this.result_highlight.position().top + this.search_results.scrollTop(), e = i + this.result_highlight.outerHeight(), e >= o) return this.search_results.scrollTop(e - n > 0 ? e - n : 0);
          if (s > i) return this.search_results.scrollTop(i)
        }
      }, i.prototype.result_clear_highlight = function () {
        return this.result_highlight && this.result_highlight.removeClass("highlighted"), this.result_highlight = null
      }, i.prototype.results_show = function () {
        return this.is_multiple && this.max_selected_options <= this.choices_count() ? (this.form_field_jq.trigger("chosen:maxselected", {
          chosen: this
        }), !1) : (this.container.addClass("chosen-with-drop"), this.results_showing = !0, this.search_field.focus(), this.search_field.val(this.search_field.val()), this.winnow_results(), this.form_field_jq.trigger("chosen:showing_dropdown", {
          chosen: this
        }))
      }, i.prototype.update_results_content = function (t) {
        return this.search_results.html(t)
      }, i.prototype.results_hide = function () {
        return this.results_showing && (this.result_clear_highlight(), this.container.removeClass("chosen-with-drop"), this.form_field_jq.trigger("chosen:hiding_dropdown", {
          chosen: this
        })), this.results_showing = !1
      }, i.prototype.set_tab_index = function (t) {
        var e;
        return this.form_field.tabIndex ? (e = this.form_field.tabIndex, this.form_field.tabIndex = -1, this.search_field[0].tabIndex = e) : void 0
      }, i.prototype.set_label_behavior = function () {
        var e = this;
        return this.form_field_label = this.form_field_jq.parents("label"), !this.form_field_label.length && this.form_field.id.length && (this.form_field_label = t("label[for='" + this.form_field.id + "']")), this.form_field_label.length > 0 ? this.form_field_label.bind("click.chosen", function (t) {
          return e.is_multiple ? e.container_mousedown(t) : e.activate_field()
        }) : void 0
      }, i.prototype.show_search_field_default = function () {
        return this.is_multiple && this.choices_count() < 1 && !this.active_field ? (this.search_field.val(this.default_text), this.search_field.addClass("default")) : (this.search_field.val(""), this.search_field.removeClass("default"))
      }, i.prototype.search_results_mouseup = function (e) {
        var i;
        return i = t(e.target).hasClass("active-result") ? t(e.target) : t(e.target).parents(".active-result").first(), i.length ? (this.result_highlight = i, this.result_select(e), this.search_field.focus()) : void 0
      }, i.prototype.search_results_mouseover = function (e) {
        var i;
        return i = t(e.target).hasClass("active-result") ? t(e.target) : t(e.target).parents(".active-result").first(), i ? this.result_do_highlight(i) : void 0
      }, i.prototype.search_results_mouseout = function (e) {
        return t(e.target).hasClass("active-result") ? this.result_clear_highlight() : void 0
      }, i.prototype.choice_build = function (e) {
        var i, n, o = this;
        return i = t("<li />", {
          class: "search-choice"
        }).html("<span>" + this.choice_label(e) + "</span>"), e.disabled ? i.addClass("search-choice-disabled") : (n = t("<a />", {
          class: "search-choice-close",
          "data-option-array-index": e.array_index
        }), n.bind("click.chosen", function (t) {
          return o.choice_destroy_link_click(t)
        }), i.append(n)), this.search_container.before(i)
      }, i.prototype.choice_destroy_link_click = function (e) {
        return e.preventDefault(), e.stopPropagation(), this.is_disabled ? void 0 : this.choice_destroy(t(e.target))
      }, i.prototype.choice_destroy = function (t) {
        return this.result_deselect(t[0].getAttribute("data-option-array-index")) ? (this.show_search_field_default(), this.is_multiple && this.choices_count() > 0 && this.search_field.val().length < 1 && this.results_hide(), t.parents("li").first().remove(), this.search_field_scale()) : void 0
      }, i.prototype.results_reset = function () {
        return this.reset_single_select_options(), this.form_field.options[0].selected = !0, this.single_set_selected_text(), this.show_search_field_default(), this.results_reset_cleanup(), this.form_field_jq.trigger("change"), this.active_field ? this.results_hide() : void 0
      }, i.prototype.results_reset_cleanup = function () {
        return this.current_selectedIndex = this.form_field.selectedIndex, this.selected_item.find("abbr").remove()
      }, i.prototype.result_select = function (t) {
        var e, i;
        return this.result_highlight ? (e = this.result_highlight, this.result_clear_highlight(), this.is_multiple && this.max_selected_options <= this.choices_count() ? (this.form_field_jq.trigger("chosen:maxselected", {
          chosen: this
        }), !1) : (this.is_multiple ? e.removeClass("active-result") : this.reset_single_select_options(), e.addClass("result-selected"), i = this.results_data[e[0].getAttribute("data-option-array-index")], i.selected = !0, this.form_field.options[i.options_index].selected = !0, this.selected_option_count = null, this.is_multiple ? this.choice_build(i) : this.single_set_selected_text(this.choice_label(i)), (t.metaKey || t.ctrlKey) && this.is_multiple || this.results_hide(), this.show_search_field_default(), (this.is_multiple || this.form_field.selectedIndex !== this.current_selectedIndex) && this.form_field_jq.trigger("change", {
          selected: this.form_field.options[i.options_index].value
        }), this.current_selectedIndex = this.form_field.selectedIndex, t.preventDefault(), this.search_field_scale())) : void 0
      }, i.prototype.single_set_selected_text = function (t) {
        return null == t && (t = this.default_text), t === this.default_text ? this.selected_item.addClass("chosen-default") : (this.single_deselect_control_build(), this.selected_item.removeClass("chosen-default")), this.selected_item.find("span").html(t)
      }, i.prototype.result_deselect = function (t) {
        var e;
        return e = this.results_data[t], !this.form_field.options[e.options_index].disabled && (e.selected = !1, this.form_field.options[e.options_index].selected = !1, this.selected_option_count = null, this.result_clear_highlight(), this.results_showing && this.winnow_results(), this.form_field_jq.trigger("change", {
          deselected: this.form_field.options[e.options_index].value
        }), this.search_field_scale(), !0)
      }, i.prototype.single_deselect_control_build = function () {
        return this.allow_single_deselect ? (this.selected_item.find("abbr").length || this.selected_item.find("span").first().after('<abbr class="search-choice-close"></abbr>'), this.selected_item.addClass("chosen-single-with-deselect")) : void 0
      }, i.prototype.get_search_text = function () {
        return t("<div/>").text(t.trim(this.search_field.val())).html()
      }, i.prototype.winnow_results_set_highlight = function () {
        var t, e;
        return e = this.is_multiple ? [] : this.search_results.find(".result-selected.active-result"), t = e.length ? e.first() : this.search_results.find(".active-result").first(), null != t ? this.result_do_highlight(t) : void 0
      }, i.prototype.no_results = function (e) {
        var i;
        return i = t('<li class="no-results">' + this.results_none_found + ' "<span></span>"</li>'), i.find("span").first().html(e),
          this.search_results.append(i), this.form_field_jq.trigger("chosen:no_results", {
            chosen: this
          })
      }, i.prototype.no_results_clear = function () {
        return this.search_results.find(".no-results").remove()
      }, i.prototype.keydown_arrow = function () {
        var t;
        return this.results_showing && this.result_highlight ? (t = this.result_highlight.nextAll("li.active-result").first()) ? this.result_do_highlight(t) : void 0 : this.results_show()
      }, i.prototype.keyup_arrow = function () {
        var t;
        return this.results_showing || this.is_multiple ? this.result_highlight ? (t = this.result_highlight.prevAll("li.active-result"), t.length ? this.result_do_highlight(t.first()) : (this.choices_count() > 0 && this.results_hide(), this.result_clear_highlight())) : void 0 : this.results_show()
      }, i.prototype.keydown_backstroke = function () {
        var t;
        return this.pending_backstroke ? (this.choice_destroy(this.pending_backstroke.find("a").first()), this.clear_backstroke()) : (t = this.search_container.siblings("li.search-choice").last(), t.length && !t.hasClass("search-choice-disabled") ? (this.pending_backstroke = t, this.single_backstroke_delete ? this.keydown_backstroke() : this.pending_backstroke.addClass("search-choice-focus")) : void 0)
      }, i.prototype.clear_backstroke = function () {
        return this.pending_backstroke && this.pending_backstroke.removeClass("search-choice-focus"), this.pending_backstroke = null
      }, i.prototype.keydown_checker = function (t) {
        var e, i;
        switch (e = null != (i = t.which) ? i : t.keyCode, this.search_field_scale(), 8 !== e && this.pending_backstroke && this.clear_backstroke(), e) {
          case 8:
            this.backstroke_length = this.search_field.val().length;
            break;
          case 9:
            this.results_showing && !this.is_multiple && this.result_select(t), this.mouse_on_container = !1;
            break;
          case 13:
            this.results_showing && t.preventDefault();
            break;
          case 32:
            this.disable_search && t.preventDefault();
            break;
          case 38:
            t.preventDefault(), this.keyup_arrow();
            break;
          case 40:
            t.preventDefault(), this.keydown_arrow()
        }
      }, i.prototype.search_field_scale = function () {
        var e, i, n, o, s, r, a, l, c;
        if (this.is_multiple) {
          for (n = 0, a = 0, s = "position:absolute; left: -1000px; top: -1000px; display:none;", r = ["font-size", "font-style", "font-weight", "font-family", "line-height", "text-transform", "letter-spacing"], l = 0, c = r.length; c > l; l++) o = r[l], s += o + ":" + this.search_field.css(o) + ";";
          return e = t("<div />", {
            style: s
          }), e.text(this.search_field.val()), t("body").append(e), a = e.width() + 25, e.remove(), i = this.container.outerWidth(), a > i - 10 && (a = i - 10), this.search_field.css({
            width: a + "px"
          })
        }
      }, i
    }(e)
  }.call(this), ! function (t) {
    if ("object" == typeof exports && "undefined" != typeof module) module.exports = t();
    else if ("function" == typeof define && define.amd) define([], t);
    else {
      var e;
      e = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this, e.Clipboard = t()
    }
  }(function () {
    var t;
    return function t(e, i, n) {
      function o(r, a) {
        if (!i[r]) {
          if (!e[r]) {
            var l = "function" == typeof require && require;
            if (!a && l) return l(r, !0);
            if (s) return s(r, !0);
            var c = new Error("Cannot find module '" + r + "'");
            throw c.code = "MODULE_NOT_FOUND", c
          }
          var u = i[r] = {
            exports: {}
          };
          e[r][0].call(u.exports, function (t) {
            var i = e[r][1][t];
            return o(i ? i : t)
          }, u, u.exports, t, e, i, n)
        }
        return i[r].exports
      }
      for (var s = "function" == typeof require && require, r = 0; r < n.length; r++) o(n[r]);
      return o
    }({
      1: [function (t, e, i) {
        function n(t, e) {
          for (; t && t.nodeType !== o;) {
            if (t.matches(e)) return t;
            t = t.parentNode
          }
        }
        var o = 9;
        if (Element && !Element.prototype.matches) {
          var s = Element.prototype;
          s.matches = s.matchesSelector || s.mozMatchesSelector || s.msMatchesSelector || s.oMatchesSelector || s.webkitMatchesSelector
        }
        e.exports = n
      }, {}],
      2: [function (t, e, i) {
        function n(t, e, i, n, s) {
          var r = o.apply(this, arguments);
          return t.addEventListener(i, r, s), {
            destroy: function () {
              t.removeEventListener(i, r, s)
            }
          }
        }

        function o(t, e, i, n) {
          return function (i) {
            i.delegateTarget = s(i.target, e), i.delegateTarget && n.call(t, i)
          }
        }
        var s = t("./closest");
        e.exports = n
      }, {
        "./closest": 1
      }],
      3: [function (t, e, i) {
        i.node = function (t) {
          return void 0 !== t && t instanceof HTMLElement && 1 === t.nodeType
        }, i.nodeList = function (t) {
          var e = Object.prototype.toString.call(t);
          return void 0 !== t && ("[object NodeList]" === e || "[object HTMLCollection]" === e) && "length" in t && (0 === t.length || i.node(t[0]))
        }, i.string = function (t) {
          return "string" == typeof t || t instanceof String
        }, i.fn = function (t) {
          var e = Object.prototype.toString.call(t);
          return "[object Function]" === e
        }
      }, {}],
      4: [function (t, e, i) {
        function n(t, e, i) {
          if (!t && !e && !i) throw new Error("Missing required arguments");
          if (!a.string(e)) throw new TypeError("Second argument must be a String");
          if (!a.fn(i)) throw new TypeError("Third argument must be a Function");
          if (a.node(t)) return o(t, e, i);
          if (a.nodeList(t)) return s(t, e, i);
          if (a.string(t)) return r(t, e, i);
          throw new TypeError("First argument must be a String, HTMLElement, HTMLCollection, or NodeList")
        }

        function o(t, e, i) {
          return t.addEventListener(e, i), {
            destroy: function () {
              t.removeEventListener(e, i)
            }
          }
        }

        function s(t, e, i) {
          return Array.prototype.forEach.call(t, function (t) {
            t.addEventListener(e, i)
          }), {
            destroy: function () {
              Array.prototype.forEach.call(t, function (t) {
                t.removeEventListener(e, i)
              })
            }
          }
        }

        function r(t, e, i) {
          return l(document.body, t, e, i)
        }
        var a = t("./is"),
          l = t("delegate");
        e.exports = n
      }, {
        "./is": 3,
        delegate: 2
      }],
      5: [function (t, e, i) {
        function n(t) {
          var e;
          if ("SELECT" === t.nodeName) t.focus(), e = t.value;
          else if ("INPUT" === t.nodeName || "TEXTAREA" === t.nodeName) {
            var i = t.hasAttribute("readonly");
            i || t.setAttribute("readonly", ""), t.select(), t.setSelectionRange(0, t.value.length), i || t.removeAttribute("readonly"), e = t.value
          } else {
            t.hasAttribute("contenteditable") && t.focus();
            var n = window.getSelection(),
              o = document.createRange();
            o.selectNodeContents(t), n.removeAllRanges(), n.addRange(o), e = n.toString()
          }
          return e
        }
        e.exports = n
      }, {}],
      6: [function (t, e, i) {
        function n() {}
        n.prototype = {
          on: function (t, e, i) {
            var n = this.e || (this.e = {});
            return (n[t] || (n[t] = [])).push({
              fn: e,
              ctx: i
            }), this
          },
          once: function (t, e, i) {
            function n() {
              o.off(t, n), e.apply(i, arguments)
            }
            var o = this;
            return n._ = e, this.on(t, n, i)
          },
          emit: function (t) {
            var e = [].slice.call(arguments, 1),
              i = ((this.e || (this.e = {}))[t] || []).slice(),
              n = 0,
              o = i.length;
            for (n; n < o; n++) i[n].fn.apply(i[n].ctx, e);
            return this
          },
          off: function (t, e) {
            var i = this.e || (this.e = {}),
              n = i[t],
              o = [];
            if (n && e)
              for (var s = 0, r = n.length; s < r; s++) n[s].fn !== e && n[s].fn._ !== e && o.push(n[s]);
            return o.length ? i[t] = o : delete i[t], this
          }
        }, e.exports = n
      }, {}],
      7: [function (e, i, n) {
        ! function (o, s) {
          if ("function" == typeof t && t.amd) t(["module", "select"], s);
          else if ("undefined" != typeof n) s(i, e("select"));
          else {
            var r = {
              exports: {}
            };
            s(r, o.select), o.clipboardAction = r.exports
          }
        }(this, function (t, e) {
          "use strict";

          function i(t) {
            return t && t.__esModule ? t : {
              default: t
            }
          }

          function n(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
          }
          var o = i(e),
            s = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (t) {
              return typeof t
            } : function (t) {
              return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
            },
            r = function () {
              function t(t, e) {
                for (var i = 0; i < e.length; i++) {
                  var n = e[i];
                  n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n)
                }
              }
              return function (e, i, n) {
                return i && t(e.prototype, i), n && t(e, n), e
              }
            }(),
            a = function () {
              function t(e) {
                n(this, t), this.resolveOptions(e), this.initSelection()
              }
              return r(t, [{
                key: "resolveOptions",
                value: function () {
                  var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                  this.action = t.action, this.emitter = t.emitter, this.target = t.target, this.text = t.text, this.trigger = t.trigger, this.selectedText = ""
                }
              }, {
                key: "initSelection",
                value: function () {
                  this.text ? this.selectFake() : this.target && this.selectTarget()
                }
              }, {
                key: "selectFake",
                value: function () {
                  var t = this,
                    e = "rtl" == document.documentElement.getAttribute("dir");
                  this.removeFake(), this.fakeHandlerCallback = function () {
                    return t.removeFake()
                  }, this.fakeHandler = document.body.addEventListener("click", this.fakeHandlerCallback) || !0, this.fakeElem = document.createElement("textarea"), this.fakeElem.style.fontSize = "12pt", this.fakeElem.style.border = "0", this.fakeElem.style.padding = "0", this.fakeElem.style.margin = "0", this.fakeElem.style.position = "absolute", this.fakeElem.style[e ? "right" : "left"] = "-9999px";
                  var i = window.pageYOffset || document.documentElement.scrollTop;
                  this.fakeElem.style.top = i + "px", this.fakeElem.setAttribute("readonly", ""), this.fakeElem.value = this.text, document.body.appendChild(this.fakeElem), this.selectedText = (0, o.default)(this.fakeElem), this.copyText()
                }
              }, {
                key: "removeFake",
                value: function () {
                  this.fakeHandler && (document.body.removeEventListener("click", this.fakeHandlerCallback), this.fakeHandler = null, this.fakeHandlerCallback = null), this.fakeElem && (document.body.removeChild(this.fakeElem), this.fakeElem = null)
                }
              }, {
                key: "selectTarget",
                value: function () {
                  this.selectedText = (0, o.default)(this.target), this.copyText()
                }
              }, {
                key: "copyText",
                value: function () {
                  var t = void 0;
                  try {
                    t = document.execCommand(this.action)
                  } catch (e) {
                    t = !1
                  }
                  this.handleResult(t)
                }
              }, {
                key: "handleResult",
                value: function (t) {
                  this.emitter.emit(t ? "success" : "error", {
                    action: this.action,
                    text: this.selectedText,
                    trigger: this.trigger,
                    clearSelection: this.clearSelection.bind(this)
                  })
                }
              }, {
                key: "clearSelection",
                value: function () {
                  this.target && this.target.blur(), window.getSelection().removeAllRanges()
                }
              }, {
                key: "destroy",
                value: function () {
                  this.removeFake()
                }
              }, {
                key: "action",
                set: function () {
                  var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "copy";
                  if (this._action = t, "copy" !== this._action && "cut" !== this._action) throw new Error('Invalid "action" value, use either "copy" or "cut"')
                },
                get: function () {
                  return this._action
                }
              }, {
                key: "target",
                set: function (t) {
                  if (void 0 !== t) {
                    if (!t || "object" !== ("undefined" == typeof t ? "undefined" : s(t)) || 1 !== t.nodeType) throw new Error('Invalid "target" value, use a valid Element');
                    if ("copy" === this.action && t.hasAttribute("disabled")) throw new Error('Invalid "target" attribute. Please use "readonly" instead of "disabled" attribute');
                    if ("cut" === this.action && (t.hasAttribute("readonly") || t.hasAttribute("disabled"))) throw new Error('Invalid "target" attribute. You can\'t cut text from elements with "readonly" or "disabled" attributes');
                    this._target = t
                  }
                },
                get: function () {
                  return this._target
                }
              }]), t
            }();
          t.exports = a
        })
      }, {
        select: 5
      }],
      8: [function (e, i, n) {
        ! function (o, s) {
          if ("function" == typeof t && t.amd) t(["module", "./clipboard-action", "tiny-emitter", "good-listener"], s);
          else if ("undefined" != typeof n) s(i, e("./clipboard-action"), e("tiny-emitter"), e("good-listener"));
          else {
            var r = {
              exports: {}
            };
            s(r, o.clipboardAction, o.tinyEmitter, o.goodListener), o.clipboard = r.exports
          }
        }(this, function (t, e, i, n) {
          "use strict";

          function o(t) {
            return t && t.__esModule ? t : {
              default: t
            }
          }

          function s(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
          }

          function r(t, e) {
            if (!t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !e || "object" != typeof e && "function" != typeof e ? t : e
          }

          function a(t, e) {
            if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function, not " + typeof e);
            t.prototype = Object.create(e && e.prototype, {
              constructor: {
                value: t,
                enumerable: !1,
                writable: !0,
                configurable: !0
              }
            }), e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e)
          }

          function l(t, e) {
            var i = "data-clipboard-" + t;
            if (e.hasAttribute(i)) return e.getAttribute(i)
          }
          var c = o(e),
            u = o(i),
            h = o(n),
            d = function () {
              function t(t, e) {
                for (var i = 0; i < e.length; i++) {
                  var n = e[i];
                  n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n)
                }
              }
              return function (e, i, n) {
                return i && t(e.prototype, i), n && t(e, n), e
              }
            }(),
            p = function (t) {
              function e(t, i) {
                s(this, e);
                var n = r(this, (e.__proto__ || Object.getPrototypeOf(e)).call(this));
                return n.resolveOptions(i), n.listenClick(t), n
              }
              return a(e, t), d(e, [{
                key: "resolveOptions",
                value: function () {
                  var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                  this.action = "function" == typeof t.action ? t.action : this.defaultAction, this.target = "function" == typeof t.target ? t.target : this.defaultTarget, this.text = "function" == typeof t.text ? t.text : this.defaultText
                }
              }, {
                key: "listenClick",
                value: function (t) {
                  var e = this;
                  this.listener = (0, h.default)(t, "click", function (t) {
                    return e.onClick(t)
                  })
                }
              }, {
                key: "onClick",
                value: function (t) {
                  var e = t.delegateTarget || t.currentTarget;
                  this.clipboardAction && (this.clipboardAction = null), this.clipboardAction = new c.default({
                    action: this.action(e),
                    target: this.target(e),
                    text: this.text(e),
                    trigger: e,
                    emitter: this
                  })
                }
              }, {
                key: "defaultAction",
                value: function (t) {
                  return l("action", t)
                }
              }, {
                key: "defaultTarget",
                value: function (t) {
                  var e = l("target", t);
                  if (e) return document.querySelector(e)
                }
              }, {
                key: "defaultText",
                value: function (t) {
                  return l("text", t)
                }
              }, {
                key: "destroy",
                value: function () {
                  this.listener.destroy(), this.clipboardAction && (this.clipboardAction.destroy(), this.clipboardAction = null)
                }
              }], [{
                key: "isSupported",
                value: function () {
                  var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : ["copy", "cut"],
                    e = "string" == typeof t ? [t] : t,
                    i = !!document.queryCommandSupported;
                  return e.forEach(function (t) {
                    i = i && !!document.queryCommandSupported(t)
                  }), i
                }
              }]), e
            }(u.default);
          t.exports = p
        })
      }, {
        "./clipboard-action": 7,
        "good-listener": 4,
        "tiny-emitter": 6
      }]
    }, {}, [8])(8)
  }), $(document).ready(function () {
    var t = getBody();
    $(".head-nav .links-cell a, .nav-cell a").clone().appendTo(".js-inwrapmenu .first-m"), $(".js-navclone a").clone().appendTo(".js-inwrapmenu .second-m"), $(".js-clone-rform").clone().appendTo(".js-cloned-form"), $(".js-cl-mobile").clone().appendTo(".js-lvr-mobile").removeClass("lvr-block").addClass("mobile-lvr"), $(".js-clone-fc").clone().appendTo(".js-hidden-fc"), $(".spoiler-block dt").click(function (t) {
      $(this).toggleClass("active"), $(this).parent(".spoiler-block").toggleClass("active"), $(this).parent(".spoiler-block").children("dd").slideToggle(), t.preventDefault()
    }), $(".chosen-select").length > 0 && $(".chosen-select").chosen({
      width: "100%",
      disable_search_threshold: 10
    }), $(".js-mdt").length > 0 && tablegradient(), $(".js-block-tables").length > 0 && inwrapheight(".js-block-tables li"), t.on("change", ".chosen-select", function () {
      var t = $(this).children("option:selected").data("sum"),
        e = $(this).data("cont");
      $(e).html(t)
    }), $(".js-forcont").length > 0 && $(".js-forcont").each(function () {
      var t = $(this).data("cont"),
        e = $(this).children("option:selected").data("sum");
      $(t).html(e)
    }), $(".tooltip").length > 0 && (1 == device.mobile() || 1 == device.tablet() ? $(".tooltip").tooltipster({
      trigger: "click"
    }) : $(".tooltip").tooltipster()), $(".open-menu").click(function (t) {
      $(this).toggleClass("active"), $(".js-cloned-menu").toggleClass("active"), t.preventDefault()
    }), t.prepend("<div class='modal-shadow'></div>"), $(".modal-window").each(function () {
      $(this).prepend("<a href='' class='closemodal'></a>")
    }), $(".ac-input").length > 0 && $(".ac-input").each(function () {
      $(this).find(".svs-input").val().length > 0 && $(this).addClass("active")
    });
    var e = $(".js-mask-phone");
    if (e.length > 0 && e.inputmask("+7 (999) 999 - 99 - 99"), t.on("click", ".closemodal, .modal-shadow", function (t) {
        $(this).removeClass("overflow-owt"), $("body").removeClass("overflow-owt"), $(".modal-window, .modal-shadow").animate({
          opacity: 0
        }, 500, function () {
          $(".modal-window, .modal-shadow").removeClass("js-active"), $(".modal-window").removeClass("js-window-absolut"), $("#checkout").removeClass("dv-active")
        }), t.preventDefault()
      }), $(document).on("keyup keypress", ".svs-input", function () {
        $(this).val().length > 0 ? $(this).parents(".ac-input").addClass("active") : $(this).parents(".ac-input").removeClass("active")
      }), $(".search-input").length > 0 && $(".search-input").focus(), $(".js-next-blr").click(function (t) {
        $(this).parent(".js-relative").addClass("active").find(".js-receipt").css({
          display: "block"
        }).animate({
          opacity: 1
        }, 500), t.preventDefault()
      }), $(".js-mc-close").click(function (t) {
        $(this).parents(".js-relative").removeClass("active").find(".js-receipt").animate({
          opacity: 0
        }, 500, function () {
          $(".js-receipt").removeAttr("style")
        }), t.preventDefault()
      }), $(".view-pass").on("click", function (t) {
        $(".js-view-pass").hasClass("viewps") ? ($(".js-view-pass").attr("type", "password"), $(".js-view-pass").addClass("viewps")) : ($(".js-view-pass").attr("type", "text"), $(".js-view-pass").removeClass("viewps")), t.preventDefault()
      }), $(".js-owl-carousel").length > 0 && $(".js-owl-carousel").owlCarousel({
        loop: !0,
        margin: 26,
        nav: !0,
        dots: !1,
        responsive: {
          0: {
            items: 1
          },
          700: {
            items: 2
          },
          1e3: {
            items: 3
          }
        }
      }), $(".chart").length > 0 && $(".chart").easyPieChart({
        easing: "easeOutBounce",
        barColor: "#ffd407",
        trackColor: "#3d3d3d",
        scaleColor: !1,
        lineWidth: 10,
        size: 50,
        lineCap: "circle"
      }), $(".js-int-height").length > 0 && inwrapheight(".js-int-height"), $(".js-height-al").length > 0 && inwrapheight(".js-height-al"), $(".js-height-al").length > 0 && inwrapheight(".js-height-al"), $(".js-height-alw").length > 0 && inwrapheight(".js-height-alw"), $(".js-height-ar").length > 0 && inwrapheight(".js-height-ar"), $(".ilt-item").length > 0 && inwrapheight(".ilt-item"), $(".js-loadpercent").length > 0)
      for (var i = $(".inload").length, n = 0; n < i; n++) ! function (t) {
        setTimeout(function () {
          $(".inload").eq(t).find(".js-loadpercent").removeClass("js-loadpercent").html("Данные получены")
        }, 1e3 * n)
      }(n);
    $(document).click(function (t) {
      $(t.target).closest(".js-relative").length || $(".js-relative.active .js-receipt").animate({
        opacity: 0
      }, 500, function () {
        $(".js-receipt").removeAttr("style")
      }), t.stopPropagation()
    })
  });
var doit;
window.onresize = function () {
  clearTimeout(doit), doit = setTimeout(function () {
    $(".js-int-height").length > 0 && inwrapheight(".js-int-height"), $(".js-block-tables").length > 0 && inwrapheight(".js-block-tables li"), $(".ilt-item").length > 0 && inwrapheight(".ilt-item"), $(".js-height-alw").length > 0 && inwrapheight(".js-height-alw"), $(".js-mdt").length > 0 && tablegradient(), $(".js-height-ar").length > 0 && inwrapheight(".js-height-ar"), $(".modal-window.js-active").length > 0 && modalresizer()
  }, 500)
}, $(function () {
  var t = window.data || {};
  window.data = t, $(".ncl-list li").length > 0 && ($(window).width() > 1e3 ? carlist(".ncl-list li", "white-class", 2) : carlist(".ncl-list li", "white-class", 1)), $(".car-list2").length > 0 && ($(window).width() > 1100 ? carlist(".car-list2 li", "white-class", 4) : $(window).width() < 1101 && $(window).width() > 800 ? carlist(".car-list2 li", "white-class", 3) : $(window).width() < 801 && $(window).width() > 600 ? carlist(".car-list2 li", "white-class", 2) : carlist(".car-list2 li", "white-class", 1)), $(".car-list").length > 0 && ($(window).width() > 1100 ? carlist(".car-list li", "white-class", 6) : $(window).width() < 1101 && $(window).width() > 1e3 ? carlist(".car-list li", "white-class", 5) : $(window).width() < 1001 && $(window).width() > 800 ? carlist(".car-list li", "white-class", 4) : $(window).width() < 801 && $(window).width() > 600 ? carlist(".car-list li", "white-class", 3) : $(window).width() < 601 && $(window).width() > 500 ? carlist(".car-list li", "white-class", 2) : carlist(".car-list li", "white-class", 1)), $(document).on("submit", ".search-form", function (t) {
    t.preventDefault(), t.stopImmediatePropagation(), yaReachGoal("CHECK_AUTO");
    var e = $("#report"),
      i = $(this).find("input").val().toUpperCase();
    e.addClass("loading"), e.find(".dtsp-table").removeClass("active"), e.find(".loader").show(), e.find(".inpr").hide(), e.find(".buy-full-report").hide(), e.find(".buy-report-package").hide(), e.find(".b-politics").hide(), e.find(".number").text(i), e.find("ul.dtsp-table li").show(),

      openmodal("report");
      

       (function () {
         var t = {
  "token": "28f832609041b29e14c13207782eb684",
  "date": "06.07.2017 \u0432 11:07:08",
  "data": {
    "isVin": false,
    "number": "A111AA77",
    "vin": "SALL*********8132",
    "model": "\u041b\u0415\u041d\u0414 \u0420\u041e\u0412\u0415\u0420 \u0420\u0415\u0419\u041d\u0414\u0416 \u0420\u041e\u0412\u0415\u0420 \u0421\u041f\u041e\u0420\u0422",
    "regnums": "\u041011***77",
    "year": 2007,
    "category": "\u2014",
    "categoryDescription": "\u2014",
    "wheel": "\u0421\u043b\u0435\u0432\u0430",
    "enginePower": "287/390",
    "engineVolume": 4197,
    "engineModel": "428PS",
    "engineNumber": "150207\u041221420",
    "engineType": "\u0411\u0435\u043d\u0437\u0438\u043d\u043e\u0432\u044b\u0439",
    "weight": "\u2014/\u2014",
    "color": "\u0427\u0415\u0420\u041d\u042b\u0419",
    "bodyNumber": "S\u0410LL*********8132",
    "bodyType": "\u2014",
    "driveNumber": "SALL*********8132",
    "driveType": "\u2014"
  },
  "old_price": 0,
  "new_price": 249,
  "old_package_price": 1659,
  "new_package_price": 945,
  "regnum_image": "https://avtoraport.ru/regnum/A111AA77"
};
 
          
            window.data.reportToken = t.token, e.find(".created").text("Проверка проведена " + t.date), e.find(".model").text(t.data.model), +t.old_price > 0 ? (e.find(".old_price").text(t.old_price), e.find(".b-old_price").show()) : e.find(".b-old_price").hide(), +t.old_package_price > 0 ? (e.find(".old_package_price").text(t.old_package_price), e.find(".b-old_package_price").show()) : e.find(".b-old_package_price").hide(), e.find(".new_price").text(t.new_price), e.find(".new_package_price").text(t.new_package_price), e.find(".activate-report").attr("href", "/auto/activate/" + t.token);
            for (var n in t.data) {
              var o = t.data[n];
              "—" != o && "—/—" != o ? e.find("." + n + " .pr-load").text(o) : e.find("." + n).hide()
            }
            e.removeClass("loading"), e.find(".dtsp-table").addClass("active"), e.find(".loader").hide(), e.find(".inpr").show(), e.find(".buy-full-report").css("display", "table-cell"), e.find(".buy-report-package").css("display", "table-cell"), e.find(".b-politics").show()
          
        })()
      
  }), $(document).on("click", ".buy-full-report", function () {
    openmodal("reportPay"), $.ajax({
      type: "GET",
      url: "/payment/pay/" + window.data.reportToken,
      success: function (t) {
        $("#reportPay").html(t), modalresizer()
      }
    })
  }), $(document).on("click", ".buy-report-package", function () {
    openmodal("reportPackagePay"), $.ajax({
      type: "GET",
      url: "/payment/pay/" + window.data.reportToken + "?is_package=1",
      success: function (t) {
        $("#reportPackagePay").html(t), modalresizer()
      }
    })
  }), $(document).on("click", ".buy-from-history", function () {
    window.data.reportToken = $(this).data("token"), openmodal("reportPay"), $.ajax({
      type: "GET",
      url: "/payment/pay/" + window.data.reportToken,
      success: function (t) {
        $("#reportPay").html(t), modalresizer()
      }
    })
  }), $(document).on("click", ".show-search-history", function () {
    function t() {
      var t = $("#searchHistory");
      t.find(".load-row").show(), t.find(".pager-wrap").remove(), t.find(".nc-tr").remove(), t.find(".search-input").val("")
    }
    return t(), openmodal("searchHistory"), $.ajax({
      type: "GET",
      url: "/auto/history",
      success: function (t) {
        var e = $("#searchHistory");
        e.find(".pager-wrap").remove(), e.find(".js-mdt").replaceWith(t)
      }
    }), !1
  }), $(document).on("click", ".show-search-modal", function () {
    return $("#searchModal").find(".search-input").val(""), openmodal("searchModal"), !1
  }), $(document).on("click", ".price-pay-btn", function () {
    var t = $(this).data("price-id"),
      e = $("#packagePay");
    if (0 != t) {
      e.find("select").val(t).trigger("chosen:updated");
      var i = e.find("option:selected").data("sum");
      e.find(".old-price").text(i), e.find(".js-sum").text(i)
    } else e.find("select option:first-child").attr("selected", "selected");
    return openmodal("packagePay"), !1
  }), $(document).on("change", ".price-select", function () {
    var t = $(this).find(":selected").data("sum");
    $("#packagePay").find(".js-sum").text(t)
  }), $(document).on("click", ".prev-link", function () {
    openmodal($(this).data("prev-modal-id"))
  }), "" != $(".header-search .search-form").find("input").val() && $(".header-search .search-form").trigger("submit"), $(".copy-btn").each(function (t, e) {
    var i = $(e),
      n = new Clipboard(e),
      o = i.tooltipster({
        trigger: "click"
      }).tooltipster("instance");
    n.on("success", function (t) {
      o.content(i.attr("data-copied"))
    }).on("error", function (t) {
      o.content(i.attr("data-copyerror"))
    })
  }), $(".search-form .search-input").on("change keyup paste", function () {
    var t = $(this).val(),
      e = /^[а-яА-Яa-zA-Z][0-9]{3}[а-яА-Яa-zA-Z]{2}[0-9]{2,3}$/,
      i = /^[0-9A-Za-zА-Яа-я]{17}$/;
    i.test(t) ? yaReachGoal("VIN_CORRECT") : e.test(t) ? yaReachGoal("NUM_CORRECT") : /[A-Za-z0-9]+/.test(t) ? yaReachGoal("VIN_TYPING") : /[А-Яа-я0-9]+/.test(t) && yaReachGoal("NUM_TYPING")
  }), $("a.irs-button").click(function () {
    yaReachGoal("BUY_PRESSED")
  })
});
var doit;
window.onresize = function () {
  clearTimeout(doit), doit = setTimeout(function () {
    $(".ncl-list li").length > 0 && ($(window).width() > 1e3 ? carlist(".ncl-list li", "white-class", 2) : carlist(".ncl-list li", "white-class", 1)), $(".car-list2").length > 0 && ($(window).width() > 1100 ? carlist(".car-list2 li", "white-class", 4) : $(window).width() < 1101 && $(window).width() > 800 ? carlist(".car-list2 li", "white-class", 3) : $(window).width() < 801 && $(window).width() > 600 ? carlist(".car-list2 li", "white-class", 2) : carlist(".car-list2 li", "white-class", 1)), $(".car-list").length > 0 && ($(window).width() > 1100 ? carlist(".car-list li", "white-class", 6) : $(window).width() < 1101 && $(window).width() > 1e3 ? carlist(".car-list li", "white-class", 5) : $(window).width() < 1001 && $(window).width() > 800 ? carlist(".car-list li", "white-class", 4) : $(window).width() < 801 && $(window).width() > 600 ? carlist(".car-list li", "white-class", 3) : $(window).width() < 601 && $(window).width() > 500 ? carlist(".car-list li", "white-class", 2) : carlist(".car-list li", "white-class", 1)), $(".js-int-height").length > 0 && inwrapheight(".js-int-height"), $(".js-block-tables").length > 0 && inwrapheight(".js-block-tables li"), $(".ilt-item").length > 0 && inwrapheight(".ilt-item"), $(".js-height-alw").length > 0 && inwrapheight(".js-height-alw"), $(".js-mdt").length > 0 && tablegradient(), $(".js-height-ar").length > 0 && inwrapheight(".js-height-ar"), $(".modal-window.js-active").length > 0 && modalresizer()
  }, 500)
};